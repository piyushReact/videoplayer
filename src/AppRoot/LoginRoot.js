import React, { } from 'react';
import { createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack'
import Landing from '../Screens/LoginRoot/Landing';
import SignUp from '../Screens/LoginRoot/SignUp';
import Login from '../Screens/LoginRoot/Login';
import ForgotPass from '../Screens/LoginRoot/ForgotPass';
// import Profile from '../Screens/LoginRoots/Profile';
// import EnterOTP from '../Screens/LoginRoot/EnterOtp';
// import welcome from '../Screens/LoginRoot/Welcome';
// import NoInternet from '../Components/NoInternet'


export default LoginRoot = () => {
    return createStackNavigator(
        {
            // Landing: { screen: Landing },
            SignUp: { screen: SignUp }, 
            Login: { screen: Login },
            ForgotPass: { screen: ForgotPass },
            // EnterOTP: { screen: EnterOTP },
            // Profile: { screen: Profile },
            // Welcome: { screen: welcome },
            // NoInternet: { screen: NoInternet },

        },
        {
            initialRouteName: "Login",
            headerMode: 'none',
          
        }
    )
}