import React, { Component } from 'react';
import { Container, Header, Content, Footer, FooterTab, Button, Icon, Text, Left, Right } from 'native-base';
import { StyleSheet, TouchableOpacity, Image, ImageBackground, View, AsyncStorage } from 'react-native';
// import IconEnts from 'react-native-vector-icons/Entypo'
import ImagePicker from 'react-native-image-picker';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { greyText, drawerbackgraoundcolr } from '../Common/Colors';

let data = [
  { index: 0, title: "Home", isSelected: true },
  { index: 1, title: "My Profile", isSelected: false },
  { index: 2, title: "Manage Subscriptions", isSelected: false },
  { index: 3, title: "My Tournaments", isSelected: false },
  { index: 4, title: "Settings", isSelected: false },
  { index: 5, title: "Logout", isSelected: false },
]

export default class SideMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      filePath: {},
      profile: {},
      profileImg: "https://i0.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?fit=256%2C256&quality=100&ssl=1",
      username: "Detra Mcmunn",
      userlocation: " Long Beach , CA",
      isselected: false,
      firstselected: true,
      colr: "#ff7536",
      btnarr: data
    };
  }

  componentDidMount = async () => {
    let profile = await AsyncStorage.getItem('profile')
    console.log("Profile : ", profile);
    if (profile) {
      this.setState({
        profileImg: profile
      })
    }
    else {
      return null
    }

    // console.log("will mount profile : ", profile)
  }

  changecolor = () => {

  }

  chooseFile = () => {
    var options = {
      title: 'Select Image',
      customButtons: [
        { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, response => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        let source = response.uri;
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };
        console.log('User Response: ', source);
        AsyncStorage.setItem('profile', source);

        this.setState({
          profileImg: source,
        });
      }
    });
  };

  logout = async () => {
    // AsyncStorage.setItem("isloggedin", "0")
    await AsyncStorage.clear().then((respone) => {
      this.props.navigation.navigate('Login')
      console.log("Logout Response : ", respone);
    }).catch((error) => {
      console.log("Logout error :", error);
    })
  }

  buttonAction = (index) => {
    console.log(index)
    let arr = this.state.btnarr;
    arr = arr.map(item => {


      if (item.index == index) {
        item.isSelected = !item.isSelected
      } else {
        item.isSelected = false;
      }
       if(index == 5){
        // let data =[]
        // this.setState({btnarr : data })
        this.logout()
     } else if (index == 4){
      this.props.navigation.navigate('Setting')   
     }  else if(index == 0){
       this.props.navigation.navigate('Home')
     }  else if(index == 1){
      this.props.navigation.navigate('MyProfile')   
     }

     
    //  else if (item.index == index) {
    //     item.isSelected = !item.isSelected
    //   } else {
    //     item.isSelected = false;
    //   }
      return item
    });
    this.setState({ btnarr: arr });
  }

  render() {
    const { btnarr, colr } = this.state;
    // <Image source={{uri: 'https://organicthemes.com/demo/profile/files/2018/05/profile-pic.jpg'}} />
    return (
      <Container style={styles.container}>
        <View style={styles.header}>

          <View style={styles.headercontent}>
            <TouchableOpacity style={styles.profile} onPress={this.chooseFile}>
              {/* <IconEnts name="pencil" style={{marginLeft : 80 }} /> */}

              <Image
                style={styles.profile}
                // source={{uri: 'https://lh3.googleusercontent.com/-8ZVYQhA9FEU/AAAAAAAAAAI/AAAAAAAAJR8/I4QafmONFwA/s512-c/photo.jpg'}}
                source={{
                  // uri: 'data:image/jpeg;base64,' + this.state.filePath.data,
                  uri: this.state.profileImg
                }}
              />
            </TouchableOpacity>
            <View style={styles.userinfo}>
              <Text style={{ fontSize: 25, color: greyText, fontWeight: "bold" }}> {this.state.username} </Text>
              <Text style={{ color: greyText }}>{this.state.userlocation}</Text>
            </View>
          </View>
        </View>

        <Content style={styles.contentitem}>
          <View>
            {btnarr.map((item, index) => {
              return (
                <View key={index}>
                  {item.isSelected ?
                    <View style={styles.selecteView}>
                      <View style={{ backgroundColor: colr, width: wp("1%"), height: "100%" }} />
                      <TouchableOpacity onPress={() => this.buttonAction(item.index)} 
                        style={[styles.content, {marginLeft: wp("6%")}]}>
                        <Text style={{ color: colr, fontSize: hp("2.2%")  }}>{item.title}</Text>
                      </TouchableOpacity >
                    </View>
                    :
                    <TouchableOpacity onPress={() => this.buttonAction(item.index)} style={styles.content}>
                      <Text style={{ color: greyText, fontSize: hp("2.2%"), }}>{item.title}</Text>
                    </TouchableOpacity >
                  }
                </View>   
              )
            })
            }
          </View>
        </Content>


        {/*<TouchableOpacity style={styles.content}>
         
            <Text style={{color : greyText, fontSize :hp("2.4%")}}>Home</Text>
          </TouchableOpacity >
          <TouchableOpacity style={styles.content}>
            <Text style={{color : greyText , fontSize :hp("2.4%")}}>My Profile</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.content}>
            <Text style={{color : greyText , fontSize :hp("2.4%")}}>Manage Subscriptions</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.content} >
            <Text style={{color : greyText , fontSize :hp("2.4%")}}>My Tournaments</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.content} >
            <Text style={{color : greyText , fontSize :hp("2.4%")}}>Settings</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.content} onPress={this.logout}>
            <Text style={{color : greyText , fontSize :hp("2.4%")}}>Logout</Text>
          </TouchableOpacity>
        </Content> */}

      </Container>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,

    backgroundColor: drawerbackgraoundcolr
    // backgroundColor : "white",
    // justifyContent: "center",
    // alignItems: "center",
    // alignSelf : "center"
  },
  header: {
    height: hp("30%"),
    width: wp("100%"),

    // justifyContent: "center",
    // alignItems: "center",
    // alignSelf : "center"


    // marginTop: hp("5%"),   

  },
  headercontent: {
    marginLeft: wp("6%"),
    // marginLeft: wp("10%"),
    marginTop: wp("20%"),
  },
  profile: {

    borderWidth: 1,
    height: hp("10%"), width: hp("10%"), borderRadius: hp("5%")

    //  paddingLeft : 130
    // padding : wp("4%"),
    // marginBottom: wp("0.8%") 

  },
  content: {
    // justifyContent: "center",
    // alignItems: 'center',
    // backgroundColor: '#F5FCFF', 
    height: hp("7%"),
    marginLeft: wp("6%"),
    alignItems: "flex-start",
    justifyContent: "center",
    // flexDirection: "row",
  },
  selecteView: {
    height: hp("7%"),
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "#000", flexDirection: "row", width: "100%"
  },
  contentitem: {
    marginTop: hp("4%"),
    

  },
 
  userinfo: {
    height: hp("20%"),
    width: wp("90%"),
    marginTop: hp("2%")

    //  marginTop : 100
    //  paddingTop: wp("0.1%")
  }

})
/* <View style={{ height: 50, width: 50, borderRadius: 25, backgroundColor: "#00000040", position: "absolute", bottom: 0, width: "100%",
alignItems: 'center', justifyContent: 'flex-end' }}>
<Text style={{ color: 'white', fontSize: 16, marginBottom: 8 }}>{"Edit"}</Text>
</View> */