import React from 'react'
import { View,Image, Text,ScrollView, SafeAreaView, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen'
import { sideBarBg, secondaryColor } from '../Common/Colors';
function SideBar() {
    const tabArr = 
     [{Name : "Home", icon : "home"}, {Name : "Entertainers",icon : "headset"}, {Name : "Fans", icon : "md-people"},
     {Name : "Celebrity Wishes", icon : "human-handsdown"},
     {Name : "Event", icon : "grid"},
     {Name : "Creative Corner",icon : "code"},
     {Name : "Blogs",icon : "creative-commons-share"},
     {Name : "Paid Services",icon : "briefcase"},
     {Name : "Setting",icon : "settings"},
     {Name : "Invite",icon : "archive"}, 
     {Name : "Logout",icon : "logout"},
    ]
    return (
        <SafeAreaView style={{ flex : 1 , backgroundColor : sideBarBg }}>
            <View style={{ height : "15%", width : "100%" }}>
                <TouchableOpacity style={{borderColor : "green",  marginLeft : 15 ,height : 70, width : 70, borderRadius : 70 }}>
                    <Image style={{height : 70, width : 70, borderRadius : 70}} source={{uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRjVXybwwswQWwuHWko_d8yyrqqLOYBqlGWkkKNs-yDqqrb2YuQ&s"}} />
                    </TouchableOpacity>
                    <Text style={{ fontSize : 18,marginTop : 10, marginLeft : 20, color : secondaryColor }}>{"Piyush Bajpai"}</Text>
                </View>
                <ScrollView>
            {
                tabArr.map((item, index) => {
                    return (
                        <TouchableOpacity style={{flex : 1, width : "100%", height : 42, marginTop : 20 , flexDirection : "row"}}>
                           <Icon name={tabArr[index].icon} style={{ flex : 2 ,marginLeft : wp("4%") }}/>
                           {/* <Icon name={"md-people"} style={{ flex : 2 ,marginLeft : wp("4%") }}/> */}
                            <Text style={{flex : 10, fontSize : 18, color : secondaryColor }}>{item.Name}</Text>
                        </TouchableOpacity>
                    )
                })
            }
            </ScrollView>
        </SafeAreaView>
    )
}

export default SideBar
