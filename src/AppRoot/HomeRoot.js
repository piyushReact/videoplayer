//This is an example code for Bottom Navigation//
import React from 'react';
// import React from 'react';
import { createStackNavigator } from 'react-navigation-stack'
import { createAppContainer } from 'react-navigation'
import { createDrawerNavigator } from 'react-navigation-drawer'
import Groups from '../Screens/HomeRoot/Groups'
import Play from '../Screens/HomeRoot/Play'
import { bgColor, mainblue, transparent, colorPrimary } from '../Common/Colors'
import { widthPercentageToDP as wp, heightPercentageToDP as hp, heightPercentageToDP } from 'react-native-responsive-screen'
import SideBar from './SideBar';
import globalComment from '../Screens/HomeRoot/Comment';
import BottomTab from '../BottomTab';
import Profile from '../Screens/HomeRoot/Profile';
import { Button, Text, View, TouchableOpacity, StyleSheet, Image } from 'react-native';
import { createBottomTabNavigator } from 'react-navigation-tabs';
const HOME = require("../../assests/footerIcon.js/Home.png");
const HOMEA = require("../../assests/footerIcon.js/Home-grediant.png");
const CATEGORY = require("../../assests/footerIcon.js/Category.png");
const CATEGORYA = require("../../assests/footerIcon.js/Category-grediant.png");
const EXPOLRE = require("../../assests/footerIcon.js/Explore.png");
const EXPOLREA = require("../../assests/footerIcon.js/Explore-grediant.png");
const NOTIFICATIONS = require("../../assests/footerIcon.js/Notification.png");
const NOTIFICATIONSA = require("../../assests/footerIcon.js/Notification-Grediant.png");
const PROFILE = require("../../assests/footerIcon.js/Profile.png");
const PROFILEA = require("../../assests/footerIcon.js/Profile-grediant.png");

const HOME_STACK = createStackNavigator(
  {
    Groups: { screen: Groups },
    Profile: { screen: Profile },
    // DetaPlayils: { screen: Play },
  },
  {
    headerMode: 'none',
    defaultNavigationOptions: {
      
      
    },
  }
);
const CATEGORY_STACK = createStackNavigator(
  {
    Groups: { screen: Groups },
    // DetaPlayils: { screen: Play },
  },
  {
    headerMode: "none",
    defaultNavigationOptions: {

      //Header customization of the perticular Screen
      headerStyle: {
        backgroundColor: '#42f44b',
      },
      headerTintColor: '#FFFFFF',
      title: 'Home',
      //Header title
    },
  }
);
const EXPOLRE_STACK = createStackNavigator(
  {
    Groups: { screen: Groups },
    // DetaPlayils: { screen: Play },
  },
  {
    headerMode: "none",
    defaultNavigationOptions: {

      //Header customization of the perticular Screen
      headerStyle: {
        backgroundColor: '#42f44b',
      },
      headerTintColor: '#FFFFFF',
      title: 'Home',
      //Header title
    },
  }
);
const NOTIFICATIONS_STACK = createStackNavigator(
  {
    Groups: { screen: Groups },
    // DetaPlayils: { screen: Play },
  },
  {
    headerMode: "none",
    defaultNavigationOptions: {

      //Header customization of the perticular Screen
      headerStyle: {
        backgroundColor: '#42f44b',
      },
      headerTintColor: '#FFFFFF',
      title: 'Home',
      //Header title
    },
  }
);
const PROFILE_STACK = createStackNavigator(
  {
    DetaPlayils: { screen: Profile },
  },
  {
    headerMode: 'none',

    defaultNavigationOptions: {
      headerStyle: {
        backgroundColor: '#42f44b',
      },
      headerTintColor: '#FFFFFF',
      title: 'Settings',
    },
  }
);
const App = createBottomTabNavigator(
  {
    HOME: { screen: HOME_STACK },
    CATEGORY: { screen: CATEGORY_STACK },
    EXPOLRE: { screen: EXPOLRE_STACK },
    NOTIFICATIONS: { screen: NOTIFICATIONS_STACK },
    PROFILE: { screen: PROFILE_STACK },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        // let IconComponent = Ionicons;
        let iconName;

        if (routeName === 'HOME') {
          
          iconName = focused ? HOMEA : HOME
        }
         else if (routeName === 'CATEGORY') { 
          iconName = focused ? CATEGORYA : CATEGORY
        }
         else if (routeName === 'EXPOLRE') {
          iconName = focused ? EXPOLREA : EXPOLRE
        }
         else if (routeName === 'NOTIFICATIONS') {
          iconName = focused ? NOTIFICATIONSA : NOTIFICATIONS
        }
         else if (routeName === 'PROFILE') {
          iconName = focused ? PROFILEA : PROFILE
        }
        return <Image source={iconName} style={{ top : 2 }} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: colorPrimary,
      inactiveTintColor: 'white',
      // swipeEnabled: true,
      tabStyle: {
        marginBottom:-10,   //Padding 0 here
    },
      style: {
        backgroundColor: 'grey',

      }
    },
  }
);
export default createAppContainer(App);
