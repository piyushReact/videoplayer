import React, { Component } from 'react';
import { View, YellowBox } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createRootNavigator } from './router';
import AsyncStorage from './Common/AsyncStorage';
import { getApiWithoutHeader, getApi } from './Common/API';
// import { apiHeaders } from './Common/Utils';
import { API_GET_CATEGORIES, STATUS_SUCCESS } from './Common/APIConstant';
import { PrintLog } from './Components/PrintLog';
import Loader from './Components/Loader';
import SplashScreen from 'react-native-splash-screen'
import NetInfo from "@react-native-community/netinfo";
// import branch, { BranchEvent } from 'react-native-branch'
// import firebase from 'react-native-firebase';


export default class App extends Component {
    constructor(props) {
        super(props);
        YellowBox.ignoreWarnings(
            ['Remote debugger', "Async Storage has", "Required dispatch_sync", "Attempted to invoke", "Warning: componentDidMount",
                "Warning: WebView", "Unrecognized WebSocket", "Warning: ListView", "Warning: componentWillReceiveProps"]);
        this.state = {
            isLogin: false,
            checkedSignIn: false,
            isInternet: true
        }
    }

    async componentDidMount() {
        //  this.shareReferralLink();
        NetInfo.fetch().then(state => {
            console.log("Connection type", state.type);
            if (state.isConnected != true) {
           
                console.log("If Is connected? ", state.isConnected);

              
            } else {

                console.log("else Is connected?", state.isConnected);
            }
        });
        let isLogin = await AsyncStorage.read("isLogin"); 
        this.setState({ checkedSignIn: true, isLogin });
        SplashScreen.hide();
    }
    //1
//     async checkPermission() {
        
//         const enabled = await firebase.messaging().hasPermission();
//         if (enabled) {
//             this.getToken();
//         } else {
//             this.requestPermission();
//         }
//     }
//       //3
// async getToken() {
//     let fcmToken = await AsyncStorage.read('fcmToken');
//     if (!fcmToken) {
//         fcmToken = await firebase.messaging().getToken();
//         if (fcmToken) {
//             // user has a device token
//             PrintLog("FCM TOKEn ==> ",fcmToken)
//             // await AsyncStorage.setItem('fcmToken', fcmToken);
//         }
//     }
//   }
//     //2
// async requestPermission() {
//     try {
//         await firebase.messaging().requestPermission();
//         // User has authorised
//         this.getToken();
//     } catch (error) {
//         // User has rejected permissions
//         console.log('permission rejected');
//     }
//   }

    // async shareReferralLink() {
    //     let branchUniversalObject = await branch.createBranchUniversalObject('canonicalIdentifier', {
    //         locallyIndex: true,
    //         title: 'Cool Content!',
    //         contentDescription: 'Cool Content Description',
    //         contentMetadata: {
    //             ratingAverage: 4.2,
    //             customMetadata: {
    //                 userId: "125",
    //             }
    //         }
    //     })
    //     let linkProperties = {
    //         feature: 'share',
    //         channel: 'facebook',
    //         userId: "125",
    //     }

    //     let controlParams = {
    //         $desktop_url: ''
    //     }

    //     let { url } = await branchUniversalObject.generateShortUrl(linkProperties, controlParams)
    //     console.log("URL ", url)
    // }


    render() {
        const { isLogin, checkedSignIn } = this.state;

        if (!checkedSignIn) {
            return (
                <View style={{ flex: 1 }}>
                    <Loader loading={true} />
                </View>
            );
        }

        const Layout = createAppContainer(createRootNavigator(isLogin));

        return (
            <View style={{ flex: 1 }}>
                {<Layout />}
            </View>
        )
    }
}