import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import { Container, Header, Content, Footer, FooterTab, Button, Text } from 'native-base';
// let currentActiveIndexStatus = 0;
// const HOME = require("../assests/footerIcon.js/Home.png");
// const HOMEA = require("../assests/footerIcon.js/Home-grediant.png");
// const CATEGORY = require("../assests/footerIcon.js/Category.png");
// const CATEGORYA = require("../assests/footerIcon.js/Category-grediant.png");
// const EXPOLRE = require("../assests/footerIcon.js/Explore.png");
// const EXPOLREA = require("../assests/footerIcon.js/Explore-grediant.png");
// const NOTIFICATIONS = require("../assests/footerIcon.js/Notification.png");
// const NOTIFICATIONSA = require("../assests/footerIcon.js/Notification-Grediant.png");
// const PROFILE = require("../assests/footerIcon.js/Profile.png");
// const PROFILEA = require("../assests/footerIcon.js/Profile-Grediant.png");

export default class FooterTabs extends Component {
    // : require("../assests/footerIcon.js/Home-grediant.png")
    constructor(props) {
        super(props)
        this.state = {
            list: [{
                name: "Home",
                Icon: require("../assests/footerIcon.js/Home.png"),
                color: "green"
            }, {
                name: "CATEGORY",
                Icon: require("../assests/footerIcon.js/Category.png"),
                color: "red"
            }, {
                name: "EXPLORE",
                Icon: require("../assests/footerIcon.js/Explore.png"),
                color: "yellow"

            }, {
                name: "NOTIFICATION",
                Icon: require("../assests/footerIcon.js/Notification.png"),
                color: "green"

            }, {
                name: "PROFILE",
                Icon: require("../assests/footerIcon.js/Profile.png"),
                color: "orange"
            }],
            ListActiveIcon: 
            [
            // require("../assests/footerIcon.js/Home.png"),
            require("../assests/footerIcon.js/Home-grediant.png"),
            // require("../assests/footerIcon.js/Category.png"),
            require("../assests/footerIcon.js/Category-grediant.png"),
            // require("../assests/footerIcon.js/Explore.png"),
            require("../assests/footerIcon.js/Explore-grediant.png"),
            // require("../assests/footerIcon.js/Notification.png"),
            require("../assests/footerIcon.js/Notification-Grediant.png"),
            // require("../assests/footerIcon.js/Profile.png"),
            require("../assests/footerIcon.js/Profile-grediant.png")],
            currentActiveIndex: 0
        }
    }
    render() {
        const { list } = this.state
        return (
            <View style={{ flexDirection: "row", height: hp("12%"), width: wp("100%"), backgroundColor: "red", position: "absolute", bottom: 0 }}>
                {list.map((item, index) => {
                    return (
                        <TouchableOpacity
                            onPress={() => this.setState({ currentActiveIndex : index })}
                            style={{ justifyContent: "center", width: wp("20%"), alignItems: "center" }}>
                            {
                                this.state.currentActiveIndex == index ?
                                    <Image source={this.state.ListActiveIcon[this.state.currentActiveIndex]} />
                                    :
                                    <Image source={item.Icon} />
                            }
                            <Text numberOfLines={1} style={{ marginTop: hp("1%"), color : "white", fontWeight : "500" }}>{item.name}</Text>
                        </TouchableOpacity>
                    )
                })}
            </View>
        );
    }
}