import React, { Component } from 'react';
import { createSwitchNavigator } from 'react-navigation';
import LoginRoot from './AppRoot/LoginRoot';
import HomeRoot from './AppRoot/HomeRoot';

export const createRootNavigator = (isLogin = false) => {
    return createSwitchNavigator ( 
        {
            LoginRoot: { screen: LoginRoot() },
            HomeRoot: { screen: HomeRoot }
        },
        { 
            initialRouteName: (!isLogin) ? "LoginRoot" : "HomeRoot",
        }
    ) 
}