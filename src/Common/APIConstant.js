// Local server URL

export const BASE_URL               =   "";
export const BASE_URL_PUBLIC              =   ""

export const STATUS_SUCCESS         =   "200"        
 
export const API_VERIFY_USER              =   BASE_URL + "verifyphone-no"
export const API_REGISTER           =   BASE_URL + "create/account"
export const API_USER_UPDATE           =   BASE_URL + "userUpdate"
export const API_GET_DETAILS           =   BASE_URL + "getDetails"
export const API_ADD_GROUP           =   BASE_URL + "group/add"   
export const API_LIST_GROUP          =   BASE_URL + "listgroup" 
export const API_ADD_GROUP_ASSEST          =   BASE_URL + "asset/addAssets"
export const API_GET_NOTIFICATION         =   BASE_URL + "getnoti"
export const API_GET_ACCEPT_REQ         =   BASE_URL + "user/grpreq"
export const API_ADD_ASSETS           =  BASE_URL +'asset/addAssets'
export const API_ADD_IMAGE            = BASE_URL +'asset/addgallery'
export const API_GET_ASSET            = BASE_URL +'getAsset'
export const API_GET_MY_BOOKING = BASE_URL + 'booking/getlist'
export const API_BOOK_ASSETS = BASE_URL + 'create/reqreserve'
export const API_GETBOOKING_DETAILS = BASE_URL + 'asset' 
export const API_MANAGE_BOOKING_REQUEST = BASE_URL + 'booking/managereq'
export const API_DELETE_ASSETS = BASE_URL + 'asset/remove'
export const API_REMOVE_ASSETS = BASE_URL + 'asset/group/remove'
export const API_GET_GROUP_DETAILS = BASE_URL + 'group/details'
export const API_REMOVE_GROUP = BASE_URL + 'removegroup'
export const API_ASSIGN_MEMBER = BASE_URL + 'assign/member'
export const API_ASSIGN_ASSETS_OWNER = BASE_URL + 'assign/asset'
export const API_DEACTIVATE_ACCOUNT = BASE_URL + 'account/deactive'
export const API_GROUP_EDIT_MEMBER = BASE_URL + 'group/editmember'
export const API_REMOVE_PROFILE_IMAGE = BASE_URL + 'userUpdate/removeImage'
export const API_REMOVE_MEMBER = BASE_URL + 'removegroup/member'
 
export const API_GET_PENDING_BOOKING = BASE_URL + 'booking/getpendinglist'

export const API_DASHBOARD = BASE_URL + 'assets/utilization'
export const API_WEEK_ASSET = BASE_URL + 'getassets/utilization'
export const API_GRP_EDIT = BASE_URL + 'group/edit'
export const API_ASSET_EDIT = BASE_URL + 'assets/edit'
export const API_REMOVE_IMAGE = BASE_URL + 'assets/delimage'
export const API_Add_ASSET = BASE_URL + 'group/addasset'
export const API_INVITE = BASE_URL + 'users/invite'
export const API_PUSH_NOTI = BASE_URL + 'users/updatepush'



export const API_FORGOT_PASS_REQ    =   BASE_URL + "create/password/reset/request"
export const API_TOKEN_CHECK        =   BASE_URL + "password/find/"
export const API_RESET_PASS         =   BASE_URL + "reset/password"
export const API_LOGOUT             =   BASE_URL + "logout"
export const API_VERIFY_OTP         =   BASE_URL + "verify/phone/number"
export const API_RESEND_OTP         =   BASE_URL + "resend/otp"
export const API_SKIP_PASS          =   BASE_URL + "skip/forgot/password"

export const API_GET_CATEGORIES     =   BASE_URL + "get/courses/categories"
export const API_COURSE_POLICIES    =   BASE_URL + "all/course/policies"
export const API_PLACE_POLICIES     =   BASE_URL + "all/place/policies"
export const API_GET_NO_PROFILES    =   BASE_URL + "get/number/of/profiles"

export const API_FILTER_TRAINER     =   BASE_URL + "get/filtered/data/for/trainers"
export const API_FILTER_COURSES_IN  =   BASE_URL + "get/filtered/data/for/courses/for/loggedin/user"
export const API_FILTER_COURSES     =   BASE_URL + "get/filtered/data/for/courses"
export const API_LEARNER_ACTIONS    =   BASE_URL + "do/enrolled/follwed/intersted"
export const API_FOLLOW             =   BASE_URL + "do/follow"

//Home
export const API_PUBLIC_COURSES     =   BASE_URL + "get/all/public/courses"
export const API_PRIVATE_COURSES    =   BASE_URL + "get/all/private/courses"
export const API_ALL_TRAINERS       =   BASE_URL + "get/all/trainers"
export const API_PRIVATE_TRAINERS   =   BASE_URL + "get/all/trainers/for/logged/in/user"
export const API_ALL_LEARNERS       =   BASE_URL + "all/learners"
export const API_TIMELINE_DATA      =   BASE_URL + "get/timeline/data"
export const API_GET_PLACES         =   BASE_URL + "get/all/private/places"
export const API_COURSE_DETAILS     =   BASE_URL + "get/course/detail"
export const API_P_COURSE_DETAILS   =   BASE_URL + "get/possible/course/detail"
export const API_TRAINER_DETAILS    =   BASE_URL + "get/trainer/detail"
export const API_PLACE_DETAILS      =   BASE_URL + "get/place/detail"
export const API_PUBLIC_P_COURSES   =   BASE_URL + "get/all/public/possible/courses"
export const API_PRIVATE_P_COURSES  =   BASE_URL + "get/all/private/possible/courses"



export const api_header = {
    headers: {
        'Content-Type': 'application/json',
    }
}