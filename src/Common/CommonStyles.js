import { StyleSheet, Platform, StatusBar, Dimensions } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { colorPrimary, white, black, grey, borderColor, textColor } from './Colors';
import { STATUSBAR_HEIGHT } from './Constants';

let statusBarHeight = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const { height, width } = Dimensions.get("window");

export default (Styles = {
  container: {
    flex: 1, alignItems: 'center', width: '100%'
  },
  parentView: {
    flex: 1, width: '100%', backgroundColor: white, marginTop: -statusBarHeight - 1
  },
  headerStyle: {
    backgroundColor: colorPrimary,
    paddingLeft: 0, paddingRight: 0,
    shadowColor: 'grey',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8, shadowRadius: 2,
    elevation: 10, position: 'relative',
    borderTopWidth: 0, zIndex: 4,
    marginTop: Platform.OS === 'ios' ? 0 : statusBarHeight,
  },
  headerStyleWithHeight: {
    width: '100%',
    height: height * 28 / 100
  },
  headerViewStyle: {
    flexDirection: 'row', width: '100%', alignItems: 'center',
    justifyContent: 'center', backgroundColor: colorPrimary
  },
  textHeaderStyle: {
    fontSize: 18, color: black, textAlign: 'center'
  },
  backButtonContainer: {
    position: "absolute",
    top: Platform.OS === "ios" ? hp("3%") : hp("2%"),
    left: wp("3%"),
  },
  rightSideContainer: {
    position: "absolute",
    top: Platform.OS === "ios" ? hp("6%") : hp("4%"),
    right: wp("3%"),
  },
  backIconStyle: {
    color: 'grey',
    fontSize: hp("3%"),
  },
  divider: {
    marginVertical: hp("1%"),
    backgroundColor: borderColor,
    width: wp("100%"),
    height: 0.8
  },
  tabView: {
    width: wp("100%"),
    height: 52,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colorPrimary,
    flexDirection: 'row',
  },
  tabSelected: {
    width: wp("100%") / 2,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: hp("0.3%"),
    borderBottomColor: white,
  },
  tabUnselected: {
    width: wp("100%") / 2,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: hp("0.4%"),
    borderBottomColor: 'transparent',
  },
  textTab: {
    color: white,
    fontSize: hp("2%"),
    textAlign: "center"
  },
  textTabSelected: {
    color: white,
    fontWeight: 'bold',
    fontSize: hp("2%"),
    textAlign: "center"
  },
  followViewStyle: {
    height: 28, width: wp("24%"), borderRadius: 14, borderWidth: 1, borderColor: white,
    justifyContent: 'center', alignItems: "center", backgroundColor: 'transparent'
  },
  followTextStyle: {
    color: white, fontSize: hp("1.8%")
  },
  viewEnrolled: {
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: borderColor,
    width: wp("28%"),
    height: hp("4%"),
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  enrolledText: {
    color: textColor,
    fontSize: hp("2%"),
    letterSpacing: 0.4
  },
  typeSelected: {
    borderWidth: 1, borderColor: colorPrimary, padding: 8, width: '40%',
    alignItems: 'center', justifyContent: 'center'
  },
  typeUnselected: {
    borderWidth: 1, borderColor: grey, padding: 8, width: '40%',
    alignItems: 'center', justifyContent: 'center'
  },
  textSelected: {
    color: colorPrimary,
    fontSize: hp("1.8%")
  },
  textUnselected: {
    color: textColor,
    fontSize: hp("1.8%")
  },
  typeContainer: {
    flexDirection: 'row', width: wp("100%"), alignSelf: 'center', margin: hp("1.4%"),
    alignItems: 'center', justifyContent: 'center'
  }
});