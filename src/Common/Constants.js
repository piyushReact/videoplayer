export const txt_full_name = "Full Name";

export const Profile_Image_Url = "";

export const txt_email = "E-mail";
export const txt_mobile = "Mobile";
export const txt_password = "Password";
export const txt_new_password = "New Password";
export const txt_confirm_password = "Confirm Password";
export const txt_date_of_birth = "Date of Birth";
export const txt_login = "Login";
export const txt_sign_up = "Sign Up";
export const txt_send = "Send";
export const txt_send_otp = "Send OTP";
export const txt_sign_up_account = "Already a User ?";
export const txt_already_member_login = "Already Member Log In";
export const txt_already_member = "Already Member Login";
export const txt_logo = "Logo";
export const txt_new_member = "New Member";
export const txt_signout = "Signout";
export const txt_name = "Name";
export const txt_continue_as_guest = "Continue as Guest";
export const txt_login_with_email = "Login with Mobile Number";
export const txt_or = "Or";
export const txt_forgot_pass = "Forgot Password";
export const txt_reset_pass = "Reset Password";
export const txt_update_pass = "Update Password";
export const txt_enter_otp = "Enter OTP";
export const txt_resend = "Resend OTP";
export const txt_verify = "Verify";
export const txt_remember_me = "Remember me";
export const txt_address = "Address";
export const txt_city = "City";
export const txt_state = "State";
export const txt_country = "Country";
export const txt_place_location = "Place Location";
export const txt_pick_location = "Pick Location";
export const txt_place_size = "Place Size";

export const txt_timeline = "Timeline";
export const txt_courses = "Courses";
export const txt_possible_courses = "Possible Courses";
export const txt_trainers = "Trainers";
export const txt_places = "Places";
export const txt_about_me = "About Me";
export const txt_about_trainer = "About Trainer";
export const txt_about_provider = "About Provider";
export const txt_my_courses = "My Courses";
export const txt_connections = "Connections";
export const txt_learner_profile = "Learner Profile";
export const txt_trainer_profile = "Trainer Profile";
export const txt_place_profile = "Place Profile";
export const txt_place_provider = "Place Provider";
export const txt_filter = "Filter";
export const txt_location = "Location";

export const txt_hide_profile = "Hide Profile";
export const txt_save = "Save";
export const txt_save_continue = "Save & Continue";
export const txt_delete_acc = "Delete Account";
export const txt_follow = "Follow";
export const txt_unfollow = "Unfollow";
export const txt_followed = "Followed";
export const txt_unfollowed = "Unfollowed";
export const txt_interested = "Interested";
export const txt_uninterested = "Uninterested";
export const txt_enrolled = "Enrolled In";

export const internet_warning = 'Please check your internet connection';
export const enter_valid_credential = 'Please enter valid credentials';
export const enter_email = 'Please enter email';
export const enter_valid_email = 'Please enter valid email';
export const enter_name = "Please enter your Name";
export const enter_phone_number = "Please enter mobile number";
export const enter_valid_number = "Please enter valid Phone number";
export const enter_password = "Please enter password";
export const password_not_match = "Password did not match";
export const password_length_short = "Password length is short";
export const select_date_of_birth = "Please select Date of Birth";
export const enter_otp = "Please enter the OTP";
export const no_records_found = "No records found.";
export const enter_all_details = "Please enter all the details.";
export const login_first = "Please login first.";

export const txt_course_background = "Course Background";
export const txt_course_title = "Course Title";
export const txt_course_type = "Course Type";
export const txt_course_fee = "Course Fee Per Person";
export const txt_courseFee = "NOK: 0";
export const txt_learner = "No. of learners";
export const txt_max_learners = "Max. No of learners";
export const txt_min_learners = "Min. No of learners";
export const txt_social = "Share This Course With Social Network";
export const txt_add_email = "Add Multiple Email";
export const txt_add_leaners = "Add Learners";
export const txt_corse_status = "Course Status";
export const txt_course_fee_title = "Course Fee Refund Policy";
export const txt_add_multiple_image = "Add Multiple Image";
export const txt_invite = "Invite & Promote Course";
export const txt_multiple_email = "muthura@gmail.com, Email here...";
export const txt_invite_user = "Invite connection user";
export const txt_course_schedule = "Schedule a course";
export const txt_payment_type = "Payment Type";
export const txt_hide_course = "Hide Course";

export const txt_place_background = "Place Background";
export const txt_place_title = "Place Title";
export const txt_rent_refund_policy = "Rent Refund Policy";

export const txt_add_image = "Add Image";
export const txt_choose_gallery = "Choose From Gallery";
export const txt_take_photo = "Take Photo";
export const txt_cancel = "Cancel";

export const txt_card_number = "Card Number";
export const txt_card_name = "Card Name";
export const txt_exp_month = "Expiration Month";
export const txt_exp_year = "Expiration Year";
export const txt_security_code = "Security Code";
export const txt_paypal_user_id = "PayPal User ID";


export const txt_country_code = "+91";
// export const txt_country_code1 = "+47"


export const txt_dummy = "Lorem epsum Lorem epsum Lorem epsum Lorem epsum Lorem epsum";


import { Platform, StatusBar, Dimensions } from 'react-native';

export const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;