export const colorPrimary1 = '#674172';
export const colorPrimary = 'orange';
export const primaryColor = '#674172';
export const sideBarBg = '#674172';
export const secondaryColor = 'orange';

export const bgColor = '#f0f0f0';
export const greyText = '#cccccc';
export const grey = 'grey';
export const textColor = '#333333';
export const turquoise = '#a1ddd1';
export const lightGreen = '#87d37b';
export const pink = '#ddc6e0';
export const yellow = '#e9d461';
export const filterBackground = '#f5f5f5';
export const settingBackground = '#494949';
export const notification = '#e44e3f';
export const priceColor = '#9fcda6';
export const main = '#0ddfc6';
export const mainblue = '#0e65d2';

export const black = '#000';
export const white = '#fff';
export const borderColor = '#C3CBCE';
export const shadowColor = '#67417280';
export const transparent = 'transparent';