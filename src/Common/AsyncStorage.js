// import { AsyncStorage } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

module.exports = {
    save: async function (key, data) {
        try {
            await AsyncStorage.setItem(key, JSON.stringify(data));
            return true;
        } catch (error) {
            return false;
        }
    },

    read: async function (key) {
        try {
            let data = await AsyncStorage.getItem(key);
            if (data !== null) {
                return JSON.parse(data);
            } else {
                return false;
            }
        } catch (error) {
            return false;
        }
    },

    empty: function (key) {
        try {
            AsyncStorage.setItem(key, '');
            return true;
        } catch (error) {
            return false;
        }
    },

    flushQuestionKeys: function () {
        AsyncStorage.getAllKeys().then((keys) => {
            const questionKeys = keys.filter((key) => {
                return key.includes("question-");
            });

            questionKeys.map((key) => {
                this.save(key, false);
            });
        });
    }
}