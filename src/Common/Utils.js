import React from 'react';
import { Platform, Alert, NetInfo, ActionSheetIOS, Linking } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import AStorage from './AsyncStorage';
import { PrintLog } from '../Components/PrintLog';
// import ImagePicker from 'react-native-image-crop-picker';
// import ImagePickerPrompt from "react-native-image-picker";
import { txt_take_photo, txt_choose_gallery, txt_add_image, txt_cancel } from './Constants';
import Permissions from 'react-native-permissions';
import AndroidOpenSettings from 'react-native-android-open-settings';

var Utils = {
    emailValidation: (emailId) => {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(emailId) !== false) {
            return true;
        }
        return false;
    },

    urlValidation: (url) => {
        let isValidUrl = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.​\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[​6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1​,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00​a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u​00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;
        if (isValidUrl.test(url)) {
            return true;
        }
        return false;
    },

    isNetworkConnected: async () => {
        await NetInfo.isConnected.fetch().then(isConnected => {
            return isConnected
        });
    },

    alertMessage: (message) => {
        Alert.alert(
            '', message, [{ text: 'OK', },
            ], { cancelable: false }
        )
    },

    isEmpty: (text) => {
        if (text.toString().trim() === "") {
            return true;
        } else {
            return false;
        }
    },

    apiHeaders: (token) => {
        const apiHeaders = {
            headers: {
                // 'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token,
            }
        }
        return apiHeaders
    },

    onChangeNumber: (text) => {
        let newText = '';
        let numbers = '0123456789';
        for (var i = 0; i < text.length; i++) {
            if (numbers.indexOf(text[i]) > -1) {
                newText = newText + text[i];
            }
        }
        return newText
    },

    onChangeCharacter: (text) => {
        let newText = '';
        let ch = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ';
        for (var i = 0; i < text.length; i++) {
            if (ch.indexOf(text[i]) > -1) {
                newText = newText + text[i];
            }
        }
        return newText
    },

    testID: (id) => {
        return Platform.OS === 'android' ?
            { accessible: true, accessibilityLabel: id } :
            { testID: id }
    },

    // showImagePicker: async (circle, type, successPicker) => {
    //     let width = 500, height = 500;
    //     if (type === "profile") {
    //         width = 240;
    //         height = 240;
    //     } else if (type === "background") {
    //         width = 828;
    //         height = 640;
    //     }
    //     let obj = {
    //         width: width,
    //         height: height,
    //         cropping: true,
    //         cropperCircleOverlay: circle,
    //         compressImageMaxWidth: 828,
    //         compressImageMaxHeight: 640,
    //         compressImageQuality: 0.5,
    //         compressVideoPreset: 'MediumQuality',
    //         includeBase64: true,
    //         includeExif: true,
    //     }
    //     if (Platform.OS === 'ios') {
    //         ActionSheetIOS.showActionSheetWithOptions({
    //             options: [txt_choose_gallery, txt_take_photo, txt_cancel],
    //             title: txt_add_image,
    //             cancelButtonIndex: 2
    //         }, (result) => {
    //             if (result == 0) {
    //                 ImagePicker.openPicker(obj).then((image) => {
    //                     let imageData = {
    //                         uri: image.path, width: image.width, height: image.height,
    //                         name: image.filename === undefined ? "image" : image.filename, type: image.mime
    //                     }
    //                     // PrintLog("Image Data : ", imageData);
    //                     // let base64 = `data:${image.mime};base64,`+ image.data;
    //                     if (image.path) {
    //                         successPicker(image.path, imageData);
    //                     }
    //                 }).catch((response) => {
    //                     PrintLog("Gallery error : ", response);
    //                     if (response.code !== "E_PICKER_CANCELLED") {
    //                         Linking.canOpenURL('app-settings:').then(supported => {
    //                             console.log(`Settings url works`)
    //                             Linking.openURL('app-settings:')
    //                         }).catch(error => {
    //                             console.log(`An error has occured: ${error}`)
    //                         })
    //                     }
    //                 });
    //             } else if (result == 1) {
    //                 ImagePicker.openCamera(obj).then((image) => {
    //                     PrintLog("Utils Image : ", image);
    //                     let imageData = {
    //                         uri: image.path, width: image.width, height: image.height,
    //                         name: image.filename === undefined || image.filename === null ? "image" : image.filename, type: image.mime
    //                     }
    //                     if (image.path) {
    //                         successPicker(image.path, imageData);
    //                     }
    //                 }).catch((response) => {
    //                     PrintLog("Camera error : ", response);
    //                     if (response.code !== "E_PICKER_CANCELLED") {
    //                         Linking.canOpenURL('app-settings:').then(supported => {
    //                             console.log(`Settings url works`)
    //                             Linking.openURL('app-settings:')
    //                         }).catch(error => {
    //                             console.log(`An error has occured: ${error}`)
    //                         })

    //                     }
    //                 });
    //             }
    //         });
    //     } else {
    //         ImagePickerPrompt.showImagePicker({
    //             title: txt_add_image,
    //             takePhotoButtonTitle: null,
    //             chooseFromLibraryButtonTitle: null,
    //             cancelButtonTitle: txt_cancel,
    //             customButtons: [{ name: "0", title: txt_choose_gallery }, { name: "1", title: txt_take_photo }],
    //         }, (response) => {
    //             if (response.customButton == "0") {
    //                 ImagePicker.openPicker(obj).then((image) => {
    //                     let imageData = {
    //                         uri: image.path, width: image.width, height: image.height,
    //                         name: image.filename === undefined ? "image" : image.filename, type: image.mime
    //                     }
    //                     if (image.path) {
    //                         successPicker(image.path, imageData);
    //                     }
    //                 }).catch((response) => {
    //                     PrintLog("Gallery error : ", response);
    //                     if (response.code !== "E_PICKER_CANCELLED") {
    //                         AndroidOpenSettings.appDetailsSettings();
    //                     }
    //                 });
    //             } else if (response.customButton == "1") {
    //                 ImagePicker.openCamera(obj).then((image) => {
    //                     PrintLog("Response : ", image)
    //                     let imageData = {
    //                         uri: image.path, width: image.width, height: image.height,
    //                         name: image.filename === undefined ? "image" : image.filename, type: image.mime
    //                     }
    //                     if (image.path) {
    //                         successPicker(image.path, imageData);
    //                     }
    //                 }).catch((response) => {
    //                     PrintLog("Camera error : ", JSON.stringify(response));
    //                     if (response.code !== "E_PICKER_CANCELLED") {
    //                         AndroidOpenSettings.appDetailsSettings();
    //                     }
    //                 });
    //             }
    //         })
    //     }
    // },

    logout: () => {
        AsyncStorage.setItem('isLogin', JSON.stringify(false)); //awali
        AsyncStorage.setItem('AuthToken', ''); //awali
        AsyncStorage.setItem('token', ''); //awali
        AsyncStorage.setItem('name', '');
        AsyncStorage.setItem('loginData', '');
    
        AStorage.flushQuestionKeys();
      
    }
};

module.exports = Utils;