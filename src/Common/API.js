import axios from "axios";
import axiosRetryInterceptor from "axios-retry-interceptor";
import { PrintLog } from "../Components/PrintLog";


const axiosInstance = axios.create();

axiosRetryInterceptor(axiosInstance, {
  maxAttempts: 100,
  waitTime: 10000,
  errorCodes: []
});

let apiHeaders = {
  headers: {
    "Accept": "application/json", 
    "Content-Type": "application/json"
  }
}; 

var ApiCalling = {
  getApi: async (url, header, successCallback, errorCallback) => {
    PrintLog("getApi url=>", url);
    axiosInstance
      .get(url, header)
      .then(response => {
        PrintLog("GET api Response >> ", response);
        successCallback(response);
      })
      .catch(error => {
        errorCallback(error);
      });
  },

  postApi: async (urlStr, params, header, successCallback, errorCallback) => {
    axiosInstance
      .post(urlStr, params, header)
      .then(response => {
        PrintLog("POST api Response >> ", response);
        successCallback(response);
      })
      .catch(error => {
        errorCallback(error);
      });
  },

  deleteApi: async (urlStr, header, successCallback, errorCallback) => {
    axiosInstance
      .delete(urlStr , header)
      .then(response => {
        PrintLog("delete api Response >> ", response);
        successCallback(response);
      })
      .catch(error => {
        errorCallback(error);
      });
  },


  getApiWithoutHeader: async (urlStr, successCallback, errorCallback) => {
    axiosInstance
      .get(urlStr)
      .then(response => {
        successCallback(response);
      })
      .catch(error => {
        errorCallback(error);
      });
  },
 
  postApiWithoutHeader: async (
    urlStr,
    params,
    successCallback,
    errorCallback
  ) => {
    axiosInstance
      .post(urlStr, params)
      .then(response => {
        successCallback(response);
      })
      .catch(error => {
        console.log("errrrrr ==> ",error);
        errorCallback(error);
      });
  }
};

module.exports = ApiCalling;