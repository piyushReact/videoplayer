import React from "react";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Platform, Image, ImageBackground } from 'react-native';
import { Header } from 'native-base';
import { colorPrimary, white, shadowColor } from "../Common/Colors";
import { STATUSBAR_HEIGHT, txt_hide_profile } from "../Common/Constants";
import { Switch } from 'react-native-switch';
import { PrintLog } from "./PrintLog";
import IconAntDesign from 'react-native-vector-icons/AntDesign';

export default class ImageHeader extends React.Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            disable: false,
        }
    }

    componentDidMount() {
        this.setState({ data: this.props.data });
        if (this.props.data.hide_profile !== undefined) {
            setTimeout(() => {
                this.setState({ disable: true });
            }, 100);
        }
        // PrintLog("componentDidMount : ", this.props.data.hide_profile);
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.data.hide_profile !== undefined && this.state.data.hide_profile !== nextProps.data.hide_profile) {
            setTimeout(() => {
                this.setState({ disable: false });
            }, 100);
            // PrintLog("componentWillReceiveProps IF : ", nextProps.data.hide_profile);
        } else {
            // PrintLog("componentWillReceiveProps Else : ", nextProps.data.hide_profile);
            setTimeout(() => {
                this.setState({ disable: true });
            }, 100);
        }
        setTimeout(() => {
            this.setState({ data: nextProps.data, disable: true });
        }, 200);
    }

    render() {
        const { backAction, drawerAction, editButton, editAction } = this.props;
        const { disable, data } = this.state;
        let size = hp("10%");
        let top = Platform.OS === "ios" ? hp("5%") : hp("2%")
        return (
            <ImageBackground style={{ height: hp("32%"), width: wp("100%") }}
                source={{ uri: data.background_img }}>
                <View style={{ width: '100%', height: '100%', backgroundColor: shadowColor }}>
                    <View style={{
                        width: '100%', marginTop: top, flexDirection: 'row', alignItems: 'center',
                        justifyContent: 'space-between', paddingLeft: wp("4%"), paddingRight: wp("4%")
                    }}>
                        <TouchableOpacity onPress={() => backAction()} style={{}} >
                            <View style={{ padding: 8 }}>
                                <Image source={require("../../assets/images/back-arrow.png")} />
                            </View>
                        </TouchableOpacity>
                        {editButton ?
                            <View style={{ alignItems: 'center', justifyContent: "center", flexDirection: 'row' }}>
                                <TouchableOpacity style={{}}
                                    onPress={() => editAction()}>
                                    <View style={{ padding: 8 }} >
                                        <IconAntDesign name={"edit"} style={{ color: white, fontSize: hp("3.4%") }} />
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{}}
                                    onPress={() => drawerAction()}>
                                    <View style={{ padding: 8 }} >
                                        <Image source={require("../../assets/images/push.png")} />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            :
                            <TouchableOpacity style={{}}
                                onPress={() => drawerAction()}>
                                <View style={{ padding: 8 }} >
                                    <Image source={require("../../assets/images/push.png")} />
                                </View>
                            </TouchableOpacity>
                        }
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: hp("2%"), marginLeft: hp("2%") }}>
                        <Image source={{ uri: data.profile_img }}
                            style={{ width: size, height: size, borderRadius: size / 2 }} />
                        <View style={{ margin: hp("2%") }} >
                            <Text style={{ color: white, fontSize: hp("2.6%"), fontWeight: '500' }}>{data.name}</Text>
                            <Text style={{ color: white, fontSize: hp("2%") }}>{data.city}</Text>
                        </View>
                    </View>
                    {/* <View style={{
                        position: 'absolute', bottom: hp("2%"), right: wp("4%"), alignItems: 'center',
                        flexDirection: 'row'
                    }}>
                        <Text style={{ color: white, fontSize: hp("2%") }}>{txt_hide_profile}</Text>
                        <View style={{ marginLeft: wp("2%") }}>
                            <Switch
                                value={data.hide_profile === "1" ? true : false}
                                disabled={disable}
                                circleSize={28}
                                barHeight={30}
                                circleBorderWidth={0}
                                backgroundActive={colorPrimary}
                                backgroundInactive={'grey'}
                                circleActiveColor={'#FFF'}
                                circleInActiveColor={'#FFF'}
                            />
                        </View>
                    </View> */}
                </View>
            </ImageBackground>
        );
    }
}
const { height, width } = Dimensions.get("window");
const styles = StyleSheet.create({
});
