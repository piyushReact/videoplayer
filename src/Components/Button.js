import React from 'react';
import { StyleSheet, Dimensions, Text, View, TouchableOpacity } from 'react-native';
import { colorPrimary, borderColor, textColor } from '../Common/Colors';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default class Button extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    render() {

        const { width } = Dimensions.get('window')

        const styles = StyleSheet.create({
            container: {
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: this.props.color ? this.props.color : colorPrimary,
                paddingTop: 12,
                paddingBottom: 12,
                width: this.props.width ? this.props.width : (width - 150),
                height: this.props.height ? this.props.height : 48,
                borderRadius: this.props.radius ? this.props.radius : 24,
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 12,
                },
                shadowOpacity: this.props.noShadow ? 0 : 0.28,
                shadowRadius: 16.00,
                elevation: this.props.noShadow ? 0 : 24,
            }, 
            containerLow: {
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: this.props.color ? this.props.color : colorPrimary,
                paddingTop: 12,
                paddingBottom: 12,
                width: this.props.width ? this.props.width : (width - 150),
                height: this.props.height ? this.props.height : 48,
                borderRadius: 24,
                shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 8,
                },
                shadowOpacity: this.props.noShadow ? 0 : 0.1,
                shadowRadius: 12.00,
                elevation: 24,
            },
            title: {
                color: '#FFF',
                fontWeight: '700',
                fontSize: 14,
            },
            containerSimple: {
                backgroundColor: this.props.color ? this.props.color : '#FFF',
                borderWidth: 1,
                borderColor: borderColor,
                width: this.props.width ? this.props.width : wp("80%"),
                height: this.props.height ? this.props.height : 48,
                borderRadius: this.props.radius ? this.props.radius : 5,
                justifyContent: 'center',
                alignItems: 'center',
            },
            containerNext: {
                backgroundColor: this.props.color ? this.props.color : colorPrimary,
                width: this.props.width ? this.props.width : 120,
                height: this.props.height ? this.props.height : 32,
                borderRadius: this.props.radius ? this.props.radius : 16,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'row'
            },
            titleSimple: {
                fontWeight : "600",
                // fontSize : 90,
                color: this.props.textColor ? this.props.textColor : colorPrimary,
                fontSize: this.props.fontSize ? this.props.fontSize : 18,
            },
            titleBlack: {
                color: this.props.textColor ? this.props.textColor : textColor,
                fontSize: this.props.fontSize ? this.props.fontSize : hp("2%"),
                letterSpacing: 0.4
            }
        });

        if (this.props.simple) {
            return (
                <TouchableOpacity onPress={this.props.onPress}>
                    <View style={styles.containerSimple}>
                        <Text style={styles.titleSimple}>{this.props.title}</Text>
                    </View>
                </TouchableOpacity>
            )
        } else if (this.props.isWhite) {
                return (<TouchableOpacity style={{ marginTop: this.props.top ? this.props.top : 0 }} 
                    onPress={this.props.onPress}>
                <View style={styles.containerSimple}>
                    <Text style={styles.titleBlack}>{this.props.title}</Text>
                </View>
            </TouchableOpacity>)
        } else {
            return (
                <TouchableOpacity onPress={this.props.onPress}>
                    <View style={this.props.lowShadow ? styles.containerLow : styles.container}>
                        <Text style={styles.title}>{this.props.title}</Text>
                    </View>
                </TouchableOpacity>
            )
        }
    }
}

