import React, { Component } from 'react';
import { Text, View, TextInput, TouchableWithoutFeedback, TouchableOpacity, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import reject from 'lodash/reject';
import { textColor, colorPrimary, borderColor, transparent } from '../Common/Colors';
import { PrintLog } from './PrintLog';
import IconEntypo from 'react-native-vector-icons/Entypo';
import { Input } from 'react-native-elements';

export default class MultiSelection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selector: false,
            userInput: '',
            selectedItems: [],
            showError: false
        };
    }


    componentDidMount() {
        this.setState({ selectedItems: this.props.selectedData })
    }

    componentWillReceiveProps = (nextProps) => {
        if (nextProps.selectedData !== undefined && nextProps.selectedData.length > 0) {
            this.setState({
                selectedItems: nextProps.selectedData
            })
        }
    }

    validateInput = () => {
        const { userInput, selectedItems } = this.state;
        if ((selectedItems !== undefined && selectedItems.length <= 0)) {
            this.setState({ showError: true });
        } else {
            this.setState({ showError: false });
        }

    }

    _addItem = () => {
        // let newItem = this.state.item;
        const { userInput, selectedItems } = this.state;
        let newSelectedItems = selectedItems;
        if (userInput) {
            newSelectedItems.push(userInput);
        }
        this.setState({ userInput: '', selectedItems: newSelectedItems })
        this.props.updateSelected(newSelectedItems);
    };

    _removeItem = (index) => {
        const { selectedItems } = this.state;
        let list = reject(
            selectedItems,
            singleItem => selectedItems[index] === singleItem
        );
        this.setState({ selectedItems: list });
        this.props.updateSelected(list);
    }

    render() {
        const { userInput, selectedItems, showError } = this.state;
        const { placeholder, noError } = this.props;
        return (
            <View style={styles.container}>

                <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                    {selectedItems.length > 0 &&
                        selectedItems.map((item, index) => {
                            let wdt = item.length * 8 + 52;
                            if (wdt > wp("32%")) {
                                wdt = wp("32%")
                            }
                            return (
                                <View key={index}
                                    style={[styles.selectedItemStyle, { width: wdt, }]}>
                                    <Text onPress={() => alert(item)} style={[styles.textStyle, {}]} numberOfLines={1}>{item}</Text>
                                    <TouchableOpacity onPress={() => this._removeItem(index)} >
                                        <IconEntypo name="cross" style={{ fontSize: 20, color: colorPrimary, }} />
                                    </TouchableOpacity>
                                </View>
                            )
                        })
                    }
                </View>

                <View style={[styles.inputContainer, { borderColor: placeholder ? transparent : borderColor }]}>
                    <Input
                        containerStyle={{ marginLeft: 0, marginRight: 0, paddingLeft: 0, paddingRight: 0, width: '100%' }}
                        autoCapitalize='none'
                        autoCorrect={false}
                        keyboardType={'default'}
                        selectionColor={colorPrimary}
                        placeholder={placeholder ? placeholder : "Add multiple keywords"}
                        placeholderTextColor={textColor}
                        onSubmitEditing={this._addItem}
                        returnKeyType={'done'}
                        returnKeyLabel={'Done'}
                        value={userInput}
                        underlineColorAndroid="transparent"
                        onBlur={this.validateInput}
                        onChangeText={(userInput) => this.setState({ userInput })}
                        inputContainerStyle={{ borderBottomColor: (showError && noError !== "true") ? 'red' : borderColor }}
                        inputStyle={[styles.input, { marginBottom: -8 }]}
                        errorStyle={{ color: 'red' }}
                        errorMessage={(showError && noError !== "true") ? 'PLEASE PRESS SELECTION AFTER DETAIL' : ''}
                    />

                    {/* <TextInput
                        autoCapitalize='none'
                        autoCorrect={false}
                        keyboardType={'default'}
                        maxLength={email ? 32 : 16}
                        selectionColor={colorPrimary}
                        placeholder={placeholder ? placeholder : "Add multiple keywords"}
                        placeholderTextColor={textColor}
                        onSubmitEditing={this._addItem}
                        returnKeyType={'done'}
                        returnKeyLabel={'Done'}
                        value={userInput}
                        underlineColorAndroid="transparent"
                        onBlur={this.validate}
                        onChangeText={(userInput) => this.setState({ userInput })}
                        style={styles.input} /> */}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {

    },
    inputContainer: {
        height: hp('6.5%'),
        width: '100%',
        // borderBottomColor: borderColor,
        // borderBottomWidth: 0.8
    },
    input: {
        height: hp('6.5%'),
        fontSize: hp('2%'),
        color: textColor,
    },
    textStyle: {
        flex: 1,
        color: colorPrimary,
        fontSize: hp("2%")
    },
    selectedItemStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 4,
        paddingLeft: 8,
        margin: 3,
        borderRadius: 14,
        borderWidth: 0.8,
        borderColor: colorPrimary,
        height: 28,
    },
})