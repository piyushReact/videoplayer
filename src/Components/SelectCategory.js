import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Platform, FlatList, Image } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { textColor, grey } from '../Common/Colors';
import AsyncStorage from '../Common/AsyncStorage';
import { PrintLog } from './PrintLog';

export default class SelectCategory extends Component {
    constructor(props) {
        super(props);
        const { categories, categoryIds } = this.props;
        this.state = {
            categories: categories ? categories : [],
            categoryIds: categoryIds ? categoryIds : [],
        }
    }

    async componentDidMount() {
        const { categoryIds } = this.props;
        if (this.state.categories.length <= 0) {
            let categories = await AsyncStorage.read("categories");
            if (categoryIds !== undefined && categoryIds !== null) {
                this.showSelected(categories, categoryIds);
            } else {
                this.setState({ categories })
            }
        }
    }

    componentWillReceiveProps = async (props) => {
        const { categories, categoryIds } = props;
        if (categories.length <= 0) {
            let categories = await AsyncStorage.read("categories");
            if (categoryIds !== undefined && categoryIds !== null) {
                this.showSelected(categories, categoryIds);
            } else {
                this.setState({ categories })
            }
        }
    }

    showSelected = (categories, id) => {
        const { multiSelect } = this.props;
        let arr = categories.map((item) => {
            if (item.id == id) {
                item.isSelected = true
                return item
            } else {
                if (!multiSelect) {
                    item.isSelected = false
                }
                return item;
            }
        });
        this.setState({ categories: arr });
    }


    selectFilterAction = (id, index) => {
        const { multiSelect } = this.props;
        const { categories } = this.state;
        let arr = categories.map((item) => {
            if (item.id == id) {
                item.isSelected = !item.isSelected
                return item
            } else {
                if (!multiSelect) {
                    item.isSelected = false
                }
                return item;
            }
        });
        let ids = [];
        let categoryId = [];
        if (multiSelect) {
            for (let i = 0; i < arr.length; i++) {
                if (arr[i].isSelected) {
                    ids.push(arr[i].id);
                }
            }
        } else {
            for (let i = 0; i < arr.length; i++) {
                if (arr[i].isSelected) {
                    categoryId = arr[i].id
                }
            }
        }
        this.props.handleCategories(arr);
        this.props.handleCategoryIds(multiSelect ? ids : categoryId);
        this.setState({ categories: arr });
    }

    _renderItem = (item) => {
        const { id, category, image, isSelected } = item.item;
        return (
            <TouchableOpacity onPress={() => this.selectFilterAction(id, item.index)}
                style={{ width: "33%", alignItems: 'center', justifyContent: 'center', marginTop: hp("1%") }}>
                {isSelected ?
                    <View>
                        <Image style={styles.imageStyle} source={{ uri: image }} />
                        <Image style={{ position: 'absolute', top: 0, right: 0 }}
                            source={require("../../assets/images/selected-tick.png")} />
                    </View>
                    :
                    <Image style={styles.imageStyle} source={{ uri: image }} />
                }
                <Text style={styles.textStyle}>{category}</Text>
            </TouchableOpacity>
        )
    }
    render() {
        const { categories } = this.state;

        return (
            <View>
                <Text style={styles.textHeader}>{'Select Category'}</Text>
                <View style={{ alignItems: 'center', margin: hp("1%"), marginTop: hp("0.6%"), width: '100%' }}>
                    <FlatList
                        extraData={this.state}
                        data={categories}
                        numColumns={3}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={this._renderItem}
                    />
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    textHeader: {
        color: grey,
        fontSize: hp("2%"),
        marginTop: hp("2%"),
    },
    textStyle: {
        color: textColor,
        fontSize: hp("2%"),
        marginTop: hp("1%"),
    },
    imageStyle: {
        width: wp("15%"), height: wp("15%")
    }
});


// categories: [
            //     { index: 0, category: "Development", image: require("../../assets/images/development.png"), isSelected: false },
            //     { index: 1, category: "Design", image: require("../../assets/images/design.png"), isSelected: false },
            //     { index: 2, category: "Cook", image: require("../../assets/images/cook.png"), isSelected: false },
            //     { index: 3, category: "Marketing", image: require("../../assets/images/marketing.png"), isSelected: false },
            //     { index: 4, category: "Music", image: require("../../assets/images/music.png"), isSelected: false },
            //     { index: 5, category: "Language", image: require("../../assets/images/language.png"), isSelected: false },
            // ],