import React, { Component } from 'react';
import { colorPrimary,filterBackground,} from '../Common/Colors';
import { Switch } from 'react-native-switch';


const SwitchComponent=(props)=>{
    const {value,onValueChange,circleSize,barHeight}=props;

    return (
        <Switch
            value={value}
            onValueChange={onValueChange}
            circleSize={circleSize}
            barHeight={barHeight}
            circleBorderWidth={0}
            backgroundActive={colorPrimary}
            backgroundInactive={'grey'}
            circleActiveColor={'#FFF'}
            circleInActiveColor={'#FFF'}
        />        
    )
}
export default SwitchComponent;