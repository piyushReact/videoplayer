import React, { Component } from 'react';
import { View, Image, TouchableOpacity, StyleSheet } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default class SocialLogin extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { linkedinAction, facebookAction, googleAction } = this.props;
        return (
            <View style={{ width: wp("80%"), alignItems: 'center', justifyContent: 'space-between', 
                flexDirection: 'row' }}>
                <TouchableOpacity style={styles.viewStyle} onPress={() => linkedinAction()}>
                    <Image source={require('../../assets/images/linkedin.png')} style={styles.imageStyle} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.viewStyle} onPress={() => facebookAction()}>
                    <Image source={require('../../assets/images/facebook.png')} style={styles.imageStyle} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.viewStyle} onPress={() => googleAction()}>
                    <Image source={require('../../assets/images/google-plus.png')} style={styles.imageStyle} />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewStyle: {

    },
    imageStyle: {

    }
})