import React, { Component } from 'react';
import { Dimensions, View, Text, TextInput, StyleSheet, Platform, StatusBar, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Form, Item, Input, Button, Icon, Label } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { colorPrimary, main, mainblue } from "../Common/Colors";
import AsyncStorage from "../Common/AsyncStorage";
import { Avatar, Badge, withBadge } from 'react-native-elements'
import IconIon from 'react-native-vector-icons/Ionicons'
import IconFont from 'react-native-vector-icons/FontAwesome'
import IconEnt from 'react-native-vector-icons/Feather'
import IconMetCom from 'react-native-vector-icons/MaterialCommunityIcons'
import IconMet from 'react-native-vector-icons/MaterialIcons'
import { FloatingAction } from "react-native-floating-action";


const actions = [
    {
      text: "Accessibility",
    //   icon: require("./images/ic_accessibility_white.png"),
      name: "bt_accessibility",
      position: 2
    },
    {
      text: "Language",
    //   icon: require("./images/ic_language_white.png"),
      name: "bt_language",
      position: 1
    },
    {
      text: "Location",
    //   icon: require("./images/ic_room_white.png"),
      name: "bt_room",
      position: 3
    },
    {
      text: "Video",
    //   icon: require("./images/ic_videocam_white.png"),
      name: "bt_videocam",
      position: 4
    }
  ];

export default class Headerr extends Component {
    //   static navigationOptions = {
    //     header: null
    //   } 
    constructor(props) {
        super(props);
        this.state = {
            Left: this.props.left,
            Center: this.props.center,
            Right: this.props.right,
            Icons: this.props.Icons,
            iconname: this.props.iconname,
            update: this.props.update

        }
    }
    componentWillMount() {
    }
    openDrawer1 = async () => {
        const open = await AsyncStorage.read('isloggedin')
        console.log(open)
        if (open) {
            this.props.navigation.openDrawer();
        } else {
            this.props.navigation.navigate('Login')
        }
    }
    openSearch = () => {
        this.props.navigation.navigate('SearchPage')
    }

    render() {
        const { username, password } = this.state;
        const { notiCount, rightSkip, notification, iconname, rightBtnShare, search, map, back, menu, rightmenu, update, bgColorHeader, bgColorContatiner, iconColor, leftName } = this.props;
        return (

            <View style={[styles.header, { backgroundColor: "white" }]}>
                <View style={[styles.container, { backgroundColor: "white" }]}>
                    <View style={{ left: wp("2%"), padding: 8, padding: 5, position: "absolute", alignItems: "center", justifyContent: "center" }}>

                        {menu &&
                            <View style={{ left: wp("1%"), padding: 8, position: "absolute", alignItems: "center", justifyContent: "center" }}>
                                <TouchableOpacity onPress={this.openDrawer1} style={{}}>
                                    <Icon style={styles.iconStyle} name={"menu"} />
                                </TouchableOpacity>
                            </View>}

                        {back &&
                            <View style={{ left: wp("0.6%"), padding: 4, position: "absolute", alignItems: "center", justifyContent: "center" }}>
                                <TouchableOpacity onPress={() => this.props.goBackPrevious()}>
                                    <IconIon name='ios-arrow-round-back' style={[styles.iconStyle, { color: iconColor }]} size={45}
                                    />

                                </TouchableOpacity>
                            </View>
                        }

                        {leftName &&
                            <View style={{ left: wp("0.6%"), padding: 4, position: "absolute", alignItems: "center", justifyContent: "center" }}>
                                <TouchableOpacity >
                                    <Text style={{ color: iconColor, fontWeight: "500", fontSize: wp("6%") }}>{leftName}</Text>

                                </TouchableOpacity>
                            </View>
                        }

                        {rightBtnShare &&
                            <View style={{ right: wp("0.6%"), padding: 4, position: "absolute", backgroundColor: "green" }}>
                                <TouchableOpacity
                                    onPress={() => this.props.goToShare()}
                                    style={{
                                        justifyContent: "center",
                                        alignItems: "center", width: wp("25%"), height: 27, borderRadius: 32, borderWidth: 1,
                                        borderColor: mainblue
                                    }}>
                                    <Text style={{ alignItems: "center", fontSize: 12 }}> {"Share Group"} </Text>
                                </TouchableOpacity>
                            </View>
                        }
                        {/* 
                        {back &&
                            <View style={{ left: wp("2%"), padding: 8, position: "absolute", alignItems: "center", justifyContent: "center" }}>
                                { <Icon style={styles.iconStyle} name={"back"} onPress={() => this.props.navigation.back()}/> }
                                <TouchableOpacity onPress={() => this.props.navigation.goBack(null, { key : "somedata" })}>

                                    <IconIon name='ios-arrow-round-back' style={styles.iconStyle} size={45}  
                                        style={{ color: "white" }}

                                    />
                                </TouchableOpacity>
                            </View>
                        } */}
                    </View>
                    <Text numberOfLines={1}
                        style={styles.title}> {this.props.title}</Text>


                    {search &&
                        <View style={{ right: wp("3%"), padding: 8, position: "absolute", alignItems: "center", justifyContent: "center" }}>
                            <TouchableOpacity onPress={this.openSearch}>
                                <Icon style={styles.iconStyle} name={"search"} />

                            </TouchableOpacity>
                        </View>
                    }
                    {map &&
                        <View style={{ right: wp("3%"), padding: 8, position: "absolute", alignItems: "center", justifyContent: "center" }}>
                            <TouchableOpacity onPress={() => this.props.map()}>
                                {/* {/ <TouchableOpacity onPress={() => this.props.navigation.navigate('Map')}> /} */}

                                <Icon style={styles.iconStyle} name={this.props.map ? "map" : ""} />
                            </TouchableOpacity>
                        </View>
                    }
                    {rightmenu &&
                        <View style={{ flexDirection : "row", right: wp("3%"),marginTop : hp("2%"), padding: 8, position: "absolute", alignItems: "center", justifyContent: "center" }}>
                             {/* <TouchableOpacity onPress={()=> this.props.goToShare()} > 
                             <IconEnt style={[styles.iconStyle, {color : "black"}]} name={"share"} size={25} color={"black"} />
                             </TouchableOpacity> */}  
                             <TouchableOpacity onPress={()=> this.props.goTomakeOption()} > 
                             <IconMetCom style={[styles.iconStyle, {color : "black"}]} name={"dots-vertical"} size={25} color={"black"} />
                             </TouchableOpacity>
                             
                            
                                {/* <TouchableOpacity onPress={()=> this.props.deleteGroup()} style={{ flexDirection : "row" }}> 
                                <IconMetCom style={[styles.iconStyle, {color : "black" , marginLeft : wp("2%")}]} name={"delete-empty"} size={30} color={"black"} />
                             </TouchableOpacity> */}
                          
                            

                        </View>}

                    {rightSkip &&

                        <View style={{ right: wp("3%"), padding: 8, position: "absolute", alignItems: "center", justifyContent: "center", backgroundColor: "transparents" }}>
                            <TouchableOpacity onPress={() => this.props.skipAction()}>
                                <Text style={{ color: "black", fontSize: wp("5%") }}>{"Skip"}</Text>
                            </TouchableOpacity>
                        </View>

                    }

                    {update &&

                        <View style={{ right: wp("3%"), padding: 8, position: "absolute", alignItems: "center", justifyContent: "center" }}>
                            <TouchableOpacity onPress={() => this.props.updateAction()}>
                                <IconFont style={styles.iconStyle} name={"pencil-square-o"} size={25} />
                            </TouchableOpacity>
                        </View>

                    }
                    {notification &&

                        <View style={{ width: hp("20%"), height: 40, marginRight: wp("7%") }}>
                            <Badge value={notiCount} size={5} badgeStyle={{ position: "absolute", right: -2, backgroundColor: main }} />
                            <TouchableOpacity
                                onPress={() => this.props.onPressNotification()}
                                style={{ right: wp("0%"), top: hp("1%"), position: "absolute", alignItems: "center", justifyContent: "center", }}>
                                {/* <Avatar
                                        rounded
                                        source={{
                                            uri: 'https://randomuser.me/api/portraits/men/41.jpg',
                                        }}
                                        size="large"
                                    /> */}
                                <IconMet size={26} name={"notifications-none"} />
                            </TouchableOpacity>

                        </View>
                    }

                </View>
            </View>
        );
    }
}



const styles = StyleSheet.create({
    header: {
        width: wp("100%"),
        height: Platform.OS === "ios" ? 56 + hp("4%") : 56,
        backgroundColor: "white",
        alignItems: 'center',
        justifyContent: "center",
    },
    container: {
        width: wp("100%"),
        height: 56,
        backgroundColor: "white",
        position: "absolute",
        bottom: 0,
        alignItems: 'center',
        justifyContent: "center",
        flexDirection: "row",
    },
    iconStyle: {
        marginLeft: wp("2%"),
        color: "white",
    },
    title: {
        fontSize: 20,
        color: "white",
        width: "60%",
        textAlign: "center"

    }
});
