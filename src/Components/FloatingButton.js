import React from "react";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Platform, Image } from 'react-native';
import { Header } from 'native-base';
import { colorPrimary, white } from "../Common/Colors";
import { STATUSBAR_HEIGHT } from "../Common/Constants";
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';

export default class    FloatingButton extends React.Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
    }

    renderIco = () => {
        return <Image source={require("../../assets/images/chat.png")} />
    }

    render() {
        const {offsetY} = this.props;
        return (
            <ActionButton renderIcon={this.renderIco} bgColor={"#00000040"} 
                offsetX={wp("4%")} offsetY={offsetY ? offsetY : hp("9%")}
                buttonColor={colorPrimary}>
                <ActionButton.Item buttonColor='#9b59b6' title="New Task" onPress={() => console.log("notes tapped!")}>
                    <Icon name="md-create" style={styles.actionButtonIcon} />
                </ActionButton.Item>
                <ActionButton.Item buttonColor='#3498db' title="Notifications" onPress={() => { }}>
                    <Icon name="md-notifications-off" style={styles.actionButtonIcon} />
                </ActionButton.Item>
                <ActionButton.Item buttonColor='#1abc9c' title="All Tasks" onPress={() => { }}>
                    <Icon name="md-done-all" style={styles.actionButtonIcon} />
                </ActionButton.Item>
            </ActionButton>
        );
    }
}
const { height, width } = Dimensions.get("window");
const styles = StyleSheet.create({
    actionButtonIcon: {
        fontSize: 20,
        height: 22,
        color: 'white',
    },
});
