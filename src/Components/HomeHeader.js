import React from "react";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, Platform, Image } from 'react-native';
import { Header } from 'native-base';
import { colorPrimary, white, notification } from "../Common/Colors";
import { STATUSBAR_HEIGHT } from "../Common/Constants";

export default class HomeHeader extends React.Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
    }

    render() {
        const { filter } = this.props;
        return (
            // <Header style={styles.container}>
            <View style={styles.headerStyle}>
                <View style={styles.container}>
                    <TouchableOpacity
                        style={styles.backButtonContainer} >
                        {/* <View style={{ padding: 8 }}>
                            <Image source={require("../../assets/images/back-arrow.png")} />
                        </View> */}
                    </TouchableOpacity>

                    <View style={styles.rightSideContainer}>
                        {filter &&
                            <TouchableOpacity onPress={() => this.props.filterAction()}>
                                <View style={{ padding: 8 }} >
                                    <Image source={require("../../assets/images/filter.png")} />
                                </View>
                            </TouchableOpacity>
                        }
                        <TouchableOpacity onPress={() => this.props.notificationAction()}>
                            <View style={{ padding: 8 }} >
                                <Image source={require("../../assets/images/globe-white.png")} />
                                {/* <View style={{ width: 20, height: 20, borderRadius: 10, backgroundColor: notification,
                                    justifyContent: 'center', alignItems: 'center', position: 'absolute', top: 0, right: 0 }}>
                                    <Text style={{ fontSize: 8, color: white }}>{""}</Text>
                                </View> */}
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.drawerAction()}>
                            <View style={{ padding: 8 }} >
                                <Image source={require("../../assets/images/push.png")} />
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                {/* </Header> */}
            </View>
        );
    }
}
const { height, width } = Dimensions.get("window");
const styles = StyleSheet.create({
    headerStyle: {
        backgroundColor: colorPrimary,
        height: Platform.OS === "ios" ? 52 + hp("4%") : 52,
        justifyContent: "center",
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4
        },
        shadowOpacity: 0.1,
        shadowRadius: 3.0,
    },
    container: {
        marginTop: Platform.OS === "ios" ? STATUSBAR_HEIGHT : 0,
        backgroundColor: colorPrimary,
        height: 52,
        justifyContent: "center",
        alignItems: "center",
        width: wp("100%"),
        position: 'absolute',
        bottom: 0
    },
    backButtonContainer: {
        // position: "absolute",
        // left: wp("3%")
    },
    rightSideContainer: {
        position: "absolute",
        right: wp("3%"),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    profileHeaderText: {
        fontSize: hp("2.4%"), color: white, fontFamily: 'Ubuntu'
    }
});
