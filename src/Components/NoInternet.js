import React, { Component } from 'react';
import { View, Text, TouchableOpacity,Image } from 'react-native'
import { black, borderColor, grey, colorPrimary, textColor, mainblue, bgColor } from '../Common/Colors';

export default class NoInternet extends Component {w
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <View style={{ width: '80%', height: '30%',justifyContent:'center',alignItems:'center' }}>
          <Image 
          source={{uri:'https://previews.123rf.com/images/alekseyvanin/alekseyvanin1704/alekseyvanin170400720/75323535-wifi-icon-in-prohibition-red-circle-no-internet-signal-ban-or-stop-sign-forbidden-symbol-vector-illu.jpg'}} 
          resizeMode='contain' style={{width:'90%',height:'100%'}}></Image>
        </View>

        <Text style={{ padding: '3%', alignSelf: 'center', color: mainblue, fontSize: 35, fontWeight: 'bold' }}>Ooops!</Text>

        <View style={{ width: '60%', }}>
          <Text
            style={{ padding: '5%', alignSelf: 'center', color: 'green', fontSize: 17, }}>Slow or no internet connection,  Check your internet connection</Text>
        </View>
        <TouchableOpacity style={{ marginTop: 35, width: '42%', height: '7%', backgroundColor: mainblue, justifyContent: 'center', borderRadius: 20 }}>
          <Text style={{ color: 'white', fontSize: 17, textAlign: 'center', fontWeight: 'bold' }}>Try Again</Text>
        </TouchableOpacity> 
      </View>
    );
  }
}