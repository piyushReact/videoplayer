
import ImagePicker from 'react-native-image-picker';
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';
import {
    View, Text, TouchableWithoutFeedback, Keyboard, StyleSheet, ImageBackground, TouchableOpacity,
    Platform, Image, TouchableHighlight, DeviceEventEmitter, Linking
} from 'react-native';

const {  } = this.props

const ImagePick = () => {

        if (Platform.OS == "ios") {
            check(PERMISSIONS.IOS.PHOTO_LIBRARY)
                .then(result => {
                    switch (result) {
                        case RESULTS.UNAVAILABLE:
                            console.log(
                                'This feature is not available (on this device / in this context)',
                            );
                            break;
                        case RESULTS.DENIED:
                            console.log('The permission has not been requested / is denied but requestable');
                            // this._requestPermissions();

                            _requestPermissions = () => {
                                if (Platform.OS == "ios") {
                                    request(PERMISSIONS.IOS.PHOTO_LIBRARY).then(result => {
                                        console.log('Getting Access');    //result== granted or blocked
                        
                                    }).catch(error => {
                                        console.log(error)
                                    });
                                } else {
                                    request(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE).then(result => {
                                        console.log('Getting Access');    //result== granted or blocked
                        
                                    }).catch(error => {
                                        console.log(error)
                                    });
                                }
                        
                            }
                        break;
                        case RESULTS.GRANTED:
                            console.log('The permission is granted');
                            // this._selectImage();

                            _selectImage = () => {

                                const options = {
                                    title: 'Select Avatar',
                                    storageOptions: {
                                        skipBackup: true,
                                        path: 'images',
                                        // quality : 0.1
                        
                                    },
                                    maxWidth: 250,
                                    maxHeight: 250,
                                    quality: 2,
                        
                                };
                        
                                ImagePicker.launchImageLibrary(options, (response) => {
                                    console.log('Response = ', response);
                        
                                    if (response.didCancel) {
                                        console.log('User cancelled image picker');
                                    } else if (response.error) {
                                        console.log('ImagePicker Error: ', response.error);
                                    } else if (response.customButton) {
                                        console.log('User tapped custom button: ', response.customButton);
                                    } else {
                                        const source = { uri: response.uri };
                        
                                        // You can also display the image using data:
                                        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                        
                                        PrintLog("response Image ==> ", response)
                                        // You can also display the image using data:
                                        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                                        let imageData = {
                                            uri: response.uri,
                                            width: 300,
                                            height: 300,
                                            name: "captured_image.JPG",
                                            type: "image/jpeg"
                                        };
                                        PrintLog("image data ==> ", imageData)
                                          return { 
                                            userImage1: imageData.uri, userImage: imageData
                                          }
                                        // this.setState({ userImage1: imageData.uri, userImage: imageData })
                                    }
                                });
                        }

                            break;
                        case RESULTS.BLOCKED:

                            Alert.alert(
                                'Permissions',
                                'You have not Access Photos.Go to Settings and Allow Permissions',
                                [
                                    {
                                        text: 'Cancel',
                                        onPress: () => console.log('Cancel Pressed'),
                                        style: 'cancel',
                                    },
                                    { text: 'OK', onPress: () => Linking.openURL('app-settings:') },
                                ],
                                { cancelable: false }
                            )


                            console.log('The permission is denied and not requestable anymore');
                            // Linking.openURL('app-settings:');
                            break;
                    }
                })
                .catch(error => {
                    console.log(error)
                });
        }
        else {
            check(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE)
                .then(result => {
                    switch (result) {
                        case RESULTS.UNAVAILABLE:
                            console.log(
                                'This feature is not available (on this device / in this context)',
                            );
                            break;
                        case RESULTS.DENIED:
                            console.log('The permission has not been requested / is denied but requestable');
                            // _requestPermissions();

                            _requestPermissions = () => {
                                if (Platform.OS == "ios") {
                                    request(PERMISSIONS.IOS.PHOTO_LIBRARY).then(result => {
                                        console.log('Getting Access');    //result== granted or blocked
                        
                                    }).catch(error => {
                                        console.log(error)
                                    });
                                } else {
                                    request(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE).then(result => {
                                        console.log('Getting Access');    //result== granted or blocked
                        
                                    }).catch(error => {
                                        console.log(error)
                                    });
                                }
                        
                            }




                            break;
                        case RESULTS.GRANTED:
                            console.log('The permission is granted');
                            // this._selectImage();

                            _selectImage = () => {

                                const options = {
                                    title: 'Select Avatar',
                                    storageOptions: {
                                        skipBackup: true,
                                        path: 'images',
                                        // quality : 0.1
                        
                                    },
                                    maxWidth: 250,
                                    maxHeight: 250,
                                    quality: 2,
                        
                                };
                        
                                ImagePicker.launchImageLibrary(options, (response) => {
                                    console.log('Response = ', response);
                        
                                    if (response.didCancel) {
                                        console.log('User cancelled image picker');
                                    } else if (response.error) {
                                        console.log('ImagePicker Error: ', response.error);
                                    } else if (response.customButton) {
                                        console.log('User tapped custom button: ', response.customButton);
                                    } else {
                                        const source = { uri: response.uri };
                        
                                        // You can also display the image using data:
                                        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                        
                                        PrintLog("response Image ==> ", response)
                                        // You can also display the image using data:
                                        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
                                        let imageData = {
                                            uri: response.uri,
                                            width: 300,
                                            height: 300,
                                            name: "captured_image.JPG",
                                            type: "image/jpeg"
                                        };
                                        PrintLog("image data ==> ", imageData)
                                        return   imageData 
                                        // this.setState({ userImage1: imageData.uri, userImage: imageData })
                                    }
                                });
                        }

                            break;
                        case RESULTS.BLOCKED:

                            Alert.alert(
                                'Permissions',
                                'You have not Access Photos.Go to Settings and Allow Permissions',
                                [
                                    {
                                        text: 'Cancel',
                                        onPress: () => console.log('Cancel Pressed'),
                                        style: 'cancel',
                                    },
                                    { text: 'OK', onPress: () => Linking.openURL('app-settings:') },
                                ],
                                { cancelable: false }
                            )

                            console.log('The permission is denied and not requestable anymore');
                            // Linking.openURL('app-settings:');
                            break;
                    }
                })
                .catch(error => {
                    console.log(error)
                });
        }
    }

  export default ImagePick ;