import React from 'react';
import { StyleSheet, Platform, Text, View, ScrollView, Dimensions } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { borderColor, colorPrimary } from '../Common/Colors';

export default class Ratings extends React.Component{
    constructor(props)
    {
        super(props)
        this.state = {
            ratings: -1,
        }
    }

    render(){
        const { ratings } = this.props
        return (
            <View style={styles.mainContainer}>
                <View style={styles.container}>
                    <View style={ratings > 0 ? styles.dotOn : styles.dotOff}/>
                    <View style={ratings > 1 ? styles.dotOn : styles.dotOff}/>
                    <View style={ratings > 2 ? styles.dotOn : styles.dotOff}/>
                    <View style={ratings > 3 ? styles.dotOn : styles.dotOff}/>
                    <View style={ratings > 4 ? styles.dotOn : styles.dotOff}/>
                </View>
            </View>
                
        )
    }
}
const { height, width } = Dimensions.get('window')
let size = hp("1.3%");
const styles = StyleSheet.create({
    mainContainer: {
        justifyContent: 'center',
        // alignItems: 'center',
        marginTop: 2
    },
    container: {
        backgroundColor: '#FFF',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent:'space-between',
        width: wp('20%'),

    },
    dotOn: {
        width: size,
        height: size,
        backgroundColor: colorPrimary,
        borderRadius: size / 2
    },
    dotOff: {
        width: size,
        height: size,
        backgroundColor: '#FFF',
        borderColor: borderColor,
        borderWidth: 0.8,
        borderRadius: size / 2
    }
  });