import React, { Component } from 'react';
import { View, Image, TouchableOpacity, StyleSheet, FlatList, Text } from 'react-native';
import profileImage from '../../assets/images/download.jpeg'
import { Item } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { borderColor, grey, white, textColor, colorPrimary, greyText } from '../Common/Colors';

const dummyArray = [
    { name: "Ajay Bharti", arr: ["", "", ""] },
    { name: "Ajay Bharti", arr: ["", ""] },
    { name: "Ajay Bharti", arr: ["", ""] },
]


export default class Timeline extends Component {
    constructor(props) {
        super(props);
    }

    _renderTimeLineList = (item) => {
        const { arr } = item.item;
        let size = hp("7%");
        return (
            <View style={{ width: wp("100%"), justifyContent: 'center', marginLeft: wp("2%") }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', }}>
                    <View style={{ alignItems: 'center', width: wp("20%") }}>
                        <View style={styles.verticleLine} />
                        <View style={{
                            backgroundColor: white, borderWidth: 0.8, borderColor: grey,
                            borderRadius: 12, height: 24, paddingLeft: 12, paddingRight: 12, alignItems: 'center',
                            justifyContent: 'center', margin: 2
                        }}>
                            <Text style={{ fontSize: hp("1.8%"), color: grey }}>{"2016"}</Text>
                        </View>
                        {/* <View style={styles.verticleLine} /> */}
                    </View>
                    <Text style={{ fontSize: hp("1.8"), color: grey, marginLeft: wp("0.4%"), marginTop: hp("2%") }}>{"June"}</Text>
                </View>

                {arr.map((item, index) => {
                    return (
                        <View key={index} style={{ flexDirection: 'row' }}>
                            <View style={{ alignItems: 'center', width: wp("20%") }}>
                                <View style={styles.verticleLine} />
                                <View style={{
                                    backgroundColor: white, borderWidth: 0.8, borderColor: colorPrimary,
                                    borderRadius: 8, height: 16, width: 16, margin: 2
                                }} />
                                <View style={[styles.verticleLine, { height: hp("12%") }]} />
                            </View>
                            <View style={{ marginLeft: wp("0.4%"), marginTop: hp("2%") }}>
                                <Text style={{ fontSize: hp("1.8"), color: grey }}>{"11:48 am"}</Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: hp("1%") }}>
                                    <Text style={{ color: textColor, fontSize: hp("2%") }}>{"John Cena"}</Text>
                                    <View style={{
                                        backgroundColor: greyText, height: 20, borderRadius: 10, paddingLeft: 12,
                                        paddingRight: 12, marginLeft: wp("2%"), justifyContent: 'center'
                                    }}>
                                        <Text style={{ fontSize: hp("1.8%"), color: textColor }}>{"Followed"}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: hp("1%") }} >
                                    <Image style={{ width: size, height: size, borderRadius: size / 2, borderColor: grey }}
                                        source={require('../../assets/images/download.jpeg')} />
                                    <Text numberOfLines={1} style={{
                                        fontSize: hp("2%"), marginLeft: wp("2%"), alignSelf: 'center',
                                        color: textColor, width: wp("54%")
                                    }}>{"Ajay Bharti Trust Center sdalfglsdfjg a lsgjalsdfjkgn "} </Text>
                                </View>
                            </View>
                        </View>
                    )
                })}
            </View>

        )
    }

    render() {
        const { } = this.props;
        return (
            <View style={{
                width: wp("100%"), alignItems: 'center', justifyContent: 'space-between',
                flexDirection: 'row'
            }}>
                <FlatList
                    data={dummyArray}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={this._renderTimeLineList}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewStyle: {
    },
    imageStyle: {
    },
    verticleLine: {
        width: 1, backgroundColor: borderColor, height: hp("2%"),
    }
})