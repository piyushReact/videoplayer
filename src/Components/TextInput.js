import React from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { View, Text, StyleSheet, Dimensions, TextInput, Image } from 'react-native';
import { Container, Content, Form, Item, Input, Label, Textarea } from 'native-base';
import { onChangeCharacter, emailValidation } from '../Common/Utils';
import { PrintLog } from './PrintLog';
import { black, colorPrimary, grey, borderColor, textColor } from '../Common/Colors';
// import { Input as InputEl } from 'react-native-elements';

export default class CustomTextInput extends React.Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props); 
    this.state = {
      placeholder: this.props.placeholder,
      value: this.props.value != "" ? this.props.value : null,
      showError: false,
    }
  }

  onFocus() {
    const { value } = this.state;
    if (this.props.value == "") {
      this.setState({ placeholder: null })
    }
    if (this.props.isCountry) {
      if (value != this.props.value) {
        if (this.props.value) {
          this.setState({ value: this.props.value })
        }
      }
    }
  }

  onEndEditing() {
    const { value } = this.state;
    if (this.props.value == "") {
      this.setState({ placeholder: this.props.placeholder })
    }
  }

  onEndEditingEmail() {
    if (this.props.value == "") {
      this.setState({ placeholder: this.props.placeholder })
    } else if (!emailValidation(this.state.value)) {
      this.props.showErrorDialog(true);
    }
  }

  onChangeText(text) {
    if (text !== "") {
      this.setState({ value: text, showError: false });
    } else {
      this.setState({ showError: true });
    }
    this.props.onChange(text);
  }

  onChangeTextName(text) {
    let value = onChangeCharacter(text);
    this.setState({ value })
    this.props.onChange(value);
  }

  validateInput = () => {
    const { value } = this.props;
    if (value === undefined || value === null || value === "") {
      this.setState({ showError: true });
    } else {
      this.setState({ showError: false });
    }
  }

  render() {
    const { showError } = this.state;
    const { placeholder, marginTop, keyboardType, editable, multiline, width, paddingLeft, leftIcon, value } = this.props;
    if (this.props.profileInput) {
      return (
        <View style={[styles.profileContainer, { width: width ? width : '100%' }]}>
          {/* <InputEl
            containerStyle={{ marginLeft: 0, marginRight: 0, paddingLeft: 0, paddingRight: 0, width: '100%' }}
            leftIcon={leftIcon}
            leftIconContainerStyle={{ width: '12%', marginLeft: 0, paddingLeft: 0, marginBottom: -8 }}
            autoCapitalize='none'
            autoCorrect={false}
            editable={!editable ? editable : true}
            multiline={multiline ? multiline : false}
            keyboardType={keyboardType ? keyboardType : 'default'}
            selectionColor={colorPrimary}
            placeholder={placeholder}
            placeholderTextColor={'grey'}
            onEndEditing={() => this.onEndEditing()}
            onFocus={() => this.onFocus()}
            onBlur={this.validateInput}
            value={value}
            underlineColorAndroid="transparent"
            onChangeText={(text) => this.onChangeText(text)}
           inputContainerStyle={{ borderBottomColor: showError ? 'red' : borderColor }}
            inputStyle={[styles.input, { marginBottom: -8, paddingLeft: 4 }]}
            errorStyle={{ color: 'red' }}
            errorMessage={showError ? 'PLEASE ENTER VALID DETAIL HERE' : ""}
          /> */}
          {/* <TextInput
            autoCapitalize='none'
            autoCorrect={false}
            editable={!editable ? editable : true}
            multiline={multiline ? multiline : false}
            keyboardType={keyboardType ? keyboardType : 'default'}
            selectionColor={colorPrimary}
            placeholder={placeholder}
            placeholderTextColor={'grey'}
            onEndEditing={() => this.onEndEditing()}
            // onSubmitEditing={() => onSubmitEditing() ? onSubmitEditing() : null}
            onFocus={() => this.onFocus()}
            value={this.props.value}
            underlineColorAndroid="transparent"
            onChangeText={(text) => this.onChangeText(text)}
            style={[styles.input, { paddingLeft: paddingLeft ? paddingLeft : 4 }]} /> */}
        </View>
      );
    }

    if (this.props.cardInput) {
      return (
        <View style={[styles.profileContainer, { width: width ? width : '90%' }]}>
          <TextInput
            autoCapitalize='none'
            autoCorrect={false}
            keyboardType={keyboardType ? keyboardType : 'default'}
            selectionColor={colorPrimary}
            placeholder={placeholder}
            placeholderTextColor={'grey'}
            onEndEditing={() => this.onEndEditing()}
            onFocus={() => this.onFocus()}
            value={this.props.value}
            underlineColorAndroid="transparent"
            onChangeText={(text) => this.onChangeText(text)}
            style={[styles.input, { paddingLeft: paddingLeft ? paddingLeft : 4 }]} />
        </View>
      );
    }

    if (this.props.multiline) {
      return (
        <View style={[styles.profileContainer, { height: hp("13%"), width: '100%', marginTop: hp("1%") }]}>
          <InputEl
            containerStyle={{ marginLeft: 0, marginRight: 0, paddingLeft: 4, paddingRight: 0, width: '100%' }}
            autoCapitalize='none'
            autoCorrect={false}
            editable={!editable ? editable : true}
            multiline={true}
            keyboardType={'default'}
            selectionColor={colorPrimary}
            placeholder={placeholder}
            placeholderTextColor={'grey'}
            onEndEditing={() => this.onEndEditing()}
            onFocus={() => this.onFocus()}
            onBlur={this.validateInput}
            value={value}
            underlineColorAndroid="transparent"
            onChangeText={(text) => this.onChangeText(text)}
            inputContainerStyle={{ borderBottomColor: showError ? 'red' : borderColor }}
            inputStyle={[styles.input, { paddingLeft: 4, height: hp("13%") }]}
            errorStyle={{ color: 'red' }}
            errorMessage={showError ? 'PLEASE ENTER VALID DETAIL HERE' : ""}
          />
          {/* <Textarea
            autoCapitalize='none'
            autoCorrect={false}
            multiline={true}
            keyboardType={'default'}
            selectionColor={colorPrimary}
            placeholder={placeholder}
            placeholderTextColor={textColor}
            onEndEditing={() => this.onEndEditing()}
            onFocus={() => this.onFocus()}
            value={this.props.value}
            underlineColorAndroid="transparent"
            onChangeText={(text) => this.onChangeText(text)}
            style={[styles.input, { paddingLeft: 0, height: hp("13%") }]} /> */}
        </View>
      );
    }

    if (this.props.isPassword) {
      return (
        <Form style={[styles.container, { marginTop: marginTop ? marginTop : 0, marginBottom: 0 }]}>
          <Item floatingLabel>
            <Label style={styles.label}>{placeholder}</Label>
            <Input autoCapitalize='none'
              autoCorrect={false}
              secureTextEntry={true}
              selectionColor={colorPrimary}
              onEndEditing={() => this.onEndEditing()}
              onFocus={() => this.onFocus()}
              value={this.props.value}
              defaultValue={this.props.value}
              underlineColorAndroid="transparent"
              onChangeText={(text) => this.onChangeText(text)}
              style={styles.input} />
          </Item>
        </Form>
      );
    }
//TODO 
    return (
      <Form style={[styles.container, {
        marginTop: marginTop ? marginTop : 0, marginBottom: 0,
        width: width ? width : "100%" ,
      }]}>
        <Item floatingLabel>
          <Label style={styles.label}>{placeholder}</Label>
          <Input autoCapitalize='none'
            autoCorrect={false}
            maxLength={this.props.maxLength ? this.props.maxLength : 30}
            keyboardType={keyboardType ? keyboardType : 'default'}
            selectionColor={colorPrimary}
            onEndEditing={() => this.onEndEditing()}
            onFocus={() => this.onFocus()}
            value={this.props.value}
            defaultValue={this.props.value}
            underlineColorAndroid="transparent"
            onChangeText={(text) => this.onChangeText(text)}
            style={styles.input} 
            />
        </Item>
      </Form>
    );
  }
}

const { height, width } = Dimensions.get('window')
const styles = StyleSheet.create({
  container: {
    height: hp('7.5%'),
    width: '100%',
    marginLeft: -12,
  },
  profileContainer: {
    height: hp('7.5%'),
    width: '100%',
  
  },
  input: {
    height: hp('7.5%'),
    // paddingLeft: 4,
    fontSize: hp('2%'),
    color: textColor,
  },
  label: {
    fontSize: hp('2%'),
    color: grey,
    paddingLeft: 4,
  },
  otpText: {
    height: hp('7.5%'), width: '16%', borderWidth: 1, borderColor: borderColor, color: textColor,
    borderRadius: 16, textAlign: 'center', padding: 10, fontWeight: 'bold', fontSize: hp('2%'),
  },
});
