import React, { Component } from 'react'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    View, Text, TouchableWithoutFeedback, Keyboard, StyleSheet, ImageBackground, TouchableOpacity,
    Platform, Image, TouchableHighlight, DeviceEventEmitter
} from 'react-native';
import { Container, Content, Form, Item, Input, Label } from 'native-base';
import Loader from '../../Components/Loader';
import { black, borderColor, grey, colorPrimary, textColor, mainblue } from '../../Common/Colors';
import Header from '../../Components/Header';

export default class Account extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            finish: false,
            fName: "",
            lName: " "
        }
    }

    validation = () => {
        const { fName, lName, finish } = this.state
        if (fName == "") {
            return false;
        } else if (lName == "") {
            return false;
        }
        return true
    }
    onChangeText = (text) => {
        const { fName, lName, finish } = this.state

        //   if(this.validation()){
        //   this.setState({ finish : true })
        //   this.props.navigation.navigate("welcome")
        //   }
        if (fName != "" && lName != "") {
            // DeviceEventEmitter.emit("customerName", fName , lName)
            this.props.navigation.navigate("Welcome", { "welcomeFName" : fName , "welcomeLName": lName} )
            // this.props.navigation.nav("welcome")        

        } else {
        }
    }

    navigateBack = () => {
        this.props.navigation.goBack();
    }
    goBACK=()=>{
        this.props.navigation.navigate("EnterOTP")
    }
    render() {
        return ( 
            <Container style={{ flex: 1 }}>
                <Loader loading={this.state.loading} />
                <Header  leftName = {"Account"} bgColorContatiner ={"white"} iconColor ={mainblue}  />

                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                    <View style={{ flex: 1, height : hp("100%"), width : wp("100%"), }}>
                     
                      
                    </View>
                </TouchableWithoutFeedback> 

            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: hp('7.5%'),
        width: '100%',
        marginLeft: -12,
    },
    profileContainer: {
        height: hp('7.5%'),
        width: '100%',
    },
    input: {
        height: hp('7.5%'),
        // paddingLeft: 4,
        fontSize: hp('2%'),
        color: textColor,
    }
})