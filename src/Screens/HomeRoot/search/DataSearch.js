import React from 'react';
import { View, Text, FlatList } from 'react-native';
import SearchBar from '../search/search';
import { colorPrimary } from '../../../Common/Colors';

const DataSearch = props => {
    const {
        data,
        handleResults,
        hintText,
    } = props;
    return(
        <View style = {{ height: 72, width: '100%', marginTop: -1}}>
            <SearchBar
                ref={(ref) => this.searchBar = ref}
                data={data}
            //    handleResults={(value) => handleResults(value)}
                handleChangeText = {(value) => handleResults(value)}
                onBack={() => null}
                placeholderTextColor={"black"}
                placeholder={hintText}
            />
        </View>
    )
}

export default DataSearch;