import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity , StyleSheet} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { primaryColor, secondaryColor, grey } from '../../../../Common/Colors'
import PlayThumb from '../../Play';
const threeDots = require('../../../../../assests/groupIcon/threedot.png')
const tick = require('../../../../../assests/groupIcon/tick.png')
const like = require('../../../../../assests/groupIcon/like.png')
const comment = require('../../../../../assests/groupIcon/comment.png')
const share = require('../../../../../assests/groupIcon/share.png')
import Video, { FilterType } from 'react-native-video';

const filterTypes = [
    FilterType.NONE,
    FilterType.INVERT,
    FilterType.MONOCHROME,
    FilterType.POSTERIZE,
    FilterType.FALSE,
    FilterType.MAXIMUMCOMPONENT,
    FilterType.MINIMUMCOMPONENT,
    FilterType.CHROME,
    FilterType.FADE,
    FilterType.INSTANT,
    FilterType.MONO,
    FilterType.NOIR,
    FilterType.PROCESS,
    FilterType.TONAL,
    FilterType.TRANSFER,
    FilterType.SEPIA
];
export default class Trending extends Component {
    constructor(props) {
        super(props);
        this.onLoad = this.onLoad.bind(this);
        this.onProgress = this.onProgress.bind(this);
        this.onBuffer = this.onBuffer.bind(this);
    }
    state = {
        rate: 1,
        volume: 1,
        muted: false,
        resizeMode: 'cover',
        duration: 0.0,
        currentTime: 0.0,
        controls: true,
        paused: true,
        skin: 'embed',
        ignoreSilentSwitch: null,
        isBuffering: false,
        filter: FilterType.NONE,
        filterEnabled: true,

        like : 500,
        islike : false


    };

    onLoad(data) {
        console.log('On load fired!');
        this.setState({ duration: data.duration });
    }

    onProgress(data) {
        this.setState({ currentTime: data.currentTime });
    }

    onBuffer({ isBuffering }) {
        this.setState({ isBuffering });
    }

    getCurrentTimePercentage() {
        if (this.state.currentTime > 0) {
            return parseFloat(this.state.currentTime) / parseFloat(this.state.duration);
        } else {
            return 0;
        }
    }

    setFilter(step) {
        let index = filterTypes.indexOf(this.state.filter) + step;

        if (index === filterTypes.length) {
            index = 0;
        } else if (index === -1) {
            index = filterTypes.length - 1;
        }

        this.setState({
            filter: filterTypes[index]
        })
    }

    renderSkinControl(skin) {
        const isSelected = this.state.skin == skin;
        const selectControls = skin == skin == 'native' || 'embed'; //skin == 'native' ||
        return (
            <TouchableOpacity onPress={() => {
                this.setState({
                    controls: selectControls,
                    skin: skin
                })
            }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
                    {skin}
                </Text>
            </TouchableOpacity>
        );
    }

    renderRateControl(rate) {
        const isSelected = (this.state.rate == rate);

        return (
            <TouchableOpacity onPress={() => { this.setState({ rate: rate }) }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
                    {rate}
        </Text>
            </TouchableOpacity>
        )
    }

    renderResizeModeControl(resizeMode) {
        const isSelected = (this.state.resizeMode == resizeMode);

        return (
            <TouchableOpacity onPress={() => { this.setState({ resizeMode: resizeMode }) }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
                    {resizeMode}
                </Text>
            </TouchableOpacity>
        )
    }

    renderVolumeControl(volume) {
        const isSelected = (this.state.volume == volume);

        return (
            <TouchableOpacity onPress={() => { this.setState({ volume: volume }) }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
                    {volume * 100}%
        </Text>
            </TouchableOpacity>
        )
    }

    renderIgnoreSilentSwitchControl(ignoreSilentSwitch) {
        const isSelected = (this.state.ignoreSilentSwitch == ignoreSilentSwitch);

        return (
            <TouchableOpacity onPress={() => { this.setState({ ignoreSilentSwitch: ignoreSilentSwitch }) }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
                    {ignoreSilentSwitch}
                </Text>
            </TouchableOpacity>
        )
    }

    likeHandler =() =>{
        const { like , islike } = this.state
        islike != false ? this.setState({ islike : false, like : like - 1 }) : this.setState({ islike : true, like : like + 1 })
    }

    render() {
        const { like, islike } = this.state
         return (
            <View>
                <FlatList
                    data={["", "", "", "", "", "", "", "", "", "", "",]}
                    renderItem={({ item, index }) =>
                        <View style={{ borderColor : "black", width: wp("100%"), height: hp('85%') , backgroundColor : "black", flexDirection: 'column' }}>
                            <View style={{ backgroundColor: "black"}}>
                                <TouchableOpacity
                                    style={{ justifyContent: "center", alignItems: "center",backgroundColor : "black" }}
                                    activeOpacity={true}
                                    // onPress={()=>alert('vf')}
                                    onPress={this.props.GoToPlay}
                                    >

                                    <Video
                                    source={{ uri: "https://rawgit.com/mediaelement/mediaelement-files/master/big_buck_bunny.mp4" }}
                                         style={{ height : hp("83%"), width : wp("100%"),  }}
                                         rate={this.state.rate}
                                         paused={this.state.paused}
                                         volume={this.state.volume}
                                         muted={this.state.muted}
                                         ignoreSilentSwitch={this.state.ignoreSilentSwitch}
                                         resizeMode={this.state.resizeMode}
                                         onLoad={this.onLoad}
                                         onBuffer={this.onBuffer}
                                         onProgress={this.onProgress}
                                         onEnd={() => { alert('Done!') }}
                                         repeat={true}
                                         controls={this.state.controls}
                                         filter={this.state.filter}
                                         filterEnabled={this.state.filterEnabled}
                                    />

                                    <TouchableOpacity
                                     style={{ height : 40, width : 40, position : "absolute", bottom : hp("8%"), left : wp("4%") }}>
                                    <Image source={require("../../../../../assests/footerIcon.js/Share.png")} />
                                    <Text style={{ color : "white" }}>{"500"}</Text>
                                        </TouchableOpacity>
                                    <TouchableOpacity 
                                    onPress={this.likeHandler}
                                    style={{ height : 40, width : 40, position : "absolute", bottom : hp("15%"), left : wp("4%") }}>
                                    <Image source={require("../../../../../assests/footerIcon.js/Like.png")} style={{ tintColor : islike != false ? "red" : "white" }}/>
                                    <Text style={{ color : "white" }}>{like}</Text>
                                        </TouchableOpacity>
                                    <TouchableOpacity style={{borderRadius : 42, borderWidth : 2,height : 40, width : 40, position : "absolute", bottom : hp("22%"), left : wp("3%"), justifyContent : "center", alignItems : "center" }}>
                                       <Image source={{ uri : "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS7FFrxhGko3hUyRhuXBo8cpFfx_cdN5z6syFiBHJBJNUyl2SFf" }}  style={{ height : 40, width : 40 ,borderRadius : 42,}}/>
                                      
                                        </TouchableOpacity>

                                    {/* <Image
                                        style={{ width: '100%', height: '100%' }}
                                        source={{ uri: 'http://hdwarena.com/wp-content/uploads/2018/07/Free-HD-Wallpaper.jpg' }}
                                    />
                                    <Image  
                                    onPress={this.props.GoToPlay}
                                        style={{ height : 40 , width : 40 , position : "absolute", top : "45%" }}
                                        source={require('./../../../../../assests/playIcon.png')} /> */}
                                </TouchableOpacity>
                            </View>
                        </View>} />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
    },
    fullScreen: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    controls: {
        backgroundColor: "transparent",
        borderRadius: 5,
        position: 'absolute',
        bottom: 44,
        left: 4,
        right: 4,
    },
    progress: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 3,
        overflow: 'hidden',
    },
    innerProgressCompleted: {
        height: 20,
        backgroundColor: '#cccccc',
    },
    innerProgressRemaining: {
        height: 20,
        backgroundColor: '#2C2C2C',
    },
    generalControls: {
        flex: 1,
        flexDirection: 'row',
        overflow: 'hidden',
        paddingBottom: 10,
    },
    skinControl: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    rateControl: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    volumeControl: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    resizeModeControl: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    ignoreSilentSwitchControl: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    controlOption: {
        alignSelf: 'center',
        fontSize: 11,
        color: "white",
        paddingLeft: 2,
        paddingRight: 2,
        lineHeight: 12,
    },
    nativeVideoControls: {
        top: 184,
        height: 300
    }
});