import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity , StyleSheet} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { primaryColor, secondaryColor, grey } from '../../../../Common/Colors'
import PlayThumb from '../../Play';
const threeDots = require('../../../../../assests/groupIcon/threedot.png')
const tick = require('../../../../../assests/groupIcon/tick.png')
const like = require('../../../../../assests/groupIcon/like.png')
const comment = require('../../../../../assests/groupIcon/comment.png')
const share = require('../../../../../assests/groupIcon/share.png')
import Video, { FilterType } from 'react-native-video';

const filterTypes = [
    FilterType.NONE,
    FilterType.INVERT,
    FilterType.MONOCHROME,
    FilterType.POSTERIZE,
    FilterType.FALSE,
    FilterType.MAXIMUMCOMPONENT,
    FilterType.MINIMUMCOMPONENT,
    FilterType.CHROME,
    FilterType.FADE,
    FilterType.INSTANT,
    FilterType.MONO,
    FilterType.NOIR,
    FilterType.PROCESS,
    FilterType.TONAL,
    FilterType.TRANSFER,
    FilterType.SEPIA
];
export default class Trending extends Component {
    constructor(props) {
        super(props);
        this.onLoad = this.onLoad.bind(this);
        this.onProgress = this.onProgress.bind(this);
        this.onBuffer = this.onBuffer.bind(this);
    }
    state = {
        rate: 1,
        volume: 1,
        muted: false,
        resizeMode: 'contain',
        duration: 0.0,
        currentTime: 0.0,
        controls: true,
        paused: true,
        skin: 'embed',
        ignoreSilentSwitch: null,
        isBuffering: false,
        filter: FilterType.NONE,
        filterEnabled: true
    };

    onLoad(data) {
        console.log('On load fired !');
        this.setState({ duration: data.duration });
    }

    onProgress(data) {
        this.setState({ currentTime: data.currentTime });
    }

    onBuffer({ isBuffering }) {
        this.setState({ isBuffering });
    }

    getCurrentTimePercentage() {
        if (this.state.currentTime > 0) {
            return parseFloat(this.state.currentTime) / parseFloat(this.state.duration);
        } else {
            return 0;
        }
    }

    setFilter(step) {
        let index = filterTypes.indexOf(this.state.filter) + step;

        if (index === filterTypes.length) {
            index = 0;
        } else if (index === -1) {
            index = filterTypes.length - 1;
        }

        this.setState({
            filter: filterTypes[index]
        })
    }

    renderSkinControl(skin) {
        const isSelected = this.state.skin == skin;
        const selectControls = skin == 'embed'; //skin == 'native' ||
        return (
            <TouchableOpacity onPress={() => {
                this.setState({
                    controls: selectControls,
                    skin: skin
                })
            }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
                    {skin}
                </Text>
            </TouchableOpacity>
        );
    }

    renderRateControl(rate) {
        const isSelected = (this.state.rate == rate);

        return (
            <TouchableOpacity onPress={() => { this.setState({ rate: rate }) }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
                    {rate}x
        </Text>
            </TouchableOpacity>
        )
    }

    renderResizeModeControl(resizeMode) {
        const isSelected = (this.state.resizeMode == resizeMode);

        return (
            <TouchableOpacity onPress={() => { this.setState({ resizeMode: resizeMode }) }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
                    {resizeMode}
                </Text>
            </TouchableOpacity>
        )
    }

    renderVolumeControl(volume) {
        const isSelected = (this.state.volume == volume);

        return (
            <TouchableOpacity onPress={() => { this.setState({ volume: volume }) }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
                    {volume * 100}%
        </Text>
            </TouchableOpacity>
        )
    }

    renderIgnoreSilentSwitchControl(ignoreSilentSwitch) {
        const isSelected = (this.state.ignoreSilentSwitch == ignoreSilentSwitch);

        return (
            <TouchableOpacity onPress={() => { this.setState({ ignoreSilentSwitch: ignoreSilentSwitch }) }}>
                <Text style={[styles.controlOption, { fontWeight: isSelected ? "bold" : "normal" }]}>
                    {ignoreSilentSwitch}
                </Text>
            </TouchableOpacity>
        )
    }






    render() {
        return (
            <View>
                <FlatList
                    data={["", "", "", "", "", "", "", "", "", "", "",]}
                    renderItem={({ item, index }) =>
                        <View style={{ width: wp("100%"), height: hp('57%'), backgroundColor: 'green', borderBottomWidth: 1, flexDirection: 'column' }}>
                            <View style={{ flex: 2, backgroundColor: primaryColor, flexDirection: 'row' }}>

                                <View style={{ flex: 1.6, flexDirection: 'row' }}>

                                    <View style={{ flex: 1.2, justifyContent: 'center', alignItems: 'center' }}>
                                        <View style={{ width: wp('11%'), height: wp('11%'), borderRadius: wp('5.5%'), backgroundColor: 'white', justifyContent: 'center', alignItems: 'center' }}>
                                            <Image
                                                source={{ uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSwFFD_j4rWvpmfG7KbCLNAQ77ceZ55rJVL4eNUOp2jUMYKiZn7&s" }}
                                                style={{ width: wp('10%'), height: wp('10%'), borderRadius: wp('5%'), backgroundColor: primaryColor, }}></Image>
                                        </View>
                                    </View>

                                    <View style={{ flex: 3, flexDirection: 'column' }}>

                                        <View style={{ flex: 1.3, flexDirection: 'row', }}>
                                            <View style={{ justifyContent: 'flex-end', }}>
                                                <Text style={{ fontSize: wp('4.5%'), fontWeight: '700', color: secondaryColor }}>Varun Dhavan</Text>
                                            </View>
                                            <View style={{ marginLeft: wp('3%'), justifyContent: 'flex-end', alignItems: 'center', }}>
                                                <View style={{ width: wp('5%'), height: wp('5%'), borderRadius: wp('2.5%'), backgroundColor: secondaryColor, justifyContent: 'center', alignItems: 'center', }}>
                                                    <Image source={tick} style={{ width: '70%', height: '70%', tintColor: primaryColor }}></Image>
                                                </View>
                                            </View>
                                        </View>

                                        <View style={{ flex: 1, }}>
                                            <Text style={{ fontSize: wp('3.5%'), color: 'grey', marginTop: wp('0.5%') }}>{'2019-12-31'} {'17:19:46'}</Text>
                                        </View>
                                    </View>

                                </View>

                                <View style={{ flex: 1, flexDirection: 'row', }}>
                                    <View style={{ flex: 2, justifyContent: 'center', }}>
                                        <TouchableOpacity style={{ width: '100%', height: wp('7%'), alignSelf: 'center', borderRadius: 20, backgroundColor: 'grey', justifyContent: 'center', alignItems: 'center', }}>
                                            <Text style={{ fontSize: wp('3.3%'), color: primaryColor }}>FOLLOWING</Text>
                                        </TouchableOpacity>
                                    </View>

                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                                        <TouchableOpacity style={{ width: '25%', height: '35%', }}>
                                            <Image source={threeDots} style={{ width: '100%', height: '100%', tintColor: secondaryColor }}></Image>
                                        </TouchableOpacity>
                                    </View>
                                </View>

                            </View>


                            <View style={{ flex: 10, backgroundColor: "white" }}>
                                <TouchableOpacity
                                    style={{ justifyContent: "center", alignItems: "center", }}
                                    activeOpacity={true}
                                    // onPress={this.props.GoToPlay}
                                    >

                                    <Video
                                         source={{ uri: "https://rawgit.com/mediaelement/mediaelement-files/master/big_buck_bunny.mp4" }}
                                         style={{ height : "100%", width : "100%" }}
                                         rate={this.state.rate}
                                         paused={this.state.paused}
                                         volume={this.state.volume}
                                         muted={this.state.muted}
                                         ignoreSilentSwitch={this.state.ignoreSilentSwitch}
                                         resizeMode={this.state.resizeMode}
                                         onLoad={this.onLoad}
                                         onBuffer={this.onBuffer}
                                         onProgress={this.onProgress}
                                         onEnd={() => { alert('Done!') }}
                                         repeat={true}
                                         controls={this.state.controls}
                                         filter={this.state.filter}
                                         filterEnabled={this.state.filterEnabled}
                                    />

                                    {/* <Image
                                        style={{ width: '100%', height: '100%' }}
                                        source={{ uri: 'http://hdwarena.com/wp-content/uploads/2018/07/Free-HD-Wallpaper.jpg' }}
                                    />
                                    <Image  
                                    onPress={this.props.GoToPlay}
                                        style={{ height : 40 , width : 40 , position : "absolute", top : "45%" }}
                                        source={require('./../../../../../assests/playIcon.png')} /> */}
                                </TouchableOpacity>
                            </View>


                            <View style={{ flex: 4.5, backgroundColor: primaryColor, flexDirection: 'column' }}>

                                <View style={{ flex: 4, backgroundColor: primaryColor, flexDirection: 'column' }}>
                                    <View style={{ flex: 1, justifyContent: 'center', paddingLeft: wp('3%') }}>
                                        <Text style={{ color: 'white', fontSize: wp('4.5%'), marginTop: wp('2%'), fontWeight: '600' }}>Mr. lele | In Cinema 31-01-2020</Text>
                                    </View>

                                    <View style={{ flex: 1, justifyContent: 'center', paddingLeft: wp('3%') }}>
                                        <Text style={{ color: secondaryColor, fontSize: wp('3.5%'), }}>#varunDhavan #ShahrukhKhan #akshayKumar</Text>
                                    </View>

                                    <View style={{ flex: 1, justifyContent: 'center', paddingLeft: wp('3%') }}>
                                        <Text style={{ color: 'grey', fontSize: wp('3.5%') }}>{'1729'} Likes - {'5'} Comments - {'0'} Share</Text>
                                    </View>

                                </View>

                                <View style={{ flex: 2, flexDirection: 'row' }}>

                                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                        <View style={{ width: wp('5%'), height: wp('5%'), }}>
                                            <Image style={{ width: '100%', height: '100%', backgroundColor: primaryColor, resizeMode: 'contain', tintColor: 'white' }} source={like}></Image>
                                        </View>
                                        <Text style={{ marginLeft: wp('2%'), color: 'white' }}>Like</Text>
                                    </View>

                                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                        <View style={{ width: wp('5%'), height: wp('5%'), }}>
                                            <Image style={{ width: '100%', height: '100%', backgroundColor: primaryColor, resizeMode: 'contain', tintColor: 'white' }} source={comment}></Image>
                                        </View>
                                        <Text style={{ marginLeft: wp('2%'), color: 'white' }}>Comment</Text>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                        <View style={{ width: wp('5%'), height: wp('5%'), }}>
                                            <Image style={{ width: '100%', height: '100%', backgroundColor: primaryColor, resizeMode: 'contain', tintColor: 'white' }} source={share}></Image>
                                        </View>
                                        <Text style={{ marginLeft: wp('2%'), color: 'white' }}>Share</Text>
                                    </View>

                                </View>
                            </View>
                        </View>} />
            </View>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'black',
    },
    fullScreen: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
    controls: {
        backgroundColor: "transparent",
        borderRadius: 5,
        position: 'absolute',
        bottom: 44,
        left: 4,
        right: 4,
    },
    progress: {
        flex: 1,
        flexDirection: 'row',
        borderRadius: 3,
        overflow: 'hidden',
    },
    innerProgressCompleted: {
        height: 20,
        backgroundColor: '#cccccc',
    },
    innerProgressRemaining: {
        height: 20,
        backgroundColor: '#2C2C2C',
    },
    generalControls: {
        flex: 1,
        flexDirection: 'row',
        overflow: 'hidden',
        paddingBottom: 10,
    },
    skinControl: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    rateControl: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    volumeControl: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    resizeModeControl: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    ignoreSilentSwitchControl: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    controlOption: {
        alignSelf: 'center',
        fontSize: 11,
        color: "white",
        paddingLeft: 2,
        paddingRight: 2,
        lineHeight: 12,
    },
    nativeVideoControls: {
        top: 184,
        height: 300
    }
});