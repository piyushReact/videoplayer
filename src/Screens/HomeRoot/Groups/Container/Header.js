import React, { Component } from 'react';
import { View, Text, StatusBar, Platform, Image, TouchableHighlight , TouchableOpacity} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { primaryColor, secondaryColor, grey } from '../../../../Common/Colors'


const leftmenu = require('../../../../../assests/groupIcon/leftMenu.png')
const search = require('../../../../../assests/groupIcon/search.png')
const notification = require('../../../../../assests/groupIcon/notification.png')
const rightmenu = require('../../../../../assests/groupIcon/rightMenu.png')
export default class header extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View>
                <StatusBar barStyle="light-content"></StatusBar>
                {Platform.OS == 'ios' ?
                    <View>
                        <View style={{ width: wp('100%'), height: hp('3.7%'), backgroundColor: 'black' }}></View>

                        <View style={{ width: wp('100%'), height: hp('6%'), backgroundColor: primaryColor, flexDirection: 'row' }}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <TouchableOpacity
                                onPress={this.props.LeftMenuopen}
                                style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                                    <Image style={{ width: '50%', height: '40%', tintColor: secondaryColor }} source={leftmenu}></Image>
                                </TouchableOpacity>
                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                                    <Image style={{ width: '40%', height: '40%', tintColor: secondaryColor }} source={search}></Image>
                                </View>
                            </View>

                            <View style={{ flex: 2, backgroundColor: primaryColor, justifyContent: 'center', alignItems: 'center', }}>
                                <Image style={{ height: '80%', width: wp('13%'), backgroundColor: secondaryColor }}></Image>
                            </View>

                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                                    <Image style={{ width: '40%', height: '40%', tintColor: secondaryColor }} source={notification}></Image>
                                </View>
                                <TouchableOpacity 
                                onPress={this.props.rightmenuOpen}
                                style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                                    <Image style={{ width: '50%', height: '70%', tintColor: secondaryColor }} source={rightmenu}></Image>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    :
                    <View style={{ width: wp('100%'), height: hp('6%'), backgroundColor: 'pink' }}>
                         </View>
                }
            </View>
        );
    }
}
