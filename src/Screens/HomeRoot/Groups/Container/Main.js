import React, { Component } from 'react';
import { View, Text, StatusBar, Image , SafeAreaView} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { Tab, Tabs } from 'native-base';
import TrendingUpdate from './TrendingUpdate'
import Timeline from './Timeline'
import { primaryColor, secondaryColor, grey } from '../../../../Common/Colors'
import Header from './Header'

const addIcon = require('../../../../../assests/groupIcon/add.png')

export default class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  GoToPlayVideo=()=>{
    this.props.navigation.navigate("Play")
  }
  OpenleftMenuDrawer=()=>{
    // alert("vfd")
    this.props.navigation.openDrawer();

  }
  openRightMenu=()=>{
    this.props.navigation.openDrawer();

  }
  goTocomment=()=>{
    // alert("check")
    this.props.navigation.navigate("globalComment")
  } 
  render() { 
    return (
        <SafeAreaView>
      <View style={{ flex: 1, }}>
        {/* <Header LeftMenuopen={this.OpenleftMenuDrawer} rightmenuOpen={this.openRightMenu}/> */}
        <View style={{ width: wp('100%'), height: hp('100%') }}>
          {/* <Tabs tabContainerStyle={{ borderBottomWidth: 0 }} tabBarUnderlineStyle={{ borderBottomWidth: 0 ,backgroundColor : "transparent" }}>
            <Tab heading="Album" textStyle={{ color: "white" }} activeTextStyle={{ color: secondaryColor, backgroundColor : "transparent" }} activeTabStyle={{marginLeft : wp("20%") }} tabStyle={{  borderBottomWidth: 0, marginLeft : wp("20%") }}> */}
              <TrendingUpdate GoToPlay={this.GoToPlayVideo} goToComment={this.goTocomment}/> 
            {/* </Tab>  
            <Tab heading="Studio" textStyle={{ color: "white" }} activeTextStyle={{ color: secondaryColor }} activeTabStyle={{marginRight : wp("20%") }} tabStyle={{  borderBottomWidth: 0, marginRight : wp("20%") }}> */}
              {/* <Timeline /> */}
            {/* </Tab>   
          </Tabs> */}
        </View>
        {/* <View style={{ width: wp('15%'), height: wp('15%'), borderRadius: wp('7.5%'), backgroundColor: secondaryColor, position: 'absolute', bottom: wp('3.5%'), right: wp('3.5%'), justifyContent: 'center', alignItems: 'center', }}>
          <Image style={{ width: '70%', height: '70%', tintColor: 'white' }} source={addIcon}></Image>
        </View> */}
      </View>
       </SafeAreaView>
    );
  }
}
