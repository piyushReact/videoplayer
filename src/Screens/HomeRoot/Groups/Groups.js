import React, { Component } from 'react'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    View, Text, TouchableWithoutFeedback, Keyboard, StyleSheet, TouchableOpacity,
    Image, ScrollView, RefreshControl, BackHandler, Alert
} from 'react-native';
import { Container } from 'native-base';
import Loader from '../../../Components/Loader';
import { textColor, mainblue, bgColor } from '../../../Common/Colors';
import Header from '../../../Components/Header';
import { API_LIST_GROUP, BASE_URL_PUBLIC, API_REMOVE_GROUP } from '../../../Common/APIConstant'
import { postApi, getApi } from '../../../Common/API'
import { PrintLog } from '../../../Components/PrintLog';
import Toast from 'react-native-simple-toast';
import AsyncStorage from '../../../Common/AsyncStorage'
// import Modal1 from "react-native-modal";
import Utils from "../../../Common/Utils";
import { NavigationEvents } from "react-navigation";
// import ModalExit from "react-native-modal";

import Video from 'react-native-video';

export default class Groups extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            finish: false,
            fName: "",
            lName: " ",
            feedStatus: true,
            feedData: [],
            refresh: false,
            isModalVisible: false,
            selectedDeleteId: '',
            Is_active: '',
            exitStatus: 0,
            ispaused : true,
            isShowPause : false,
            isPlay : false
        }

    }

    componentDidMount = async () => {
        await setTimeout(() => {
            // this.getGroupDetails()
        }, 600);
    }

    componentWillReceiveProps = async (nextProps) => {

        PrintLog("check props in groups ==> ", nextProps)
        await setTimeout(() => {
            // this.getGroupDetails()
        }, 500);
    }
    getGroupDetails = async () => {
        const AuthToken = await AsyncStorage.read("AuthToken")

        let apiHeaders = {
            headers: {
                "Accept": "application/json",
                "Authorization": AuthToken
            }
        };
        this.setState({ loading: true })
        getApi(API_LIST_GROUP, apiHeaders, this.successListGroup, this.errorApi);
    }
    successListGroup = async (response) => {
        this.setState({ loading: false, refresh: false })

        PrintLog("successListGroup ==> ", response)
        if (response.data.success == 200) {
            AsyncStorage.save("Is_active", response.data.Is_active)

            await this.setState({ feedData: response.data.data, Is_active: response.data.Is_active })

        } else if (response.data.status == 401) {
            setTimeout(() => {
                Toast.show(response.data.message)
                Utils.logout();
                this.props.navigation.navigate("Landing");
            }, 400)
        }
    }
    errorApi = (error) => {


        this.setState({ loading: false, refresh: false });

        PrintLog("Error : ", error);
        if (error.response) {
            PrintLog("Error response : ", error.response);
            if (error.response.status == 401) {
                // Utils.logout();
                // this.props.navigation.navigate("Landing");
            }
        } else {
            PrintLog("Error message : ", error.message);
            setTimeout(() => {
                Toast.show(error.message)
            }, 300)
        }


        // PrintLog("Error : ", error);
        // if (error.response) {
        //     PrintLog("Error response : ", error.response);
        // } else {
        //     PrintLog("Error message : ", error.message);
        // }
    }
    validation = () => {
        const { fName, lName } = this.state
        if (fName == "") {
            Toast.show("Please Fill First Name")
            return false;
        } else if (lName == "") {
            Toast.show("Please Fill Last Name")
            return false;
        }
        return true
    }
    onChangeText = (text) => {
        const { fName, lName, finish } = this.state
        // if (fName != "" && lName != "") {
        this.props.navigation.navigate("Welcome", { "welcomeFName": fName, "welcomeLName": lName })
        // } 
    }

    navigateBack = () => {
        this.props.navigation.goBack();
    }
    goBACK = () => {
        this.props.navigation.navigate("EnterOTP")
    }
    goBACK = () => {
        this.props.navigation.navigate("Groups")
    }
    goToCreateGroup = () => {
        // alert("vfvkldn")
        this.props.navigation.navigate("CreateGroup")
    }
    goToGroupDetails = (item) => {
        PrintLog("item --> -->", item)
        this.props.navigation.navigate("GroupDetails", { feedData: item })
    }
    onrefresh = () => {
        this.setState({ refresh: true })
        this.getGroupDetails()
    }
    goToDeleteGroup = (id) => {
        this.setState({ isModalVisible: true, selectedDeleteId: id })
    }
    goToDeleteGrp = async () => {
        const { selectedDeleteId } = this.state
        this.setState({ isModalVisible: false })
        const AuthToken = await AsyncStorage.read("AuthToken")

        let apiHeaders = {
            headers: {
                "Accept": "application/json",
                // "Content-Type": "application/json",
                "Authorization": AuthToken
            }
        };
        // const body = new FormData()
        this.setState({ loading: true })
        let body = new FormData()
        body.append("id", selectedDeleteId)

        postApi(API_REMOVE_GROUP, body, apiHeaders, this.successDeleteGroup, this.errorApi);

    }
    successDeleteGroup = (response) => {
        this.setState({ loading: false })

        PrintLog("successListGroup ==> ", response)
        if (response.data.success == 200) {

            this.getGroupDetails()

        } else {
            setTimeout(() => {
                Toast.show(response.data.message)
            }, 400)
        }
    }
 videoPause=()=>{
     this.setState({ ispaused : !this.state.ispaused , isPlay : !this.state.isPlay })
     setTimeout(()=>{
       this.setState({ isShowPause : false })
     },4000)
 }
  framePress=()=>{
     this.setState({ isShowPause : !this.state.isShowPause })
     setTimeout(()=>{
        this.setState({ isShowPause : false })
      },4000)
  }
    render() {
        const { feedStatus, feedData, Is_active } = this.state

        return (
            this.state.Is_active == 0 ?
                // feedData.length > 0 && feedData != "" && feedData != "undefined" && feedData != null && Is_active != "0" ?

                <Container style={{ flex: 1, backgroundColor: bgColor }}>
                    <NavigationEvents
                        onWillFocus={payload => {
                            PrintLog("checking payload ==> ", payload)

                            this.componentWillReceiveProps()
                        }}
                    />
                    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>

                        <View style={{ flex: 1, width: wp("100%"), fontSize: wp("20%") }}>
                            <View style={{ height: hp("20%"), width: wp("100%"), marginTop: 42 }}>
                            <TouchableOpacity 
                            activeOpacity={true}
                            onPress={this.framePress}
                             style={{ flex: 1, height : 90, width : wp("100%") , backgroundColor : "green", justifyContent : "center", alignItems : "center" }}>
                                <Video
                                paused={this.state.ispaused}
                                    source={{
                                        
                                        uri: "https://rawgit.com/mediaelement/mediaelement-files/master/big_buck_bunny.mp4"
                                    }
                                    // this.state.showLocal ?
                                    
                                    
                                }
                                    ref={player => {
                                        this.player = player;
                                    }}
                                    onEnd={() => { 
                                        this.player.seek(0);
                                    }}
                                    onError={(err) => {
                                        Alert.alert(JSON.stringify(err))
                                    }}
                                    style={{ flex: 1, height : 90, width : wp("100%") , backgroundColor : "green" }}
                                />
                                {
                                    this.state.isShowPause &&
                                    
                                <TouchableOpacity 
                               onPress={this.videoPause}
                                style={{ height : 30, width : 30,position: "absolute", top : 65 , left : wp("45%")}}>
                                    {
                                        this.state.ispaused ?
                                        <Image 
                                       style={{ height : 30, width : 30}} source={require("../../.././../assests/play.png")}/>
                                       :
                                       <Image 
                                      style={{ height : 40, width : 40}} source={require("../../.././../assests/paus.png")}/>
                                    }
                                   
                               </TouchableOpacity>
                         
                                }
                                </TouchableOpacity>
                            </View>
                            {/* <ScrollView
                                    refreshControl={
                                        <RefreshControl refreshing={this.state.refresh} onRefresh={() => this.onrefresh()} />
                                    }
                                    showsVerticalScrollIndicator={false}>
                                    {
                                        feedData.map((item, index) => {
                                            PrintLog("item , ", item)
                                            return (
                                                <TouchableOpacity
                                                    key={index}
                                                    onPress={() => this.goToGroupDetails(item)}
                                                    style={{ flex: 1, width: wp("100%"), }}>

                                                    <View

                                                        style={{
                                                            height: 100, backgroundColor: 'white', width: '88%', flexDirection: 'row', marginTop: 15, alignSelf: 'flex-end', borderWidth: 0.2, borderColor: '#D5D1D0', borderTopLeftRadius: 10, borderBottomLeftRadius: 10,
                                                            //ios    
                                                            shadowOpacity: 0.3, shadowRadius: 1, shadowOffset: { height: 0, width: 0 },
                                                            //android  elevation: 1
                                                        }}>
                                                        <View style={{ height: '100%', flex: 0.8, justifyContent: 'center', alignItems: 'center', }}>

                                                            {
                                                                item.grp_img != "" && item.grp_img != "undefined" && item.grp_img != null && item.grp_img != null ?
                                                                    <Image
                                                                        source={{ uri: BASE_URL_PUBLIC + item.grp_img }}
                                                                        style={{ marginRight: 50, width: 70, height: 70, backgroundColor: 'grey', borderRadius: 10 }}></Image>
                                                                    :
                                                                    <Image

                                                                        style={{ marginRight: 50, width: 70, height: 70, backgroundColor: 'grey', borderRadius: 10 }}
                                                                        source={require("../../../../assests/Groups/group-img.png")} />

                                                            }
                                                        </View>

                                                        <View style={{ flexDirection: 'column', flex: 3.7, justifyContent: 'center', }}>
                                                            <Text style={styles.item}>{item.key}</Text>
                                                            <Text style={[styles.item2, { fontSize: wp("5.2%"), fontWeight: "400", marginBottom: hp("2%") }]}>{item.grp_name}</Text>

                                                            <View style={{ flexDirection: 'row', }}>
                                                                {
                                                                    item.group_member.map((item1, index) => {
                                                                        PrintLog("inter in this", item1.get_user.User_pic)
                                                                        return item1.get_user.User_pic != "" && item1.get_user.User_pic != "undefined" && item1.get_user.User_pic != null && item1.get_user.User_pic != "0" ? <Image
                                                                            key={index}
                                                                            source={{ uri: BASE_URL_PUBLIC + item1.get_user.User_pic }}
                                                                            style={{ marginRight: -8, width: 20, height: 20, borderRadius: 10, borderRadius: 10 }}></Image>
                                                                            :
                                                                            <Image
                                                                                source={require("../../../../assests/loginRoot/user_profle_icon.png")}
                                                                                style={{ marginRight: -8, width: 20, height: 20, borderRadius: 10, backgroundColor: 'grey', borderRadius: 10 }}></Image>

                                                                    })
                                                                }

                                                                <Text style={{ marginLeft: wp("3%"), color: "grey" }}> {"+" + item.group_member.length} </Text>
                                                            </View>


                                                        </View>
                                                        <View style={{ height: "100%", justifyContent: "center", alignItems: "center" }}>
                                                            <Image source={require("../../../../assests/Groups/arrow-right.png")} resizeMode="contain" style={{ height: hp("3%"), width: wp("5%"), marginRight: wp("3%") }} />
                                                        </View>
                                                    </View>

                                                </TouchableOpacity>
                                                // </TouchableWithoutFeedback> 
                                            )
                                        })
                                    }

                                </ScrollView> */}

                            <TouchableOpacity
                                onPress={() => this.goToCreateGroup()}
                                style={{
                                    position: "absolute", right: wp("5%"), bottom: hp("2.5%"), height: hp("6%"), width: hp("6%"), borderRadius: hp("3%"),
                                    shadowOpacity: 0.3, shadowRadius: 3, shadowOffset: { height: 0, width: 0 },
                                }}
                            >
                                <Image
                                    style={{ height: hp("6%"), width: hp("6%"), borderRadius: hp("3%"), }}
                                    source={require("../../../../assests/Groups/add_icon.png")} />

                            </TouchableOpacity>

                        </View>

                    </TouchableWithoutFeedback>

                </Container>

                :
                <Container style={{ flex: 1, backgroundColor: bgColor }}>
                    <NavigationEvents
                        onWillFocus={payload => {
                            PrintLog("checking payload ==> ", payload)
                            // Is_active =="0" && this.getGroupDetails()
                            // console.log("will focus", payload);
                            this.componentWillReceiveProps()
                        }}
                    />
                    <Loader loading={this.state.loading} iconColor={mainblue} />
                    {/* <Header leftName={"Groups"} bgColorContatiner={bgColor} iconColor={mainblue} /> */}

                    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>


                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: { bgColor } }}>

                            <Text numberOfLines={2} style={{ position: "absolute", top: hp("15%"), fontSize: 20 }} onPress={() => this.props.navigation.navigate("Setting")}>{Is_active == "0" && "In Order To Activate Your Account"}</Text>
                            <Text numberOfLines={2} style={{ position: "absolute", top: hp("19%"), fontSize: 20, color: 'teal' }} onPress={() => this.props.navigation.navigate("Setting")}>{Is_active == "0" && "Go To Settings"}</Text>

                            <View style={{ width: '80%', height: '30%' }}>
                                <Image resizeMode={"contain"} source={require("../../../../assests/noRecordIcon/noGroup.png")}
                                    style={{ width: '100%', height: '100%' }}></Image>
                                <Text style={{ fontSize: 20, fontWeight: '400', color: 'teal', alignSelf: 'center', marginTop: '15%' }}>Sorry!</Text>
                                <Text style={{ fontSize: 20, fontWeight: '400', color: 'black', alignSelf: 'center' }}>{Is_active == "0" ? "Your account is deactivated" : "There are no groups record found"}</Text>
                            </View>


                            {
                                Is_active != "0" &&
                                <TouchableOpacity
                                    onPress={() => this.goToCreateGroup()}
                                    style={{
                                        position: "absolute", right: wp("5%"), bottom: hp("2.5%"), height: hp("6%"), width: hp("6%"), borderRadius: hp("3%"),
                                        shadowOpacity: 0.3, shadowRadius: 3, shadowOffset: { height: 0, width: 0 },
                                    }}
                                >
                                    <Image
                                        style={{ height: hp("6%"), width: hp("6%"), borderRadius: hp("3%"), }}
                                        source={require("../../../../assests/Groups/add_icon.png")} />

                                </TouchableOpacity>
                            }
                        </View>

                    </TouchableWithoutFeedback>

                </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: hp('7.5%'),
        width: '100%',
        marginLeft: -12,
    },
    profileContainer: {
        height: hp('7.5%'),
        width: '100%',
    },
    input: {
        height: hp('7.5%'),
        // paddingLeft: 4,
        fontSize: hp('2%'),
        color: textColor,
    },
    header: {
        // marginTop : 10,
        height: 90,
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'rgba(34,34,34,.8)'
        backgroundColor: 'white'
    },
    backgroundVideo: {
        borderWidth: 2,
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
    },
})