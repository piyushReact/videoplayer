import React, { Component } from 'react'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    View, Text, TouchableWithoutFeedback, Keyboard, StyleSheet, ImageBackground, TouchableOpacity,
    Platform, Image, TouchableHighlight, DeviceEventEmitter ,Linking ,Alert
} from 'react-native';
import { Container, Content, Form, Item, Input, Label } from 'native-base';
import Loader from '../../Components/Loader';
import { black, borderColor, grey, colorPrimary, textColor, mainblue } from '../../Common/Colors';
import Header from '../../Components/Header';
import { postApiWithoutHeader, postApi, getApi, getApiwithbody } from '../../Common/API';
import { STATUS_SUCCESS, API_LOGIN, API_RESEND_OTP, VERIFICATION_CHECK, VERIFICATION_TOKEN, X_AUTH, API_VERIFY_USER, API_REGISTER } from '../../Common/APIConstant';
import { PrintLog } from '../../Components/PrintLog';
import AsyncStorage from '../../Common/AsyncStorage';
import Toast from 'react-native-simple-toast'

// import ImagePicker from 'react-native-image-picker';
// import * as Permissions from 'react-native-permissions'
import { check, PERMISSIONS, RESULTS, request } from 'react-native-permissions';
import ImagePicker from 'react-native-image-crop-picker';
import ActionSheet from 'react-native-action-sheet';
var BUTTONSiOS = [
    'Choose Gallery',
    'Take Photo',
    'Cancel'
];

var BUTTONSandroid = [
    'Choose Gallery',
    'Take Photo',
    'Cancel'
];

export default class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            finish: false,
            fName: "",
            lName: "",
            countryCodeValue : "",
            userImage : ""
        }
    }
    componentDidMount=()=>{
        const SignUpData = this.props.navigation.getParam("SignUpData")
        PrintLog("SignUpData coming from Signup ==> ",SignUpData)
        this.setState({ countryCodeValue : SignUpData.countryCodeValue })
    }


    validation = () => {
        const { fName, lName, finish } = this.state
        if (fName == "") {
            Toast.show("Please fill the first name")
            return false;
        } else if (lName == "") {
            Toast.show("Please fill the last name")
            return false;
        }
        return true
    }
    onChangeText = (text) => {
        if (this.validation()) {
            this.goToCreateUserAccount()
        } 
    }
    goToCreateUserAccount = () => {
        const SignUpData = this.props.navigation.getParam("SignUpData")
        PrintLog("SignUpdata  ==> ", SignUpData)
        const { fName, lName , countryCodeValue, userImage } = this.state
       const body = new FormData()
        body.append("First_name", fName)
        body.append("Last_name", lName)
        body.append("Mobile_no", SignUpData.mobile)
        body.append("country_code", countryCodeValue)
        body.append("User_pic", userImage)

        
        PrintLog("create account body ==> ",body)

        postApiWithoutHeader(API_REGISTER, body, this.successCreateUserAccount, this.errorApi);
        
        
        
    }
    successCreateUserAccount = async(response) => {
        const { fName, lName } = this.state

        PrintLog("successCreateUserAccount ==> ", response)
        PrintLog("successCreateUserAccount ==> ", response.data.success)
        if (response.data.success == 200) {
            AsyncStorage.save("isLogin", true)
           await AsyncStorage.save("user_id", response.data.data.id)
        AsyncStorage.save("AuthToken", "Bearer "+response.data.data.token)           

            this.props.navigation.navigate("Welcome", { "welcomeFName": fName, "welcomeLName": lName })
        }else{
            // PrintLog("fvvf else")
        }
    } 
    errorApi = (error) => {
        this.setState({ loading: false });
        PrintLog("Error : ", error);
        if (error.response) {
            PrintLog("Error response : ", error.response);
        } else {
            PrintLog("Error message : ", error.message);
        }
    }

    navigateBack = () => {
        this.props.navigation.goBack();
    }
    goBACK = () => {
        this.props.navigation.navigate("EnterOTP")
    }
    goToselectProfileImage=()=>{
        this._checkPermissions()
    }

  //TODO CROP PICKER
  pickProfileImage = () => {
    ActionSheet.showActionSheetWithOptions({
        options: (Platform.OS == 'ios') ? BUTTONSiOS : BUTTONSandroid,
        cancelButtonIndex: 2,
        destructiveButtonIndex: 1,
        tintColor: mainblue
    },
        (buttonIndex) => {

            if (buttonIndex == 0) {
                ImagePicker.openPicker({
                    width: 400,
                    height: 400,
                    cropping: true
                }).then(image => {
                    console.log(image.path);
                    // this.setState({
                    //     imagePath: image.path
                    // })
                    let imageData = {
                        uri: image.path,
                        width: 300,
                        height: 300,
                        name: "captured_image.JPG",
                        type: "image/jpeg"
                    };
                    PrintLog("image data ==> ", imageData)
                    this.setState({ userImage : imageData })
                    // this.setState({ userImage1: imageData.uri, userImage: imageData })
                });

            } else if (buttonIndex == 1) {
                ImagePicker.openCamera({
                    width: 400,
                    height: 400,
                    cropping: true,
                }).then(image => {
                    // this.setState({
                    //     imagePath: image.path
                    // })

                    let imageData = {
                        uri: image.path,
                        width: 300,
                        height: 300,
                        name: "captured_image.JPG",
                        type: "image/jpeg"
                    };
                    PrintLog("image data ==> ", imageData)
    
                    this.setState({ userImage : imageData })
    
                    // this.setState({ userImage1: imageData.uri, userImage: imageData })
                });
            }
        });
}

//TODO IMAGE SELECT
_checkPermissions = () => {
    if (Platform.OS == "ios") {
        check(PERMISSIONS.IOS.PHOTO_LIBRARY)
            .then(result => {
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        console.log(
                            'This feature is not available (on this device / in this context)',
                        );
                        break;
                    case RESULTS.DENIED:
                        console.log('The permission has not been requested / is denied but requestable');
                        this._requestPermissions();
                        break;
                    case RESULTS.GRANTED:
                        console.log('The permission is granted');
                        this.pickProfileImage();
                        break;
                    case RESULTS.BLOCKED:

                        Alert.alert(
                            'Permissions',
                            'You have not Access Photos.Go to Settings and Allow Permissions',
                            [
                                {
                                    text: 'Cancel',
                                    onPress: () => console.log('Cancel Pressed'),
                                    style: 'cancel',
                                },
                                { text: 'OK', onPress: () => Linking.openURL('app-settings:') },
                            ],
                            { cancelable: false }
                        )


                        console.log('The permission is denied and not requestable anymore');
                        // Linking.openURL('app-settings:');
                        break;
                }
            })
            .catch(error => {
                console.log(error)
            });
    }
    else {
        check(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE)
            .then(result => {
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        console.log(
                            'This feature is not available (on this device / in this context)',
                        );
                        break;
                    case RESULTS.DENIED:
                        console.log('The permission has not been requested / is denied but requestable');
                        this._requestPermissions();
                        break;
                    case RESULTS.GRANTED:
                        console.log('The permission is granted');
                        this.pickProfileImage();
                        break;
                    case RESULTS.BLOCKED:

                        Alert.alert(
                            'Permissions',
                            'You have not Access Photos.Go to Settings and Allow Permissions',
                            [
                                {
                                    text: 'Cancel',
                                    onPress: () => console.log('Cancel Pressed'),
                                    style: 'cancel',
                                },
                                { text: 'OK', onPress: () => Linking.openURL('app-settings:') },
                            ],
                            { cancelable: false }
                        )

                        console.log('The permission is denied and not requestable anymore');
                        // Linking.openURL('app-settings:');
                        break;
                }
            })
            .catch(error => {
                console.log(error)
            });
    }
}


_requestPermissions = () => {
    if (Platform.OS == "ios") {
        request(PERMISSIONS.IOS.PHOTO_LIBRARY).then(result => {
            console.log('Getting Access');    //result== granted or blocked

        }).catch(error => {
            console.log(error)
        });
    } else {
        request(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE).then(result => {
            console.log('Getting Access');    //result== granted or blocked

        }).catch(error => {
            console.log(error)
        });
    }

}

    
    
    //   _selectImage = () => {
    
    //     const options = {
    //       title: 'Select Avatar',
    //       storageOptions: {
    //         skipBackup: true,
    //         path: 'images',
    //       },
    //       maxWidth : 250,
    //       maxHeight : 250,
    //       quality : 2,
    //     };
    
    //     ImagePicker.launchImageLibrary(options, (response) => {
    //       console.log('Response = ', response);
    
    //       if (response.didCancel) {
    //         console.log('User cancelled image picker');
    //       } else if (response.error) {
    //         console.log('ImagePicker Error: ', response.error);
    //       } else if (response.customButton) {
    //         console.log('User tapped custom button: ', response.customButton);
    //       } else {
    //         const source = { uri: response.uri };
    
    //         // You can also display the image using data:
    //         // const source = { uri: 'data:image/jpeg;base64,' + response.data };
    
    //             PrintLog("response Image ==> ", source)
    //             // You can also display the image using data:
    //             // const source = { uri: 'data:image/jpeg;base64,' + response.data };
    //             let imageData = {
    //                 uri: response.uri,
    //                 // width: 200,
    //                 width: response.width,
    //                 // height: 200,
    //                 height: response.height,
    //                 name: "captured_image.JPG",
    //                 type: "image/jpeg"
    //             };
    //             PrintLog("image data ==> ", imageData)
            
    //             this.setState({ userImage : imageData })
    //       }
    //     });
    //   }

    render() {
        return (
            <Container style={{ flex: 1 }}>
                <Loader loading={this.state.loading} />
                <Header back iconColor={"black"} bgColorContatiner={"white"} goBackPrevious={() => this.goBACK()} />

                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                    <View style={{ flex: 1, height: hp("100%"), width: wp("100%"), }}>

                        {/* <TouchableOpacity
                            style={{ marginLeft: hp("2%"), marginTop: hp("7%") }}
                            onPress={() => this.navigateBack()}>
                            <View style={{ padding: 8 }}>
                                <Image source={require("../../../assests/loginRoot/arrow_back_black.png")} style={{ height: hp("2.2%"), width: wp("8%") }} />
                            </View>
                        </TouchableOpacity> */}
                        <View style={{ marginLeft: hp("3%"), marginTop: hp("2%") }}>
                            <Text style={{ color: mainblue, fontWeight: "500", fontSize: hp("3%") }}> {"Profile"} </Text>
                            <Text style={{marginTop : hp("1%"), color : "grey" }}> {"Complete your profile"} </Text>
                        </View>
                        <View style={{ marginLeft: hp("3%"), marginTop: hp("4%"), flexDirection: "row" }}>
                            <TouchableOpacity 
                            onPress={()=> this.goToselectProfileImage()}
                            style={{ backgroundColor: mainblue, height: hp("10%"), width: hp("10%"), borderRadius: hp("5%") }}>
                                
                                {
                                    this.state.userImage == "" ? 
                                    <Image
                                    style={{ backgroundColor: mainblue, height: hp("10%"), width: hp("10%"), borderRadius: hp("5%") }}
                                    source={require("../../../assests/loginRoot/user_profle_icon.png")} 
                                    
                                    />
                                    :
                                    <Image
                                    style={{ backgroundColor: mainblue, height: hp("10%"), width: hp("10%"), borderRadius: hp("5%") }}
                                    source={{ uri : this.state.userImage.uri }} 
                                    
                                    />
                                }
                               

                            </TouchableOpacity>
                            <Text style={{ marginTop: hp("4%"), marginLeft: hp("3%"), color: mainblue }} > {"Upload Image"} </Text>
                        </View>
                        <View style={{ width: wp("100%"), justifyContent: "center", alignItems: "center" }}>
                            <Item floatingLabel style={{ width: wp("88%"), marginTop: hp("2.6%") }}>
                                <Label >{"First Name"}</Label>
                                <Input 
                                    autoCorrect={false}
                                    // maxLength={this.props.maxLength ? this.props.maxLength : 30}
                                    keyboardType={'default'}
                                    selectionColor={mainblue}
                                    // onEndEditing={() => this.onEndEditing()}
                                    // onFocus={() => this.onFocus()}
                                    // value={this.props.value}
                                    // defaultValue={this.props.value}
                                    underlineColorAndroid="transparent"
                                    // onChangeText={(text) => this.onChangeText(text)}
                                    onChangeText={(v1) => this.setState({ fName: v1 })}
                                    style={styles.input}
                                />
                            </Item>
                            <Item floatingLabel style={{ width: wp("88%"), marginTop: hp("2.6%") }}>
                                <Label >{"Last Name"}</Label>
                                <Input 
                                    autoCorrect={false}
                                    // maxLength={this.props.maxLength ? this.props.maxLength : 30}
                                    keyboardType={'default'}
                                    selectionColor={mainblue}
                                    // onEndEditing={() => this.onEndEditing()}
                                    // onFocus={() => this.onFocus()}
                                    // value={this.props.value}
                                    // defaultValue={this.props.value}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(v2) => this.setState({ lName: v2 })}
                                    style={styles.input}
                                />
                            </Item>
                        </View>
                        <View style={{ justifyContent: "center", alignItems: "center", marginTop: hp("22%"), height: hp("20%"), width: wp("100%") }}>
                            <TouchableOpacity
                                onPress={() => this.onChangeText()}
                                style={{
                                    height: 48, width: wp("90%"), justifyContent: "center", alignItems: "center", position: "absolute", bottom: hp("3%"), alignContent: "center", alignItems: "center", borderRadius: 42,
                                    backgroundColor: this.state.fName != "" && this.state.lName != "" ? mainblue : "grey"
                                }}>
                                <Text style={{ color: "white" }}> {"Done"} </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </TouchableWithoutFeedback>

            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height: hp('7.5%'),
        width: '100%',
        marginLeft: -12,
    },
    profileContainer: {
        height: hp('7.5%'),
        width: '100%',
    },
    input: {
        height: hp('7.5%'),
        // paddingLeft: 4,
        fontSize: hp('2%'),
        color: textColor,
    }
})