import React, { Component } from 'react';
import {
    View, Text, TouchableWithoutFeedback, Keyboard, StyleSheet, ImageBackground, TouchableOpacity,
    Image, TouchableHighlight, TextInput, ScrollView
} from 'react-native';
import { Container, Content } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Toast from 'react-native-simple-toast';
import CustomTextInput from '../../Components/TextInput';
import { txt_email, txt_login, txt_sign_up_account, txt_already_member, txt_logo, enter_phone_number, txt_mobile, txt_country_code, txt_send, txt_send_otp, enter_valid_number } from '../../Common/Constants';
import { black, grey, colorPrimary } from '../../Common/Colors';
import Loader from '../../Components/Loader';
import { API_LOGIN, STATUS_SUCCESS, API_RESEND_OTP } from '../../Common/APIConstant';
import { PrintLog } from '../../Components/PrintLog';
// import { onChangeNumber } from '../../Common/Utils';
// import ModalDropdown from 'react-native-modal-dropdown';
import CountryCodes from '../../Common/CountryCodes';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import LinearGradient from 'react-native-linear-gradient';

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            userName: "",
            Password: ""
        }
    }

    componentDidMount = () => {
    }

    navigateBack = () => {
        this.props.navigation.goBack();
    }

    navigateToSignup = () => {
        this.props.navigation.navigate("SignUp");
    }

    validateFields() {
        const { userName, Password } = this.state;
        if (userName == "") {
            Toast.show("Please enter email or Phone no");
            return false;
        } else if (Password == "") {
            Toast.show("PLease enter your password");
            return false;
        }
        return true;
    }

    onLoginPress = () => {
        const { userName, Password } = this.state;
        let body = {
            userName: userName,
            // code: txt_country_code,
            Password: Password,
        }
        if (this.validateFields()) {
            this.props.navigation.navigate("Groups")
        }
    }

    successLogin = (response) => {
        this.setState({ loading: false });
        PrintLog("Login Response : ", response);
        if (response.data.status === STATUS_SUCCESS) {
            // Toast.show(response.data.message);
            this.props.navigation.navigate("EnterOTP", { mobile: this.state.mobile, isFrom: "login" });
        } else {
            setTimeout(() => {
                Toast.show(response.data.message);
            }, 200);
        }
    }

    errorApi = (error) => {
        this.setState({ loading: false });
        PrintLog("Error : ", error);
        if (error.response) {
            PrintLog("Error response : ", error.response);
            if (error.response.status == 401) {
                setTimeout(() => {
                    Toast.show(error.response.data.message);
                }, 100);
            }
        } else {
            PrintLog("Error message : ", error.message);
        }
    }


    render() {
        const { loading, mobile, selectedIndex, selectedValue } = this.state;
        return (
            <KeyboardAwareScrollView>
                {/* <Container style={{ flex: 1 }}> */}
                <ImageBackground style={{ height: hp("100%"), width: wp("100%") }}
                    source={require("../../../assests/theam.jpg")}
                >
                    <Loader loading={loading} />
                    <View style={{ alignSelf: "center", height: hp("40%"), justifyContent: "center" }}>
                        <Image source={require("../../../assests/logo.png")} style={{ height: wp('46'), width: wp('60%'), marginTop: hp("10%") }} />

                    </View>
                    <View style={{ flex: 1 }}>

                        <View style={{ borderTopRightRadius: 12, borderTopLeftRadius: 12, backgroundColor: '#ffc966', height: hp("6%"), justifyContent: "center", borderBottomColor: "white", borderBottomWidth: 2, width: wp("90%"), alignSelf: "center" }}>
                            <Text style={{ alignSelf: "center", fontSize: 18, fontWeight: "bold" }}>LOGIN</Text>
                        </View>
                        <LinearGradient colors={['#FFF33B', '#FDC70C', '#ffa600']} style={{ backgroundColor: 'orange', flex: 1, width: wp("90%"), alignSelf: "center" }}>
                            <View style={{ flex: 1 / 5, width: "90%", alignSelf: "center", marginTop: hp("2%") }}>
                                <Text style={{ marginLeft: wp("2.5%"), fontSize: 15, color: "white" }}>EMAIL</Text>
                                <View style={{
                                    shadowOffset: { width: 3, height: 3, },
                                    shadowColor: 'black',
                                    shadowOpacity: 1.0, backgroundColor: "white", height: wp("12%"), width: "100%", borderRadius: 10, justifyContent: "center", marginTop: hp("1%")
                                }}> 
                                    <TextInput
                                        onChangeText={(value) => this.setState({ userName: value })}
                                        style={{ marginLeft: wp("5%") }}
                                        placeholder={"Please Enter Your Email"}
                                    />
                                </View>
                            </View>

                            <View style={{ flex: 1 / 5, width: "90%", alignSelf: "center", marginTop: hp('1%') }}>
                                <Text style={{ marginLeft: wp("2.5%"), fontSize: 15, color: "white", marginTop: hp("1%") }}>PASSWORD</Text>
                                <View style={{
                                    backgroundColor: "white", height: wp("12%"), width: "100%", borderRadius: 10, justifyContent: "center", marginTop: hp("1%")
                                    , shadowOffset: { width: 3, height: 3, },
                                    shadowColor: 'black',
                                    shadowOpacity: 1.0,
                                }}>
                                    <TextInput
                                        onChangeText={(value) => this.setState({ Password: value })}
                                        style={{ marginLeft: wp("5%") }}
                                        placeholder={"Please Enter Your Password"}
                                    />
                                </View>

                                <View style={{ height: 40, width: wp("88%"), alignSelf: "center", marginTop: hp("1%"), justifyContent: "space-between", alignItems: "center", flexDirection: "row" }}>

                                    <Text onPress={() => this.props.navigation.navigate("ForgotPass")} style={{ left: 10, fontSize: 13, color: "white" }}>Forgot Password?</Text>
                                    <View style={{ flexDirection: "row" }}>
                                        <Text style={{ color: "white", fontSize: 13 }}>New user?</Text>
                                        <Text onPress={this.navigateToSignup} style={{ color: "black", fontSize: 13, marginLeft: wp("1%") }}>SIGNUP</Text>
                                    </View>
                                </View>

                                <TouchableOpacity onPress={() => this.onLoginPress()}>
                                    <LinearGradient
                                        colors={['#FFF33B','#FDC70C', '#F3903F', '#ED683C']}

                                        style={{
                                            marginTop: hp("1%"), backgroundColor: "green", height: 42, shadowOffset: { width: 3, height: 3, },
                                            shadowColor: 'black',
                                            shadowOpacity: 1.0, width: wp("40%"), justifyContent: "center", alignItems: "center", alignSelf: "center", borderRadius: 10
                                        }}>
                                        <Text style={{ fontSize: 19, color: "white", fontWeight: "600", }}>Login</Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                                <View style={{ marginTop: hp("2%"), justifyContent: "center", flexDirection: "row" }}>
                                    <View style={{ width: wp("20%"), height: hp("0%"), borderWidth: 1, marginTop: "3.5%", borderColor: "white" }}></View>
                                    <Text style={{ color: "white", fontSize: 15 }}>OR LOGIN WITH</Text>
                                    <View style={{ width: wp("20%"), height: hp("0%"), borderWidth: 1, marginTop: "3.5%", borderColor: "white" }}></View>
                                </View>
                                <View style={{ height: hp("10%"), flexDirection: "row", alignItems: "center", justifyContent: "center" }}>
                                    <TouchableOpacity>
                                        <Image source={require("../../../assests/FB-icon.png")} style={styles.shareBtn} />
                                    </TouchableOpacity>
                                    <TouchableOpacity>
                                        <Image source={require("../../../assests/Insta.png")} style={styles.shareBtn} />
                                    </TouchableOpacity>
                                    <TouchableOpacity>
                                        <Image source={require("../../../assests/Google.png")} style={styles.shareBtn} />
                                    </TouchableOpacity>
                                    <TouchableOpacity>
                                        <Image source={require("../../../assests/Wechat.png")} style={styles.shareBtn} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </LinearGradient>
                    </View>
                    {/* </TouchableWithoutFeedback> */}
                </ImageBackground>
                {/* </Container> */}
            </KeyboardAwareScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

        // justifyContent: 'center',
    },
    textStyle: {
        color: black,
        fontSize: hp("2%")
    },
    textHintStyle: {
        color: colorPrimary,
        fontSize: hp('2.8%'),
        margin: hp('2%')
    },
    bottomView: {
        // position: 'absolute',
        // bottom: hp('6%'),
        marginTop: hp('4%'),
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    shareBtn: {
        height: wp("15%"), width: wp("15%"), shadowOffset: { width: 3, height: 3, },
        shadowColor: 'black',
        shadowOpacity: 1.0
    }
})