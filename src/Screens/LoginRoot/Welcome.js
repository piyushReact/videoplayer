import React, { Component } from "react";

import { View, Text, ImageBackground, StyleSheet, TouchableWithoutFeedback, TouchableOpacity, Image, Keyboard, DeviceEventEmitter } from 'react-native'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Container, Content, Form, Item, Input, Label } from 'native-base';
import Loader from '../../Components/Loader';
import { black, borderColor, grey, colorPrimary, textColor, mainblue } from '../../Common/Colors';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/AntDesign';
import { PrintLog } from "../../Components/PrintLog";
import AsyncStorage from '../../Common/AsyncStorage'

export default class welcome extends Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: false,
            welcomeFName : "",
            welcomeLName : ""
        }
    }

    componentDidMount=()=>{
        PrintLog("componentDidMount")
       const welcomeFName = this.props.navigation.getParam("welcomeFName")
       const welcomeLName = this.props.navigation.getParam("welcomeLName")
       PrintLog("welcomeFName ==> ",welcomeFName)
       PrintLog("welcomeLName ==> ",welcomeLName)
        // this.listener = DeviceEventEmitter.addListener("customerName", (fName , lName)=>{
            if(welcomeFName != "undefined" && welcomeFName != "" && welcomeFName != null && welcomeLName != "undefined" && welcomeLName != "" && welcomeLName != null){
                this.setState({ welcomeFName : welcomeFName , welcomeLName : welcomeLName})
                // PrintLog("fname and lname is ==> ",fName+" "+lName)
            }else{
                PrintLog("fName and lName is not getting")
            }
        // })
    }


    // static getDerivedStateFromProps(nextProps, prevState){
    //     if(nextProps.someValue!==prevState.someValue){
    //       return { someState: nextProps.someValue};
    //    }
    //    else return null;
    //  }


    //  componentWillReceiveProps(props){
    //     PrintLog("componentWillReceiveProps")
    //     this.listener = DeviceEventEmitter.addListener("customerName", (fName , lName)=>{
    //         if(fName != "undefined" && fName != "" && fName != null && lName != "undefined" && lName != "" && lName != null){
    //             this.setState({ welcomeName : fName + lName })
    //             return null
    //             PrintLog("fname and lname is from getDerivedStateFromProps==> ",fName+" "+lName)
    //         }else{
    //             PrintLog("fName and lName is not getting from getDerivedStateFromProps")
    //             return null
    //         }
    //     })
    // }

    gotomain=()=>{
        this.props.navigation.navigate("Groups")
    }
    render() {
        const { loading , welcomeFName, welcomeLName } = this.state
        return (
            <Container style={{ flex: 1 }}>
                {/* <ImageBackground style={{ height: "100%", width: "100%", justifyContent: 'center' }} */}
                {/* source={require("../../../assets/images/background.png")}> */}
                <Loader loading={loading} />
                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                    {/* <Content style={{ flex: 1 }}> */}
                        <View style={styles.container}>

                            <TouchableOpacity style={{ marginTop : hp("25%") ,height: hp("10%"), width: hp("10%"), borderRadius: hp("5%") }}>
                                <Image
                                    style={{ height: hp("10%"), width: hp("10%"), borderRadius: hp("5%") }}
                                    source={require("../../../assests/loginRoot/Congratulation_icon.png")} />

                            </TouchableOpacity>
                            <View style={{marginTop : hp("5%"), justifyContent : "center" , alignItems : "center"}}>
                            <Text style={{ color : mainblue , fontSize: 25, fontWeight: "800" }}> {"Congratulations"} </Text>
                            <Text  style={{ marginTop : hp("1%"), fontSize : hp("3%") }} > {welcomeFName + " " + welcomeLName} </Text>
                            <Text style={{ marginTop : hp("4%"), color : "grey" }}> {"Great to have you on board!"} </Text>
                           </View>
                            <Button 
                            onPress={()=> this.gotomain()}
                           buttonStyle={{ backgroundColor : mainblue , marginTop : hp("10%") , width : wp("50%"), borderRadius : 42, marginBottom : hp("10%")}}
                                title="Done"
                                loading={false}
                                iconRight={true}
                                icon={
                                    <Icon
                                    style={{ marginTop : hp("0.7%") , marginLeft : wp("1%") }}
                                      name="arrowright"
                                      size={15}
                                      color="white"
                                    />
                                  }
                            />
                            {/* <IconRightArrow name="verticleright"/> */}
                             <Image resizeMode={"contain"}
                                    style={{ height: hp("10%"), width: hp("10%"), borderRadius: hp("5%") ,position : "absolute", bottom : hp("5%")}}
                                    source={require("../../../assests/loginRoot/logo.png")} />
                          
                        </View>
                       
                    {/* </Content> */}
                </TouchableWithoutFeedback>

            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        // backgroundColor: "green",
        height: hp("100%"),
        width:"100%"

        // justifyContent: 'center',
    }
})

