import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet, Image, Platform, Modal } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { black, colorPrimary, grey, transparent, filterBackground, settingBackground, white, main, mainblue } from '../../Common/Colors';
// import { Button } from 'react-native-elements';
import { Button, Icon } from 'native-base';

export default class Landing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: ''
        }
    }

    linkedinAction = () => { }

    facebookAction = () => { }

    googleAction = () => { }

    goToSignUp = (id) => {
        if(id == 1){
            this.props.navigation.navigate("Login" , { "type" : "Login"})
        }else if(id == 2){
            this.props.navigation.navigate("SignUp", { "type" : "SignUp"})
        }

    }

    render() {
        return (
            <ImageBackground style={{ height: "100%", width: "100%", justifyContent: 'center' }}
                source={require("../../../assests/loginRoot/bg.png")}>
                {/* {Platform.OS === 'android' &&
                    <MyStatusBar backgroundColor={settingBackground} />
                } */}
                <View style={{ flex: 1, alignItems: 'center' }} >
                <View style={{ height : 80, width : wp("80%"), justifyContent : "center", alignItems : "center", alignSelf : "center", marginTop : hp("40%") }}>
                    <Text style={{ fontSize : 25 }}>Logo</Text>
                </View>
                    {/* <Image
                        resizeMode="contain"
                        style={{ height: hp("40%"), width: wp("40%") }}
                        source={require("../../../assests/loginRoot/brand.png")} /> */}
                </View>
                <View style={styles.bottomAreaView}>
                    <TouchableOpacity style={styles.bottonView} onPress={() => this.goToSignUp(1)}>
                        <Text style={[styles.textStyle, { color : "orange" }]}>{"Sign In"}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.goToSignUp(2)}
                        style={[styles.bottonView, {  backgroundColor: "orange" }]}>
                        <Text style={[styles.textStyle, { color : "black" }]}>{"Sign Up"}</Text>
                    </TouchableOpacity>

                </View>
                <View style={{ height : 80, width : wp("80%"), justifyContent : "center", alignItems : "center", alignSelf : "center" }}>
                    <Text style={{ fontSize : 18 }}>OR</Text>
                </View>
                <Button iconLeft transparent primary style={{ height : 38 , width : wp("80%"), justifyContent : "center", alignSelf : "center", marginBottom : hp("3%"), borderRadius : 10 }}>
                <Image
          style={{width: 20, height: 20, right : 10, tintColor : "white"}}
          source={require('../../../assests/facebook.png')}
        />
            <Text style={{ color : "white", fontWeight : "600", fontSize : 18}}>Continue with Facebook</Text>
          </Button> 
                {/* <Button
                    
                    title="Button with icon component"
                /> */}

            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    textStyle: {
        color: "white",
        fontSize: 18,
        fontWeight: "400",
        
    },
    textHintStyle: {
        color: colorPrimary,
        fontSize: hp("2.2%"),
        fontSize: hp('1.8%'),
        margin: hp('2%')
    },
    bottonView: {

        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 42,

        height: 49,
        width: wp("30%"),
        backgroundColor: transparent,
        borderWidth : 1,
        borderColor : "orange"
    },
    bottomAreaView: {
        flexDirection: "row",
        position: 'absolute',
        bottom: hp('12%'),
        width: wp("80%"),
        height: hp("17%"),
        justifyContent: "space-around",
        // alignContent: "center",
        alignItems: "center",
        alignSelf : "center"


    }
})