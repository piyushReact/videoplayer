import React, { Component } from 'react';
import {
    View, Text, TouchableWithoutFeedback, Keyboard, StyleSheet, ImageBackground, TouchableOpacity,
    Image, TextInput, Alert
} from 'react-native';
import { Container, Content, Form, Item, Input, Label } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
// import CustomTextInput from '../../Components/TextInput';
import { enter_otp, txt_enter_otp, txt_resend, txt_verify, txt_country_code, txt_login } from '../../Common/Constants';
import { black, grey, textColor, colorPrimary, borderColor, mainblue, main } from '../../Common/Colors';
import Button from '../../Components/Button';
// import IconAntDesign from 'react-native-vector-icons/AntDesign';
import CommonStyles from '../../Common/CommonStyles';
import AsyncStorage from '../../Common/AsyncStorage';
// import { } from '../../Common/Utils';
import Loader from '../../Components/Loader';
import Toast from 'react-native-simple-toast';
import { postApiWithoutHeader , postApi , getApi , getApiwithbody} from '../../Common/API';
import { STATUS_SUCCESS, API_LOGIN, API_RESEND_OTP , VERIFICATION_CHECK, VERIFICATION_TOKEN , X_AUTH , API_VERIFY_USER} from '../../Common/APIConstant';
import { PrintLog } from '../../Components/PrintLog';
import Header from '../../Components/Header';

export default class EnterOTP extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            email: '',
            pin1: '', pin2: '', pin3: '', pin4: '',
            countryCodeValue : +91,
            mobile : 0,
            seconds : 30
        }
        this.otp = "";
    }

    componentDidMount=()=>{
        const SignUpData = this.props.navigation.getParam("SignUpData")
        PrintLog("SignUpData coming from Signup ==> ",SignUpData)
        this.interval = setInterval(() => this.tick(), 1000);

        this.setState({ countryCodeValue : SignUpData.countryCodeValue , mobile : SignUpData.mobile })
    }

    //TODO COUNTER
    tick() {
        PrintLog("tick")
        if(this.state.seconds < 1){
         PrintLog("Value is less then 0 so we can not update now")
        }else{
            this.setState(prevState => ({
                
                seconds : prevState.seconds - 1
            }));
        }
      }

      componentWillUnmount() {
        clearInterval(this.interval);
      }


    navigateBack = () => {
        this.props.navigation.goBack();
    }

    validate = () => {
        const { pin1, pin2, pin3, pin4 } = this.state; 
        if (pin1 == '' || pin2 == '' || pin3 == '' || pin4 == '') {
            Toast.show(enter_otp);
            
        } else {
            this.otp = pin1 + pin2 + pin3 + pin4;
            this.callVerifyApi()
        }
    }

    onKeyboardPress(key, textInput) {
        if (key == "Backspace") {
            if (textInput == "second") {
                this.refs.first.focus();
            } else if (textInput == "third") {
                this.refs.second.focus();
            } else if (textInput == "fourth") {
                this.refs.third.focus();
            }
        } else {
            if (textInput == "first") {
                this.refs.second.focus();
            } else if (textInput == "second") {
                this.refs.third.focus();
            } else if (textInput == "third") {
                this.refs.fourth.focus();
            }
        }
    }

    checkCharacter(text) {
        let numbers = '0123456789';
        return numbers.indexOf(text) > -1;
    }


    callResendOTP = () => {
        const {  mobile, countryCodeValue } = this.state;
        PrintLog("mobile no ==> ",mobile)
        PrintLog("Country Code : ", countryCodeValue)
        let body = {
            phone_number : mobile,
            country_code : countryCodeValue,
            via : "sms"
        }

        let apiHeaders = {
            headers : {
              "Accept": "application/json",
              "X-Authy-API-Key": X_AUTH
            }
          }

   
        PrintLog("body ==> ",body)
        PrintLog("Header ==> ",apiHeaders)

            this.setState({ loading: true });
            postApi(VERIFICATION_TOKEN, body, apiHeaders , this.successRegister, this.errorApi);
    }

    successRegister = (response) => {
        this.setState({ seconds : 30 })

        const {  mobile, countryCodeValue } = this.state;
        this.setState({ loading: false });
        PrintLog("Resend Code Response : ", response);
        this.props.navigation.navigate("EnterOTP", { SignUpData : { mobile , countryCodeValue } })
    }

    errorApi = (error) => {
        this.setState({ loading: false });
        PrintLog("Error : ", error);
        if (error.response) {
            PrintLog("Error response : ", error.response);
        } else {
            PrintLog("Error message : ", error.message);
        }
    }

    goToCheckVerification=()=>{
        this.setState({ loading : true })
        const { pin1, pin2 , pin3 , pin4 , countryCodeValue , mobile } = this.state
        const verification_code = pin1 + pin2 + pin3 + pin4 
  
      
        let apiHeaders = {
            headers : {
              "X-Authy-API-Key": X_AUTH
            }
          }
          getApi(`https://api.authy.com/protected/json/phones/verification/check?verification_code=${verification_code}&phone_number=${mobile}&country_code=${countryCodeValue}&via=sms`, apiHeaders, this.resultverificaton, this.errorApiVerification);
    }

    resultverificaton = (response) => {
        this.setState({ loading : false })

        PrintLog("Response verification : ", response);
        if (response.status == 200) {
            setTimeout(() => {
                this.checkDbUser()
            }, 200);
        }
    
    }

    errorApiVerification = (error) => {
        this.setState({ loading: false });
        PrintLog("Error : ", error);
        if (error.response) {
            PrintLog("bgkdklnlionknknjknkl")
            PrintLog("Error response : ", error.response);
            PrintLog("printlog ==> ",error.response.data.errors.message)
            setTimeout(()=>{
                alert(error.response.data.errors.message)
            },300)
        } 
    }

    checkDbUser=()=>{
        const { mobile } = this.state
        const body = new FormData()
        body.append("phone-no",mobile)
        postApiWithoutHeader(API_VERIFY_USER, body, this.successcheckDbUser, this.errorApi);

    } 
    successcheckDbUser=(response)=>{
        PrintLog("db response check user ==> ",response)
        const { mobile , countryCodeValue } = this.state
       if(response.data.success == 200){ 
        AsyncStorage.save("isLogin", true)
           AsyncStorage.save("user_id", response.data.data.id)

        AsyncStorage.save("AuthToken", "Bearer "+response.data.data.token)
        this.props.navigation.navigate("Groups") 
       }else{
        this.props.navigation.navigate("Profile", { SignUpData : { mobile , countryCodeValue }})            
       }
            // PrintLog("successcheckDbUser ==> ",response)
    }

    goBACK=()=>{
        this.props.navigation.navigate("SignUp")
    }
    

    render() {
        const { loading, pin1, pin2, pin3, pin4 } = this.state;
        return (
            <Container style={{ flex: 1 }}>
                {/* <ImageBackground style={{ height: "100%", width: "100%", justifyContent: 'center' }} */}
                {/* source={require("../../../assets/images/background.png")}> */}
                <Loader loading={loading} />
                <Header back iconColor={"black"}  bgColorContatiner ={"white"} goBackPrevious={()=> this.goBACK()}/>

                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                    <Content style={{ flex: 1 }}>
                        <View style={styles.container}>
                            {/* <TouchableOpacity onPress={() => this.navigateBack()}
                                style={CommonStyles.backButtonContainer} >
                                <View style={{ padding: 8, marginTop: hp("0.8%") }}>
                                    <Image source={require("../../../assests/loginRoot/arrow_back_black.png")} style={{ height: hp("2.2%"), width: wp("8%") }} />
                                </View>

                            </TouchableOpacity> */}
                            <View style={{ position: "absolute", top: hp("5%"), left: wp("5%") }}>
                                <Text style={{ color: mainblue, fontWeight: "500", fontSize: hp("3%") }}>{"Verification"}</Text>
                                <Image
                                    resizeMode="contain"
                                    source={require("../../../assests/loginRoot/verification_icon.png")} style={{ height: hp("6.3%"), width: wp("8%"), marginTop: hp("3%") }} />

                                <Text style={{ color: "grey", fontSize: hp("2.1%"), marginTop: hp("3%") }}>{"We have sent you an access code "}</Text>
                                <Text style={{ color: "grey", fontSize: hp("2.1%") }}>{"via SMS for Mobile number verifications"}</Text>
                                <Text style={{ color: "grey", fontSize: hp("2.1%"), marginTop: hp("4%") }}>{"Enter code here"}</Text>

                            </View>
                            <TouchableWithoutFeedback
                                onPress={Keyboard.dismiss}
                                accessible={false}>
                                <View style={{
                                    marginTop: hp("45%"), flexDirection: 'row', justifyContent: 'space-evenly',
                                    height: hp('7.5%'), width: wp('80%')
                                }}>
                                    <TextInput style={styles.otpText}
                                        secureTextEntry={true}
                                        ref="first"
                                        caretHidden={false}
                                        maxLength={1}
                                        keyboardType='number-pad'
                                        selectionColor={colorPrimary}
                                        underlineColorAndroid='transparent'
                                        value={pin1}
                                        onChangeText={(pin1) => {
                                            if (this.checkCharacter(pin1)) {
                                                this.setState({ pin1 })
                                                if (pin1) {
                                                    this.refs.second.focus();
                                                }
                                            }
                                        }}
                                        onKeyPress={(e) => this.onKeyboardPress(e.nativeEvent.key, 'first')}
                                        returnKeyType={"next"} />
                                    <TextInput style={styles.otpText}
                                        secureTextEntry={true}
                                        ref="second"
                                        caretHidden={false}
                                        maxLength={1}
                                        keyboardType='number-pad'
                                        selectionColor={colorPrimary}
                                        underlineColorAndroid='transparent'
                                        value={pin2}
                                        onChangeText={(pin2) => {
                                            if (this.checkCharacter(pin2)) {
                                                this.setState({ pin2 })
                                                if (pin2) {
                                                    this.refs.third.focus();
                                                }
                                            }
                                        }}
                                        onKeyPress={(e) => this.onKeyboardPress(e.nativeEvent.key, 'second')}
                                        returnKeyType={"next"} />
                                    <TextInput style={styles.otpText}
                                        secureTextEntry={true}
                                        ref="third"
                                        caretHidden={false}
                                        maxLength={1}
                                        keyboardType='number-pad'
                                        selectionColor={colorPrimary}
                                        underlineColorAndroid='transparent'
                                        value={pin3}
                                        onChangeText={(pin3) => {
                                            if (this.checkCharacter(pin3)) {
                                                this.setState({ pin3 })
                                                if (pin3) {
                                                    this.refs.fourth.focus();
                                                }
                                            }
                                        }}
                                        onKeyPress={(e) => this.onKeyboardPress(e.nativeEvent.key, 'third')}
                                        returnKeyType={"next"} />
                                    <TextInput style={styles.otpText}
                                        secureTextEntry={true}
                                        ref="fourth"
                                        caretHidden={false}
                                        maxLength={1}
                                        keyboardType='number-pad'
                                        selectionColor={colorPrimary}
                                        underlineColorAndroid='transparent'
                                        value={pin4}
                                        onChangeText={(pin4) => {
                                            if (this.checkCharacter(pin4)) {
                                                this.setState({ pin4, resendOTP: '' });
                                                if (pin4 != '') {
                                                    Keyboard.dismiss();
                                                } else {
                                                    this.refs.third.focus();
                                                }
                                            }
                                        }}
                                        onKeyPress={(e) => this.onKeyboardPress(e.nativeEvent.key, 'fourth')}
                                        returnKeyType={"next"} />
                                </View>

                            </TouchableWithoutFeedback>
                            <View style={{ height : 42 , width : wp("100%") , justifyContent : "space-between" , flexDirection : "row" , marginTop : hp("3%")  }}>
                            <Text style={{ marginLeft : wp("5%") }}>{"00."+this.state.seconds}</Text>    
                            <TouchableOpacity 
                            onPress={()=> this.state.seconds < 1 ? this.callResendOTP(): ""}
                                >
                                <Text style={{
                                     fontSize: hp('2%'), marginRight : wp("5%"),
                                    fontWeight: '500', color : this.state.seconds < 1 ? "black" : "grey" 
                                }}>{"Resend code"}</Text>
                            </TouchableOpacity>
                             </View>
                            {/* <Button isWhite title={txt_login} top={hp("4%")}
                                onPress={() => this.validate()} /> */}
                        </View>
                    </Content>
                </TouchableWithoutFeedback>
                {/* </ImageBackground> */}
                <View style={{  width : wp("100%") , height : hp("20%"), justifyContent : "center" , alignItems : "center" , alignContent : "center"}}>
                <TouchableOpacity style={{ borderRadius : 48 , backgroundColor : mainblue , justifyContent : "center" , alignItems : "center" , height : 48 ,  width : wp("90%") }}
                                // onPress={() => this.validate()} 
                                onPress={() => this.goToCheckVerification()} 
                                >
                                <Text style={{
                                   color : "white"
                                }}>{"Next"}</Text>
                            </TouchableOpacity>
                            </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        // justifyContent: 'center',
    },
    backButtonContainer: {
        position: "absolute",
        top: hp("6%"),
        left: wp("5%"),
    },
    textStyle: {
        color: textColor,
        fontSize: hp("2%")
    },
    textHintStyle: {
        color: colorPrimary,
        fontSize: hp('1.8%'),
        margin: hp('2%')
    },
    otpText: {
        height: hp("6%"), width: hp('6.4%'), borderBottomWidth: 1, borderColor: borderColor, color: textColor,
        borderRadius: 8, textAlign: 'center', padding: 10, fontWeight: 'bold', fontSize: hp('2%'),
    },
})