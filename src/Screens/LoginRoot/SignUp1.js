import React, { Component } from 'react';
import {
    View, Text, TouchableWithoutFeedback, Keyboard, StyleSheet, ImageBackground, TouchableOpacity,
    Platform, Image, TouchableHighlight, Linking, TextInput, FlatList
} from 'react-native';
import { Container, Content, Form, Item, Input, Label } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import CustomTextInput from '../../Components/TextInput';
import { txt_full_name, txt_email, txt_mobile, txt_password, txt_sign_up, txt_date_of_birth, txt_already_member_login, enter_name, enter_email, enter_valid_email, enter_phone_number, enter_password, select_date_of_birth, txt_or, txt_new_member, txt_country_code, enter_valid_number } from '../../Common/Constants';
import { black, borderColor, grey, colorPrimary, textColor, mainblue } from '../../Common/Colors';
import CommonStyles from '../../Common/CommonStyles';
import { emailValidation, onChangeNumber } from '../../Common/Utils';
import AsyncStorage from '../../Common/AsyncStorage';
import Button from '../../Components/Button';
import moment from 'moment';
import Toast from 'react-native-simple-toast';
import Loader from '../../Components/Loader';
import DeviceInfo from 'react-native-device-info';
import { postApiWithoutHeader, postApi } from '../../Common/API';
import { API_REGISTER, STATUS_SUCCESS, VERIFICATION_TOKEN, API_VERIFY_USER, ACCOUNT_SID, AUTH_TOKEN, VERIFICATION_CHECK_NEW, X_AUTH } from '../../Common/APIConstant';
import { PrintLog } from '../../Components/PrintLog';
import ModalDropdown from 'react-native-modal-dropdown';
import CountryCodes from '../../Common/CountryCodes';
import Header from '../../Components/Header';
import Modal from 'react-native-modal'
import IconEnt from 'react-native-vector-icons/Entypo'
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scrollview'

let currentDate = new Date();
let date = moment(currentDate)
    .subtract(13, "y")
    .toDate();
let maxDate = moment(currentDate).toDate();

export default class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            name: '',
            email: '',
            mobile: '',
            password: '',
            dob: '',
            selectedIndex: 0,
            selectedValue: "645095", countryCodeValue: '+966',
            type: "",
            data: [],
            noData: false,
            isVisible: false,

        },
        arrayholder = [];

        // this.data = []
    }

    componentDidMount = async () => {
        
        const type = this.props.navigation.getParam("type")
        this.data = CountryCodes.map(item => {
            return item.code;
        });
        this.setState({ selectedIndex: this.data.indexOf("+91"), selectedValue: "645095" });
        
        this.setState({ type: type, data: CountryCodes })
        this.arrayholder = CountryCodes
    }

    searchText = (text) => {
        const newData = this.arrayholder.filter(item => {      
            const itemData = `${item.name.toUpperCase()}   
             ${item.code.toUpperCase()}`;
            
             const textData = text.toUpperCase();
              
             return itemData.indexOf(textData) > -1;    
          });
          
          this.setState({ data: newData }); 
       
    }

    get_country = async(countryName, countryCode, countryDialCode) => {
        // alert(countryName + "  ::  " + countryCode + "  ::  " + countryDialCode)
       await this.setState({ isVisible: false , countryCodeValue : countryDialCode})
    }

    navigateBack = () => {
        this.props.navigation.goBack();
    }

    validateFields = () => {
        const { mobile, } = this.state;

        if (mobile === "") {
            Toast.show(enter_phone_number);
            return false;
        } else if (mobile.length < 4) {
            Toast.show(enter_valid_number);
            return false;
        }
        // else if (password === "") {
        //     Toast.show(enter_password);
        //     return false;
        // } else if (dob === "") {
        //     Toast.show(select_date_of_birth)
        //     return false;
        // }
        return true;
    }
    //TODO TEMP

    checkDbUser = () => {
        const { mobile } = this.state
        const body = new FormData()
        body.append("phone-no", mobile)
        this.setState({ loading: true })
        postApiWithoutHeader(API_VERIFY_USER, body, this.successcheckDbUser, this.errorApi);

    }
    successcheckDbUser = async(response) => {
        this.setState({ loading: false })
        PrintLog("db response check user ==> ", response)
        const { mobile, countryCodeValue } = this.state
        if (response.data.success == 200) {
          await  AsyncStorage.save("isLogin", true)
           await AsyncStorage.save("user_id", response.data.data.id)

            AsyncStorage.save("AuthToken", "Bearer " + response.data.data.token)
            this.props.navigation.navigate("Groups")
        } else {
            this.props.navigation.navigate("Profile", { SignUpData: { mobile, countryCodeValue } })
        }
        // PrintLog("successcheckDbUser ==> ",response)
    }





    //TODO TEMP



    onSignupPress = () => {
        const { mobile, countryCodeValue } = this.state;
        PrintLog("mobile no ==> ", mobile)
        PrintLog("Country Code : ", countryCodeValue)
        let body = {
            // code: countryCodeValue,
            // phone_number: mobile,
            // FriendlyName : "New Awali"
            // device_token: DeviceInfo.getUniqueID(),
            // device_type: Platform.OS,
            // latitude: "",
            // longitude: "",
            phone_number: mobile,
            country_code: countryCodeValue,
            via: "sms"
        }
        // var Username = ACCOUNT_SID;
        // var Password = AUTH_TOKEN;
        // var credentials = btoa(Username + ':' + Password);
        // var basicAuth = 'Basic ' + credentials;

        // const apiHeader = {
        //     headers: {
        //         "Authorization": basicAuth,

        //     }
        // }
        // headers: {
        //     'Authorization': 'Bearer ' + DEMO_TOKEN
        //   }

        let apiHeaders = {
            headers: {
                "Accept": "application/json", 
                "X-Authy-API-Key": X_AUTH
            }
        }


        PrintLog("body ==> ", body)
        PrintLog("Header ==> ", apiHeaders)
        // PrintLog("credentials ==> ",credentials)

        if (this.validateFields()) {
            this.setState({ loading: true });
            postApi(VERIFICATION_TOKEN, body, apiHeaders, this.successRegister, this.errorApi);
        }
    }

    successRegister = (response) => {
        const { mobile, countryCodeValue } = this.state;
        this.setState({ loading: false });
        PrintLog("Register Response : ", response);
        this.props.navigation.navigate("EnterOTP", { SignUpData: { mobile, countryCodeValue } })
    }

    errorApi = (error) => {
        this.setState({ loading: false });
        PrintLog("Error : ", error);
        if (error.response) {
            setTimeout(() => {
                Toast.show(error.response.data.message);
            }, 300)

            // Toast.show(error.response.data.message, 200)
            PrintLog("Error response : ", error.response.data.message);
            // alert(error.response.data.message)
            // Toast.show("error.response.data.message")

        } else {
            PrintLog("Error message : ", error.message);
            // alert(error.response.data.message)
            Toast.show(error.message, 200)

        }
    }

    navigateToLogin = () => {
        this.props.navigation.navigate("Login");
    }

    onDatePress = () => {
        let dueDate = false;
        this.refs.dateDialog.open({
            date: dueDate ? dueDate : date,
            maxDate: date
        });
    };

    onDatePicked = date => {
        let mSelectedDate = ""
        if (!isNaN(date)) {
            mSelectedDate = moment(date).format('YYYY-MM-DD');
        }
        this.setState({
            dob: mSelectedDate
        });
    }; 

    onFocus = () => {
        this.onDatePress();
    }

    code_dropdown_renderRow = (rowData, rowID, highlighted) => {
        let data = CountryCodes.map(item => {
            if (item.code === rowData) {
                return item.name + " : " + item.dial_code;
            }
        });
        return (
            <TouchableHighlight underlayColor='silver'>
                <View style={{ backgroundColor: "green", width: wp("83%"), flexDirection: 'row', height: 50, alignItems: 'center', backgroundColor: '#FDFEFE' }}>
                    <Text style={{ fontSize: 13, color: black, width: '100%', padding: 8 }}>{data}</Text>
                </View>
            </TouchableHighlight>
        );
    }

    dropdown_onSelect = (idx, value) => {
        CountryCodes.forEach((obj) => {
            if (obj.code === value) {
                this.setState({
                    selectedValue: obj.dial_code,
                    countryCodeValue: obj.dial_code
                });
            }
        });
    }

    renderButtonText = (value) => {
        let data = CountryCodes.map(item => {
            if (item.code === value) {
                return item.dial_code;
            }
        });
        return data;
    }

    goToEnterOtp = () => {
        this.props.navigation.navigate("EnterOTP")
    }
    goBACK = () => {
        this.props.navigation.navigate("Landing")
    }

    goTochangeCtrCode=(item)=>{
        this.get_country(item.name, item.code, item.dial_code)
        this.setState({ countryCodeValue : item.code, isVisible : false })
    }
    render() {
        const { loading, mobile, selectedIndex, type } = this.state;
        return (
            <Container style={{ flex: 1 }}>
                <Loader loading={loading} />
                <Header back iconColor={"black"} bgColorContatiner={"white"} goBackPrevious={() => this.goBACK()} />
                <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                
                    {/* <Content style={{ flex: 1 }}> */}
                    <View style={styles.container}>
                      

                        <View style={{
                            position: "absolute",
                            top: hp("3%"),
                            left: wp("5%")
                        }}>
                            <Text style={{ color: mainblue, fontWeight: "500", fontSize: hp("3%") }}>{type}</Text>
                            <Image resizeMode="contain" source={require("../../../assests/loginRoot/mobile_icon.png")} style={{ height: hp("6.3%"), width: wp("8%"), marginTop: hp("3%") }} />
                            <Text style={{ color: "grey", fontSize: hp("2.1%"), marginTop: hp("3%") }}>{"What's your mobile number"}</Text>



                            <View style={{
                                flexDirection: 'row', width: '96%', borderBottomWidth: 1, borderBottomColor: "grey",
                                marginTop: hp("2.1%"),
                            }}> 
                                <TouchableOpacity style={{ 
                                    flexDirection: 'row', width: '20%', marginTop: hp("2.5%"),
                                    alignItems: 'center', height: hp("7.5%"), justifyContent: 'center',
                                }}
                                    onPress={() =>this.setState({ isVisible : true })}>
                            
                                    <Text style={{ fontSize : 14, marginTop : 10 }} >{this.state.countryCodeValue}</Text>
                                </TouchableOpacity>

                                <Item floatingLabel style={{ width: wp("80%"), marginTop: hp("2.5%"), height: hp("7.5%") }}>
                                    {/* <Label style={{ borderBottomColor : "white" }}>{"Enter your mobile number"}</Label> */}
                                    <Input
                                        maxLength={15}

                                        autoCorrect={false}
                                        // maxLength={this.props.maxLength ? this.props.maxLength : 30}
                                        keyboardType={'number-pad'}
                                        selectionColor={mainblue}
                                        // onEndEditing={() => this.onEndEditing()}
                                        // onFocus={() => this.onFocus()}
                                        // value={this.props.value}
                                        // defaultValue={this.props.value}
                                        placeholder={"Enter your mobile number"}
                                        underlineColorAndroid="transparent"
                                        onChangeText={(v2) => this.setState({ mobile: v2 })}
                                        style={[styles.input, { borderBottomColor: "white" }]}
                                    />
                                </Item>



                                <Modal
                                    isVisible={this.state.isVisible}
                                    animationIn='fadeInUp'
                                > 
                                <KeyboardAwareScrollView
                                showsVerticalScrollIndicator={false}
                                >
                                    <View style={{ width: wp('90%'), height: hp('70%'), padding: hp('1%'), backgroundColor: 'white', alignSelf: 'center', borderRadius: 20, marginTop : hp("2%") }}>
                                        <View style={{ width: wp("80%"), alignSelf: 'center', flexDirection : "row" }}><Text style={{ fontSize: 22, fontWeight: '500' }}>Select Country</Text>
                                        <IconEnt name="cross" onPress={()=> this.setState({ isVisible : false })} style={{ position : "absolute", right : 0 , padding : 5 }} size={25}  />
                                        </View>
                                        <View style={{ width: wp("80%"), paddingBottom: hp('1%'), marginTop: hp('2.5%'), alignSelf: 'center', borderBottomWidth: 1, borderBottomColor: '#0e64d1' }}>
                                            <TextInput onChangeText={(text) => this.searchText(text)} style={{ fontSize: 20 }} placeholder='search'></TextInput>
                                        </View>
                                        {/* {noData == true ?
                    <View style={{ flex: 1, backgroundColor: 'red', marginTop: hp('5%'), alignItems: 'center' }}><Text>No Record Found</Text></View>
                    : */}
                                        <FlatList
                                        showsVerticalScrollIndicator ={false}
                                            style={{ marginTop: hp('3%') }}
                                            data={this.state.data}
                                            renderItem={({ item }) =>
                                                <TouchableOpacity onPress={() => this.get_country(item.name, item.code, item.dial_code)} style={{ flexDirection: 'row', height: hp('6%'), }}>
                                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                                        <View style={{ width: '50%', height: '40%', }}>
                                                            <Image source={{ uri: item.uri }} resizeMode="contain" style={{ width: '100%', height: '100%' }}></Image>
                                                        </View>
                                                    </View>

                                                    <View style={{ flex: 3, justifyContent: 'center' }}>
                                                        <Text style={{ fontSize: 18, }}>{item.name} ({item.code})</Text>
                                                    </View>

                                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                                                        <Text style={{ fontSize: 18, }}>{item.dial_code}</Text>
                                                    </View>

                                                </TouchableOpacity>}
                                        />
                                    </View>
                                    </KeyboardAwareScrollView>
                                </Modal>

                               

                            </View>
                            <View style={{ marginLeft: - 20, marginTop: 20, height: 50, width: wp("100%"), justifyContent: "center", alignItems: "center" }}>
                                <Text style={{ color: "grey", fontSize: hp("1.8%") }}>{"In Future, you'll use this number to login."}</Text>
                                <Text style={{ color: "grey", fontSize: hp("1.8%"), }}>{"We'll send a verification code"}</Text>
                            </View>
                            <View style={{ position: "absolute", top: hp("62%"), justifyContent: "center", alignItems: "center" }}>

                                <Text style={{ color: "grey", fontSize: hp("1.8%"), marginTop: hp("3%") }}>{'By clicking the "Next" Button below, you agree to'}</Text>
                                <Text style={{ color: "black", fontSize: hp("1.8%"), }}>

                                    <Text style={{ textDecorationLine: "underline", height: 20, width: 80 }}
                                        onPress={() => Linking.openURL('http://13.235.77.70/Terms-condition')}>{"Our Terms"}</Text>
                                    {/* <View style={{  width : 60}}>   */}
                                    <Text style={{ color: "grey" , marginLeft : wp("5%")}}>{"  and  "}</Text>
                                    {/* </View> */}
                                    <Text style={{ textDecorationLine: "underline", height: 20, width: 80, marginLeft : wp("5%") }}
                                        onPress={() => Linking.openURL('http://13.235.77.70/policy')} > Privacy Policy</Text>

                                </Text>


                                <TouchableOpacity 
                                    onPress={() => this.onSignupPress()}
                                    style={{ marginTop: hp("2%"), height: 48, width: wp("90%"), borderRadius: 42, backgroundColor: mainblue, justifyContent: "center", alignContent: "center", alignItems: "center" }}>
                                    <Text style={{ color: "white", fontSize: hp("2.5%") }}> {"Next"} </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>


                    {/* </Content> */}
                </TouchableWithoutFeedback>

            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        width: '100%',
        alignSelf: 'center',
    },
    dateContainer: {
        height: hp('7.5%'),
        width: wp('80%'),
        marginLeft: -8,
        marginTop: hp("1%"),
    },
    input: {
        height: hp('7.5%'),
        paddingLeft: 4,
        fontSize: hp('2%'),
        color: textColor,
    },
    label: {
        fontSize: hp('1.8%'),
        color: grey,
        // paddingLeft: 4,
    },
    bottomView: {
        // position: 'absolute',
        // bottom: hp('6%'),
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: hp("4%"),
        marginBottom: hp("4%")
    },
    textStyle: {
        fontSize: hp("2.2%"),
        color: colorPrimary
    },
    textHintStyle: {
        color: colorPrimary,
        fontSize: hp('1.8%'),
        margin: hp('2%')
    },
})