import React, { Component } from 'react';
import {
    View, Text, TouchableWithoutFeedback, Keyboard, StyleSheet, ImageBackground, TouchableOpacity,
    Image, TouchableHighlight, TextInput, ScrollView
} from 'react-native';
import { Container, Content } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Toast from 'react-native-simple-toast';
import CustomTextInput from '../../Components/TextInput';
import { txt_email, txt_login, txt_sign_up_account, txt_already_member, txt_logo, enter_phone_number, txt_mobile, txt_country_code, txt_send, txt_send_otp, enter_valid_number } from '../../Common/Constants';
import { black, grey, colorPrimary } from '../../Common/Colors';
import Button from '../../Components/Button';
import Loader from '../../Components/Loader';
import CommonStyles from '../../Common/CommonStyles';
import AsyncStorage from '../../Common/AsyncStorage';
import { postApiWithoutHeader } from '../../Common/API';
// import IconAntDesign from 'react-native-vector-icons/AntDesign';
// import IconEntypo from 'react-native-vector-icons/Entypo';
import { API_LOGIN, STATUS_SUCCESS, API_RESEND_OTP } from '../../Common/APIConstant';
import { PrintLog } from '../../Components/PrintLog';
// import { onChangeNumber } from '../../Common/Utils';
// import ModalDropdown from 'react-native-modal-dropdown';
import CountryCodes from '../../Common/CountryCodes';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'
import LinearGradient from 'react-native-linear-gradient';

export default class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            email: "",
        }
    }

    componentDidMount = () => {
    }

    navigateBack = () => {
        this.props.navigation.goBack();
    }

    navigateToSignup = () => {
        this.props.navigation.navigate("SignUp");
    }

    validateFields() {
        const { userName, Password } = this.state;
        if (userName == "") {
            Toast.show("Please enter email or Phone no");
            return false;
        } else if (Password == "") {
            Toast.show("PLease enter your password");
            return false;
        }
        return true;
    }

    onLoginPress = () => {
        const { userName, Password } = this.state;
        let body = {
            userName: userName,
            Password: Password,
        }

        if (this.validateFields()) {
            alert("under development")
        }
    }

    successLogin = (response) => {
        this.setState({ loading: false });
        PrintLog("Login Response : ", response);
        if (response.data.status === STATUS_SUCCESS) {
            this.props.navigation.navigate("EnterOTP", { mobile: this.state.mobile, isFrom: "login" });
        } else {
            setTimeout(() => {
                Toast.show(response.data.message);
            }, 200);
        }
    }

    errorApi = (error) => {
        this.setState({ loading: false });
        PrintLog("Error : ", error);
        if (error.response) {
            PrintLog("Error response : ", error.response);
            if (error.response.status == 401) {
                setTimeout(() => {
                    Toast.show(error.response.data.message);
                }, 100);
            }
        } else {
            PrintLog("Error message : ", error.message);
        }
    }


    render() {
        const { loading, mobile, selectedIndex, selectedValue } = this.state;
        return (
            <KeyboardAwareScrollView>
                {/* <Container style={{ flex: 1 }}> */}
                <ImageBackground style={{ height: hp("100%"), width: wp("100%") }}
                    source={require("../../../assests/theam.jpg")}
                >
                    <TouchableOpacity onPress={() => this.navigateBack()}
                        style={CommonStyles.backButtonContainer} >
                        <View style={{ padding: 8 }}>
                            <Image style={{ height: 40, width: 40 }} source={require('../../../assests/back.png')} />
                        </View>
                    </TouchableOpacity>
                    <Loader loading={loading} />
                    <View style={{ alignSelf: "center", height: hp("35%"), justifyContent: "center" }}>
                        <Image source={require("../../../assests/logo.png")} style={{ height: wp('46'), width: wp('60%'), marginTop: hp("20%") }} />

                    </View>
                    <View style={{ backgroundColor: 'white', height: hp('50%'), width: wp('100%'), top: hp('60%'), position: 'absolute' }} />
                    <View style={{ flex: 1, marginTop: hp("7%"), }}>

                        <View style={{ borderTopRightRadius: 12, borderTopLeftRadius: 12, backgroundColor: '#ffbf00', height: hp("12%"), justifyContent: "space-evenly", borderBottomColor: "white", borderBottomWidth: 2, width: wp("90%"), alignSelf: "center" }}>
                            <Image source={require("../../../assests/forgotPass/Forgot.png")} style={{ alignSelf: "center" }} />
                            <Text style={{ alignSelf: "center", fontSize: 18, fontWeight: "bold" }}>FORGOT PASSWORD?</Text>
                        </View>
                        <LinearGradient colors={['#FDC70C', '#F3903F', '#ED683C']} style={{ backgroundColor: 'orange', flex: 1, width: wp("90%"), alignSelf: "center" }}>
                            <View style={{ justifyContent: "center", alignItems: "center", width: wp("100%"), alignSelf: "center", marginTop: hp("2%") }}>
                                <Text style={{ color: "white", fontSize: 13 }} >We just need your registered email address</Text>
                                <Text style={{ color: "white", fontSize: 13 }} >to send you password reset</Text>
                            </View>

                            <View style={{ flex: 1 / 5, width: "90%", alignSelf: "center", marginTop: hp("2%") }}>
                                <Text style={{ marginLeft: wp("2.5%"), fontSize: 15, color: "white" }}>EMAIL</Text>
                                <View style={{
                                    backgroundColor: "white", height: wp("12%"), width: "100%", borderRadius: 12, justifyContent: "center", marginTop: hp("1%"),
                                    shadowOffset: { width: 3, height: 3, },
                                    shadowColor: 'black',
                                    shadowOpacity: 1.0,
                                }}>
                                    <TextInput
                                        style={{ marginLeft: wp("2%") }}
                                        placeholder={"Please Enter Your Email"}
                                    />
                                </View>



                                <TouchableOpacity onPress={() => this.onLoginPress()}>
                                    <LinearGradient
                                        colors={['#e3da5f', '#ED683C']}

                                        style={{
                                            marginTop: hp("5%"), backgroundColor: "green", height: 42, shadowOffset: { width: 3, height: 3, },
                                            shadowColor: 'black',
                                            shadowOpacity: 1.0, width: wp("40%"), justifyContent: "center", alignItems: "center", alignSelf: "center", borderRadius: 10
                                        }}>
                                        <Text style={{ fontSize: 12, color: "white", fontWeight: "600", }}>RESET PASSWORD</Text>
                                    </LinearGradient>
                                </TouchableOpacity>
                                <View style={{ justifyContent: "center", alignItems: "center", width: wp("75%"), alignSelf: "center", position: "absolute", bottom: -hp("24%") }}>
                                    <Text style={{ color: "white", fontSize: 13 }} >DON'T HAVE AN ACCOUNT?</Text>
                                    <Text style={{ color: "white", fontSize: 13 }} >REGISTER</Text>
                                </View>

                            </View>
                            
                        </LinearGradient>
                        
                    </View>
                    {/* </TouchableWithoutFeedback> */}
                </ImageBackground>
                {/* </Container> */}
            </KeyboardAwareScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

        // justifyContent: 'center',
    },
    textStyle: {
        color: black,
        fontSize: hp("2%")
    },
    textHintStyle: {
        color: colorPrimary,
        fontSize: hp('2.8%'),
        margin: hp('2%')
    },
    bottomView: {
        // position: 'absolute',
        // bottom: hp('6%'),
        marginTop: hp('4%'),
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
    },
    shareBtn: {
        height: wp("15%"), width: wp("15%"), shadowOffset: { width: 3, height: 3, },
        shadowColor: 'black',
        shadowOpacity: 1.0
    }
})