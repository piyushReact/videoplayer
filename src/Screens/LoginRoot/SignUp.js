import React, { Component } from 'react';
import {
    View, Text, TouchableWithoutFeedback, Keyboard, StyleSheet, ImageBackground, TouchableOpacity,
    Image, TouchableHighlight, TextInput
} from 'react-native';
import { Container, Content } from 'native-base';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import Toast from 'react-native-simple-toast';
import { txt_logo } from '../../Common/Constants';
import { black, grey, colorPrimary } from '../../Common/Colors';
import Button from '../../Components/Button';
import Loader from '../../Components/Loader';
import CommonStyles from '../../Common/CommonStyles';
import { API_LOGIN, STATUS_SUCCESS, API_RESEND_OTP } from '../../Common/APIConstant';
import { PrintLog } from '../../Components/PrintLog';
import CountryCodes from '../../Common/CountryCodes';
import { signup_bg } from '../../Common/ImagePath'
import LinearGradient from 'react-native-linear-gradient';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview'

export default class Signin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            fullName: "",
            userName: "",
            Password: "",
            confirmPass: "",
            refCode: "",
            condi: false,
        }
        this.data = []
    }

    componentDidMount = () => {
        this.data = CountryCodes.map(item => {
            return item.code;
        });

        this.setState({ selectedIndex: this.data.indexOf("+91"), selectedValue: "645095" });
    }

    navigateBack = () => {
        this.props.navigation.goBack();
    }

    navigateToSignup = () => {
        this.props.navigation.navigate("SignUp");
    }

    validateFields() {
        const { fullName, userName, Password, confirmPass, refCode, condi } = this.state;
        if (fullName.trim() == "") {
            Toast.show("Please enter Full Name");
            return false;
        } else if (userName.trim() == "") {
            Toast.show("Please enter email or phone no");
            return false;
        } else if (Password.trim() == "") {
            Toast.show("Please enter password");
            return false;
        } else if (confirmPass.trim() == "") {
            Toast.show("please enter confirm password");
            return false;
        } else if (Password.trim() != confirmPass.trim()) {
            Toast.show("Password and confirm password should be match");
            return false;
        } else if (condi == false) {
            Toast.show("Please accept term and condition");
            return false;
        }
        return true;
    }

    onFinalPress = () => {
        const { fullName, userName, Password, confirmPass, refCode, condi } = this.state;
        let body = {
            fullName: fullName.trim(),
            userName: userName.trim(),
            Password: Password.trim(),
            confirmPass: confirmPass.trim(),
            refCode: refCode.trim(),
            condi: condi,

        }

        if (this.validateFields()) {
            this.setState({ loading: true });
            alert(JSON.stringify(body))
            // postApiWithoutHeader(API_RESEND_OTP, body, this.successLogin, this.errorApi);
        }
    }

    successLogin = (response) => {
        this.setState({ loading: false });
        PrintLog("Login Response : ", response);
        if (response.data.status === STATUS_SUCCESS) {
            // Toast.show(response.data.message);
            this.props.navigation.navigate("EnterOTP", { mobile: this.state.mobile, isFrom: "login" });
        } else {
            setTimeout(() => {
                Toast.show(response.data.message);
            }, 200);
        }
    }

    errorApi = (error) => {
        this.setState({ loading: false });
        PrintLog("Error : ", error);
        if (error.response) {
            PrintLog("Error response : ", error.response);
            if (error.response.status == 401) {
                setTimeout(() => {
                    Toast.show(error.response.data.message);
                }, 100);
            }
        } else {
            PrintLog("Error message : ", error.message);
        }
    }

    render() {
        const { loading, userName, fullName, Password, confirmPass, refCode, condi } = this.state;
        return (
            // <Container style={{ flex: 1 }}>
            <KeyboardAwareScrollView>
                <ImageBackground style={{ fleheight: hp("100%"), width: wp("100%"), justifyContent: 'center' }}
                    source={signup_bg}>
                    <Loader loading={loading} />
                    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
                        <View style={styles.container}>
                            <TouchableOpacity onPress={() => this.navigateBack()}
                                style={CommonStyles.backButtonContainer} >
                                <View style={{ padding: 8 }}>
                                    <Image style={{ height: 40, width: 40 }} source={require('../../../assests/back.png')} />
                                </View>
                            </TouchableOpacity>
                            <View style={{ height: hp("10%"), width: wp("100%"), marginTop: hp("15%") }}>
                                <Text></Text>
                            </View>
                            <View style={{backgroundColor:'white', height:hp('60%'),width:wp('100%'),top:hp('60%'),position:'absolute'}}/>
                            <View style={{ flex: 1 }}>
                                <View style={{ borderTopRightRadius: 12, borderTopLeftRadius: 12, backgroundColor: '#FDC70C', height: hp("6%"), justifyContent: "center", borderBottomColor: "white", borderBottomWidth: 1, width: wp("90%"), alignSelf: "center" }}>
                                    <Text style={{ alignSelf: "center", fontSize: 18, fontWeight: "bold" }}>REGISTER</Text>
                                </View>
                                <LinearGradient colors={['#FDC70C', '#F3903F', '#ED683C']} style={{ backgroundColor: 'orange', flex: 1, width: wp("90%"), alignSelf: "center" }}>
                                    <View style={{ flex: 1 / 8, width: "90%", alignSelf: "center", marginTop: hp("2%") }}>
                                        <Text style={{ fontSize: 12, color: "white" }}>FIRST NAME</Text>
                                        <View style={{
                                            shadowOffset: { width: 3, height: 3, },
                                            shadowColor: 'black',
                                            shadowOpacity: 1.0, backgroundColor: "white", height: wp("12%"), width: "100%", borderRadius: 10, justifyContent: "center"
                                        }}>
                                            <TextInput
                                                style={{ marginLeft: wp("2%") }}
                                                placeholder={"FIRST NAME"}
                                            />
                                        </View>
                                    </View>
                                    <View style={{ flex: 1 / 8, width: "90%", alignSelf: "center", marginTop: hp("2.5%") }}>
                                        <Text style={{ fontSize: 12, color: "white" }}>LAST NAME</Text>
                                        <View style={{
                                            shadowOffset: { width: 3, height: 3, },
                                            shadowColor: 'black',
                                            shadowOpacity: 1.0, backgroundColor: "white", height: wp("12%"), width: "100%", borderRadius: 10, justifyContent: "center"
                                        }}>
                                            <TextInput
                                                style={{ marginLeft: wp("2%") }}
                                                placeholder={"LAST NAME"}
                                            />
                                        </View>
                                    </View>
                                    <View style={{ flex: 1 / 8, width: "90%", alignSelf: "center", marginTop: hp("2.5%") }}>
                                        <Text style={{ fontSize: 12, color: "white" }}>EMAIL</Text>
                                        <View style={{
                                            shadowOffset: { width: 3, height: 3, },
                                            shadowColor: 'black',
                                            shadowOpacity: 1.0, backgroundColor: "white", height: wp("12%"), width: "100%", borderRadius: 10, justifyContent: "center"
                                        }}>
                                            <TextInput
                                                style={{ marginLeft: wp("2%") }}
                                                placeholder={"EMAIL"}
                                            />
                                        </View>
                                    </View>
                                    <View style={{ flex: 1 / 8, width: "90%", alignSelf: "center", marginTop: hp("2.5%") }}>
                                        <Text style={{ fontSize: 12, color: "white" }}>PASSWORD</Text>
                                        <View style={{
                                            shadowOffset: { width: 3, height: 3, },
                                            shadowColor: 'black',
                                            shadowOpacity: 1.0, backgroundColor: "white", height: wp("12%"), width: "100%", borderRadius: 10, justifyContent: "center"
                                        }}>
                                            <TextInput
                                                style={{ marginLeft: wp("2%") }}
                                                placeholder={"PASSWORD"}
                                            />
                                        </View>
                                    </View>

                                    <View style={{ flex: 1 / 8, width: "90%", alignSelf: "center", marginTop: hp("2.5%") }}>
                                        <Text style={{ fontSize: 12, color: "white" }}>CONFIRM PASSWORD</Text>
                                        <View style={{
                                            backgroundColor: "white", height: wp("12%"), width: "100%", borderRadius: 10, justifyContent: "center",
                                            shadowOffset: { width: 3, height: 3, },
                                            shadowColor: 'black',
                                            shadowOpacity: 1.0,
                                        }}>
                                            <TextInput
                                                style={{ marginLeft: wp("2%") }}
                                                placeholder={"CONFIRM PASSWORD"}
                                            />
                                        </View>

                                        {/* <View style={{ height: 40, width: wp("88%"), alignSelf: "center", marginTop: hp("1%"), justifyContent: "space-between", alignItems: "center", flexDirection: "row" }}>

                                                <Text onPress={() => this.props.navigation.navigate("ForgotPass")} style={{ left: 10, fontSize: 15, color: "white" }}>Forgot Password?</Text>
                                                <View style={{ flexDirection: "row" }}>
                                                    <Text style={{ color: "white", fontSize: 15 }}>New user?</Text>
                                                    <Text onPress={this.navigateToSignup} style={{ color: "black", fontSize: 15, marginLeft: wp("1%") }}>SIGNUP</Text>
                                                </View>
                                            </View> */}

                                        <TouchableOpacity onPress={() => alert("under development")}
                                            style={{
                                                shadowColor: 'black',
                                                shadowOffset: { width: 3, height: 3, },
                                                shadowOpacity: 1,
                                                elevation: 50,
                                                marginTop:hp('2%')
                                            }}>
                                            <LinearGradient
                                                colors={['#e3da5f', '#ED683C']}
                                                style={{
                                                    marginTop: hp("1%"), backgroundColor: "green", height: 42, shadowOffset: { width: 3, height: 3, },
                                                    shadowColor: 'black',
                                                    shadowOpacity: 1.0, width: wp("40%"), justifyContent: "center", alignItems: "center", alignSelf: "center", borderRadius: 10
                                                }}>
                                                <Text style={{ fontSize: 16, color: "white", fontWeight: "600", }}>REGISTER</Text>
                                            </LinearGradient>
                                        </TouchableOpacity>
                                        <View style={{ marginTop: hp("2%"), justifyContent: "center", flexDirection: "row" }}>
                                            <View style={{ width: wp("20%"), height: hp("0%"), borderWidth: 1, marginTop: "3.8%", borderColor: "white" }}></View>
                                            <Text style={{ color: "white", fontSize: 15,fontWeight:'600' }}>OR REGISTER WITH</Text>
                                            <View style={{ width: wp("20%"), height: hp("0%"), borderWidth: 1, marginTop: "3.8%", borderColor: "white" }}></View>
                                        </View>
                                        <View style={{ height: hp("8%"), flexDirection: "row", alignItems: "center", justifyContent: "center" }}>
                                            <TouchableOpacity>
                                                <Image source={require("../../../assests/FB-icon.png")} style={styles.shareBtn} />
                                            </TouchableOpacity>
                                            <TouchableOpacity>
                                                <Image source={require("../../../assests/Insta.png")} style={styles.shareBtn} />
                                            </TouchableOpacity>
                                            <TouchableOpacity>
                                                <Image source={require("../../../assests/Google.png")} style={styles.shareBtn} />
                                            </TouchableOpacity>
                                            <TouchableOpacity>
                                                <Image source={require("../../../assests/Wechat.png")} style={styles.shareBtn} />
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </LinearGradient>
                            </View>

                        </View>
                    </TouchableWithoutFeedback>
                </ImageBackground>
                {/* // </Container> */}
            </KeyboardAwareScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
    },
    textStyle: {
        color: black,
        fontSize: hp("2%")
    },
    textHintStyle: {
        color: colorPrimary,
        fontSize: hp('2.8%'),
        margin: hp('2%')
    },
    bottomView: {
        // position: 'absolute',
        // bottom: hp('6%'),
        marginTop: hp('4%'),
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',

    },
    shareBtn: { height: wp("15%"), width: wp("15%") }
})